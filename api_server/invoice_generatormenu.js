//import $ from 'jquery';

const fs = require('fs');
const pdf = require('html-pdf');
const path = require('path');
const settings = require('./settings');
//const allProductNamesByCode = require('../models/product-list').getAllProductNamesByCode();

const options = { format: 'A5', base: __dirname };

const OneDayInMillis = 24 * 60 * 60 * 1000;
//import later from '../src/assets/landing3.jpg';

const paymentTypes = {
   cod: 'Bar / EC',
   paypal: 'PayPal ({{paypalTransactionID}})'
};

const mkdirIfNotExists = function (dirPath) {
   try {
      fs.mkdirSync(dirPath)
   } catch (err) {
      if (err.code !== 'EEXIST') throw err
   }
}

const formatDate = function (date) {
   return `${date.getDate()}.${date.getMonth() + 1}.${date.getFullYear()}`;
};

exports.calculateDates = function (orderDetails) {

   orderDetails.todaysDate = formatDate(new Date());
}

exports.generateMenuInvoice = function (orderDetails) {
   /**
    * Handles generation of PDF invoices.
    * Reads an invoice template file and replaces appropriate cake-order attributes in the template.
    * Then passes it through `html-pdf` module and generates an A5 sized pdf of the html template.
    */

   // console.log('orderDetails !!!!!!!!!!!!!!!!!!!!!!!!!!',orderDetails);

   mkdirIfNotExists(settings.invoicesHome);

   let template = fs.readFileSync(path.join(__dirname, 'invoice_menuorder.html'), 'utf8');
   let invoiceName = 'online_delivery_' + orderDetails.menuOrderId + '.pdf';

   fs.appendFile('mynewfile1.txt', 'Hello content!', function (err) {
      if (err) throw err;
      //console.log('Saved!');
   });
   for (let attribute in orderDetails) {
      let attrValue = '';

      if (orderDetails[attribute] === undefined
         || orderDetails[attribute] === null) {
         //console.log('Null Attribute: ', attribute);
         continue;
      }

      if (orderDetails[attribute].constructor === Array) {
         // console.log('orderDetails[attribute] @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@',orderDetails[attribute]);
         orderDetails[attribute].map((val) => {
            // console.log('val is ', val);
            attrValue += '<tr>' + '<td width="70%">' + (val.productName ? val.productName : '') + '</td>' + '<td width="30%">' + 'x' + (val.productQty ? val.productQty : '') + '</td>' +
               '<td>' + ' &nbsp;&nbsp;&nbsp;' + '</td>' + '<td width="30%">' + (val.amount.toFixed(2).toString().replace(".", ",") ? val.amount.toFixed(2).toString().replace(".", ",") : '') + '&nbsp;&euro;' + '</td>' + '</tr>' + '<tr>' + '<td>' + (val.productExtras ? val.productExtras : '') + '</td>' + '</tr>';
            // attrValue += (val.productName ? val.productName : '') + ' '+ ' x'+(val.productQty ? val.productQty : '') + '    '+(val.amount ? val.amount : '')+'&euro;'+ '</br>';
            // attrValue += (val ? val : '') + ', ';
            // console.log('attrValue is ***********************',attrValue);
         });
      } else {
         attrValue = orderDetails[attribute] ? orderDetails[attribute] : '';
      }

      if (attrValue.length > 1 && attrValue.slice(attrValue.length - 2) === ', ') {
         attrValue = attrValue.substring(0, attrValue.length - 2);
      }

      if (attrValue === '' || attrValue === 'undefined') {
         attrValue = 'Keine angabe';
      }

      //  console.log('attribute id =====================',attribute,'------', attrValue);

      template = template.replace('{{' + attribute + '}}', attrValue);
   }

   // template = template.replace('{{waterMarkOrder}}', '');

   if (orderDetails.pickupchecked === false) {
      // console.log('if web order true', orderDetails.deliveryAddress);
      if (orderDetails.deliveryAddress) {
         //  console.log('external web order true', orderDetails.deliveryAddress);
         template = template.replace('{{deliveryAddressOrder}}', '');
      }
      else {
         //  console.log('internal admin order true');
         template = template.replace('{{deliveryAddressOrder}}', 'not-visible');
      }
   }
   else {
      // console.log('internal admin order true');
      template = template.replace('{{deliveryAddressOrder}}', 'not-visible');
   }
   // console.log('order address:', orderDetails.deliveryAddress);


   if (orderDetails.pickupchecked === true) {
      // console.log('if web order true', orderDetails.pickupTime);
      if (orderDetails.pickupTime) {
         //  console.log('external web order true', orderDetails.pickupTime);
         template = template.replace('{{pickupTimedisplay}}', '');
      }
      else {
         // console.log('internal admin order true');
         template = template.replace('{{pickupTimedisplay}}', 'not-visible');
      }
   }
   else {
      // console.log('internal admin order true');
      template = template.replace('{{pickupTimedisplay}}', 'not-visible');
   }
   // console.log('order address:', orderDetails.pickupTime);
   // if (orderDetails.deliveryAddress) {
   //    console.log('external web order true', orderDetails.deliveryAddress);
   //    template = template.replace('{{deliveryAddressOrder}}', '');
   // }

   // if (orderDetails.pickupTime) {
   //    console.log('orderDetails.pickupTime', orderDetails.pickupTime);
   //    template = template.replace('{{pickupTimedisplay}}', '');
   // }
   // else{
   //    template = template.replace('{{pickupTimedisplay}}', 'not-visible');
   // }
   // console.log('order address:', orderDetails.deliveryAddress);


   var detailedPaymentText = paymentTypes[orderDetails.paymentType];
   if (orderDetails.paymentType === 'paypal') {
      // console.log(orderDetails.paymentDetails);
      detailedPaymentText = detailedPaymentText.replace('{{paypalTransactionID}}', orderDetails.paymentDetails.transactionID);
   }

   if (orderDetails.paymentType === 'mollie') {
      detailedPaymentText = 'Mollie ('+orderDetails.paymentDetails.method+')';
   }
   template = template.replace('{{detailedPaymentText}}', detailedPaymentText);

   return new Promise((resolve, reject) => {
      pdf.create(template, options)
         .toFile(path.join(settings.invoicesHome, invoiceName), function (err, res) {
            if (err) {
               // Invoice creation was not successful. Reject the promise
               console.log(err);
               reject(err);
            }
            // Invoice created. Resolve the promise
            resolve(res);
         });
   });
}

