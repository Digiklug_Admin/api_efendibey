module.exports = {
    mode: 'DEV',
    dbConnectionString: 'mongodb://localhost:27017/efendibey?authMechanism=DEFAULT',
    httpPort: 5080,
    httpsPort: 5443,
    hrHttpPort: 5081,
    hrHttpsPort: 5442,
    httpsEnabled: true,
    emailTransport: {
        host: "smtp.zoho.com",
        port: 465,
        secure: true,
        auth: {
            user: "noreply@digiklug.com",
            pass: "Digiklug123"
        }
    },
    adminAppHost: "admin.dev.efendibey.de",
    //adminMenuHost: "adminMenu.dev.efendibey.de",
    /*emailTransport: {
        service: "gmail",
        host: "smtp.gmail.com",
        auth: {
            user: "noreplyEfendibey@gmail.com",
            pass: "efendibey123"
        }
    },*/
    productPhotosHome : './public/productTypeImage',
    invoicesHome: './invoices',
    photosHome: './photos',
    fromEmail: 'noreply@digiklug.com',
    //fromEmail: 'noreplyEfendibey@gmail.com', //'noreply@efendibey.de',
    ccEmail: ['sufyan@digiklug.com'],
    ccEmailForDBError: ['info@digiklug.com'],
    toEmailForDBError: ['saeed.ansari@digiklug.com'],
    mollieRedirectUrl: 'https://localhost:5443/',
    mollieWebhookUrl: 'https://qa.efendibey.digiklug.com/api/mollieWebhook',
    mollieWebhookUrlForOnlineOrder: 'https://qa.efendibey.digiklug.com/api/mollieWebhookForOnlineOrder',
    mollieApiKey:'test_3tEf9rceQ6kr7WxkJ6emSj57qtJnnf',
    //ccEmailOrders: 'bestellung@efendibey.de',
    //ccEmailTableReservations: 'tischreservierung@efendibey.de', 
    //ccEmailEventReservations: 'eventreservierung@efendibey.de',
    //ccEmailContactUs: 'feedback@efendibey.de'
};
