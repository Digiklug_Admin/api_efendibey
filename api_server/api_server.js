/* eslint-disable jsx-a11y/href-no-hash */
/* jshint esversion: 6 */

var express = require('express');
var http = require('http');
var https = require('https');
var settings = require('./settings');
var path = require('path');
var compression = require('compression');
var bodyParser = require('body-parser');
var fs = require('fs');
var vhost = require('vhost');

var api = require('./api');
var adminApp = require('./admin.app');

var app = express();
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
    res.header("Access-Control-Allow-Methods", "POST,GET,PUT");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
app.use(vhost(settings.adminAppHost, adminApp));
app.use(bodyParser.json({
    limit: '20mb'
}));
app.use(bodyParser.urlencoded({
    limit: '20mb',
    extended: true
}));
app.use(compression());

// Serve static files from the React app
app.use(express.static(path.join(__dirname, '../../efendibey_web/build')));

// Test route for testing incremented fields
app.get('/api/getNextSequenceValue', api.getNextSequenceValue);

//Route for saving details from the Contact Us Form.
app.post('/api/contactus', api.contactUs);

//Route for saving details from the Table Booking Form.
app.post('/api/bookTable', api.bookTable);

//Route for saving details from the Event Booking Form.
app.post('/api/bookEvent', api.bookEvent);

//Route for saving details from the Custom Cake Form.
app.post('/api/cakeOrder', api.cakeOrder);

app.get('/api/cakeOrders', api.cakeOrder);

app.post('/api/menuOrder', api.menuOrder);
//app.get('/api/generatemenuTestPDF', api.generatemenuTestPDF);

app.get('/api/getCakeOrderByDates', api.getCakeOrderByDates);
app.put('/api/getByStatus', api.getByStatus);
app.get('/api/getPDFFile', api.getPDFFile);
//app.get('/api/getCakeOrderCount', api.getCakeOrderCount);
app.get('/api/getCakeOrdersForBaker', api.getCakeOrdersForBaker);
app.get('/api/getCatlogCount', api.getCatlogCount);
app.post('/api/getCampaignList', api.getCampaignList); 
app.get('/api/getshapecount', api.getshapecount);
//app.get('/api/getsizecount', api.getsizecount);
app.get('/api/getFlavorCount', api.getFlavorCount);
app.get('/api/getPriceCount', api.getPriceCount);
app.get('/api/getInternalExternalOrder', api.getInternalExternalOrder);
app.get('/api/getPartialAmountCount', api.getPartialAmountCount);
app.get('/api/getPaymentTypeCount', api.getPaymentTypeCount);
// app.get('/api/getBalanceAmountCount', api.getBalanceAmountCount);
//app.get('/api/getdonutdata', api.getdonutdata);
app.get('/api/getEventReservationCount', api.getEventReservationCount);
app.get('/api/getTableReservationCount', api.getTableReservationCount);
app.get('/api/getPaymentTypeInExCount', api.getPaymentTypeInExCount);
app.get('/api/getcakeshapecount', api.getcakeshapecount);
app.get('/api/getTotalCountWithoutPhoto', api.getTotalCountWithoutPhoto);
app.get('/api/getTotalCountWithPhoto', api.getTotalCountWithPhoto);
app.get('/api/getPastCakeOrder', api.getPastCakeOrder);
app.get('/api/getFutureCakeOrder', api.getFutureCakeOrder);
app.get('/api/lineChartDataByRange', api.lineChartDataByRange);
app.get('/api/lineChartDataByRangeLastYear', api.lineChartDataByRangeLastYear);
app.get('/api/getPendingCakeOrders', api.getPendingCakeOrders);
app.get('/api/getOnlineOrders', api.getOnlineOrders);
app.get('/api/getPayPalOrder', api.getPayPalOrder);
app.get('/api/getTableReservationList', api.getTableReservationList);
app.get('/api/getEventReservationList', api.getEventReservationList);
app.get('/api/getColumns', api.getColumns);
app.get('/api/getCountWithPhoto', api.getCountWithPhoto);
app.get('/api/getCountWithoutPhoto', api.getCountWithoutPhoto);
app.get('/api/getCakeOrderCounts', api.getCakeOrderCounts);
app.get('/api/getCakeOrderCountsByDate', api.getCakeOrderCountsByDate);
app.get('/api/getTotalForTableListing', api.getTotalForTableListing);
app.get('/api/getTotalForBakerList', api.getTotalForBakerList);
app.get('/api/productList', api.getProductList);
app.get('/api/reviews', api.reviews);
app.post('/api/sendTestEmail', api.sendTestEmail);
app.post('/api/generateTestPDF', api.generateTestPDF);
app.put('/api/cakeOrderUpdate', api.cakeOrderUpdate);
app.put('/api/getCountForCampaign', api.getCountForCampaign);
app.put('/api/menuOrderUpdate', api.menuOrderUpdate);
app.put('/api/menuOrderUpdateAsCLSE', api.menuOrderUpdateAsCLSE);
app.get('/api/getNextSequenceValueForMenueOrder', api.getNextSequenceValueForMenueOrder);

app.get('/api/getCouponList', api.getCouponList);
app.get('/api/getTotalForCouponTable', api.getTotalForCouponTable);
app.post('/api/storeUpdateCoupon', api.storeUpdateCoupon);

app.get('/api/getOnlineOrderMenu', api.getOnlineOrderMenu);
app.get('/api/getOnlineProductType', api.getOnlineProductType);
app.get('/api/getAllMenuOrder', api.getAllMenuOrder);
app.get('/api/getMenuOrder', api.getMenuOrder);
app.get('/api/getAllMenuProducts', api.getAllMenuProducts);
app.get('/api/getMenuProduct', api.getMenuProduct);
app.get('/api/getMenuOrderPDFFile', api.getMenuOrderPDFFile);
app.get('/api/shopOpenTime', api.shopOpenTime);
app.post('/api/addMenuProduct', api.addMenuProduct);
app.put('/api/updateMenuProduct', api.updateMenuProduct);
app.put('/api/updateCakeOrderStatus', api.updateCakeOrderStatus);
app.get('/api/deliveryTime', api.deliveryTime);
app.put('/api/updateDeliveryTime', api.updateDeliveryTime);
app.post('/api/mollieWebhook', api.mollieWebhook);
app.post('/api/mollieWebhookForOnlineOrder', api.mollieWebhookForOnlineOrder);
app.post('/api/getMolliePayment', api.getMolliePayment);
app.put('/api/changeStatusOfOnlineOrder', api.changeStatusOfOnlineOrder);

// The handler for serving the main app
app.get('*', function(req, res) {
    res.sendFile(path.join(__dirname, '../../efendibey_web/build/index.html'));
});

var key = fs.readFileSync(path.join(__dirname, './certs/efendibey.de_privatekey.txt'));
var cert = fs.readFileSync(path.join(__dirname, './certs/www.efendibey.de.crt'));

if (settings.httpsEnabled) {
    https.createServer({ key: key, cert: cert }, app).listen(settings.httpsPort);

    var redirectApp = express();
    redirectApp.get('*', (req, res) => {
        res.redirect(`https://${req.headers.host.split(':')[0]}:${settings.httpsPort}${req.url}`);
    });

    http.createServer(redirectApp).listen(settings.httpPort);

    console.log(`App listening on ${settings.httpPort} (HTTP) and ${settings.httpsPort} (HTTPS)`);
} else {
    http.createServer(app).listen(settings.httpPort);

    console.log(`App listening on ${settings.httpPort} (HTTP only)`);
}