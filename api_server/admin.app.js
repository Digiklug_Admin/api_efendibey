/* jshint esversion: 6 */

var express = require('express');
var path = require('path');
var compression = require('compression');
var bodyParser = require('body-parser');

var api = require('./api');


var app = express();
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
    res.header("Access-Control-Allow-Methods", "POST,GET,PUT");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
app.use(bodyParser.json({
    limit: '20mb'
}));
app.use(bodyParser.urlencoded({
    limit: '20mb',
    extended: true
}));
app.use(compression());

// Serve static files from the Angular app
app.use(express.static(path.join(__dirname, '../../efendibey_admin/dist/efendibey-admin')));

// API routes
app.post('/adminApi/login', api.login);

// Product List
app.get('/adminApi/productList', api.getProductList);

// Product List
app.get('/adminApi/katalog', api.getKatalog);

//Route for saving details from the Custom Cake Form.
app.post('/adminApi/cakeOrder', api.cakeOrder);

//Route for getting cake orders from the Database Collection.
app.get('/adminApi/getCakeOrders', api.getCakeOrders);


app.post('/adminApi/menuOrder', api.menuOrder);
app.put('/adminApi/updateMenuProduct', api.updateMenuProduct);
//app.get('/adminApi/generatemenuTestPDF', api.generatemenuTestPDF);

//Route for getting cake orders from the Database Collection.
app.get('/adminApi/getCakeOrder', api.getCakeOrder);
app.get('/adminApi/getCakeOrderByDates', api.getCakeOrderByDates);
app.put('/adminApi/getByStatus', api.getByStatus);
app.get('/adminApi/getPDFFile', api.getPDFFile);
app.post('/adminApi/getCampaignList', api.getCampaignList);
app.get('/adminApi/getCakeOrdersForBaker', api.getCakeOrdersForBaker);
app.get('/adminApi/getcakeshapecount', api.getcakeshapecount);
app.get('/adminApi/getCatlogCount', api.getCatlogCount);
//app.get('/adminApi/getCakeOrdersForFilter', api.getCakeOrdersForFilter); 
app.get('/adminApi/getshapecount', api.getshapecount);
//app.get('/adminApi/getdonutdata', api.getdonutdata);
//app.get('/adminApi/getsizecount', api.getsizecount);
app.get('/adminApi/getFlavorCount', api.getFlavorCount);
app.get('/adminApi/getPriceCount', api.getPriceCount);
app.get('/adminApi/getInternalExternalOrder', api.getInternalExternalOrder);
app.get('/adminApi/getPartialAmountCount', api.getPartialAmountCount);
app.get('/adminApi/getPaymentTypeCount', api.getPaymentTypeCount);
app.get('/adminApi/getEventReservationCount', api.getEventReservationCount);
app.get('/adminApi/getTableReservationCount', api.getTableReservationCount);
app.get('/adminApi/getPaymentTypeInExCount', api.getPaymentTypeInExCount);
app.get('/adminApi/getTotalCountWithoutPhoto', api.getTotalCountWithoutPhoto);
app.get('/adminApi/getTotalCountWithPhoto', api.getTotalCountWithPhoto);
app.get('/adminApi/getPastCakeOrder', api.getPastCakeOrder);
app.get('/adminApi/getFutureCakeOrder', api.getFutureCakeOrder);
app.get('/adminApi/getCountWithPhoto', api.getCountWithPhoto);
app.get('/adminApi/getCountWithoutPhoto', api.getCountWithoutPhoto);
app.get('/adminApi/getCakeOrderCounts', api.getCakeOrderCounts);
app.get('/adminApi/getCakeOrderCountsByDate', api.getCakeOrderCountsByDate);
app.get('/adminApi/lineChartDataByRange', api.lineChartDataByRange);
app.get('/adminApi/lineChartDataByRangeLastYear', api.lineChartDataByRangeLastYear);
app.get('/adminApi/getPendingCakeOrders', api.getPendingCakeOrders);
app.get('/adminApi/getOnlineOrders', api.getOnlineOrders);
app.get('/adminApi/getPayPalOrder', api.getPayPalOrder);
app.get('/adminApi/getTableReservationList', api.getTableReservationList);
app.get('/adminApi/getEventReservationList', api.getEventReservationList);
app.get('/adminApi/getEventReservationList', api.getEventReservationList);
app.get('/adminApi/getColumns', api.getColumns);
app.get('/adminApi/getTotalForTableListing', api.getTotalForTableListing);
app.get('/adminApi/getTotalForBakerList', api.getTotalForBakerList);
app.put('/adminApi/getCountForCampaign', api.getCountForCampaign);
app.put('/adminApi/menuOrderUpdate', api.menuOrderUpdate);
app.put('/adminApi/menuOrderUpdateAsCLSE', api.menuOrderUpdateAsCLSE);

//Route for updating cake orders from the Database Collection.
app.put('/adminApi/updateCakeOrders', api.updateCakeOrders);
app.put('/adminApi/updateCakeOrderStatus', api.updateCakeOrderStatus);
app.put('/adminApi/cancelFromCakeOrder', api.cancelFromCakeOrder);
app.put('/adminApi/cakeOrderUpdate', api.cakeOrderUpdate);
app.post('/adminApi/getColumnsupdate', api.getColumnsupdate);

app.get('/adminApi/getCouponList', api.getCouponList);
app.get('/adminApi/getTotalForCouponTable', api.getTotalForCouponTable);
app.post('/adminApi/storeUpdateCoupon', api.storeUpdateCoupon);
app.get('/adminApi/getTotalForFoodOrder', api.getTotalForFoodOrder);
app.get('/adminApi/getFoodOrders', api.getFoodOrders);
app.put('/adminApi/changeStatusOfOnlineOrder', api.changeStatusOfOnlineOrder);
app.get('/api/getAllMenuOrder', api.getAllMenuOrder);
app.get('/adminApi/deliveryTime', api.deliveryTime);
app.put('/adminApi/updateDeliveryTime', api.updateDeliveryTime);
app.post('/adminApi/mollieWebhook', api.mollieWebhook);
app.post('/adminApi/mollieWebhookForOnlineOrder', api.mollieWebhookForOnlineOrder);
app.get('/adminApi/getColorCount', api.getColorCount);


// Keep this last as this is a fallback route
app.get('*', function(req, res) {
    console.log(`Serving ${req.url} through get('*') fallback`);
    res.sendFile(path.join(__dirname, '../../efendibey_admin/dist/efendibey-admin/index.html'));
});

module.exports = app;