/* jshint esversion: 6 */

var MongoClient = require('mongodb').MongoClient;
var settings = require('./settings');

var state = {
   dbRef: null,
   dbConn: null
}

exports.connect = function (done) {
   if (state.dbRef) return done();

   MongoClient.connect(settings.dbConnectionString, function (err, database) {
      if (err) return done(err);
      state.dbRef = database.db('efendibey');
      state.dbConn = database;
      done();
   });
};

exports.get = function () {
   return state.dbRef;
};

exports.getConn = function () {
   return state.dbConn;
};

exports.close = function (done) {
   if (state.dbConn) {
      state.dbConn.close(function (err, result) {
         state.dbConn = null;
         state.dbRef = null;
         done(err);
      });
   }
};

exports.getNextSequenceValue = function (sequenceName, done) {
   var sequenceDocument = state.dbRef.collection('Counters').findAndModify(
      { _id: sequenceName },
      [['_id','asc']],  // sort order
      { $inc: { sequence_value: 1 } },
      { new: true },
      done
   );
}
