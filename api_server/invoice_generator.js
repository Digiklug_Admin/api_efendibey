const fs = require("fs");
const pdf = require("html-pdf");
const path = require("path");
const settings = require("./settings");
const allProductNamesByCode =
  require("../models/product-list").getAllProductNamesByCode();

const options = { format: "A5", base: __dirname };

const OneDayInMillis = 24 * 60 * 60 * 1000;
//import later from '../src/assets/landing3.jpg';

const paymentTypes = {
  cod: "Bar / EC",
  creditcard: "Überweisung",
  paypal: "PayPal ({{paypalTransactionID}})",
  mollie: "Mollie ({{paypalTransactionID}})",
};

const mkdirIfNotExists = function (dirPath) {
  try {
    fs.mkdirSync(dirPath);
  } catch (err) {
    if (err.code !== "EEXIST") throw err;
  }
};

const formatDate = function (date) {
  return `${date.getDate()}.${date.getMonth() + 1}.${date.getFullYear()}`;
};

exports.calculateDates = function (orderDetails) {
  let delDateParts = orderDetails.deliveryDate.split(".");
  let delDate = new Date(
    parseInt(delDateParts[2], 10),
    parseInt(delDateParts[1], 10) - 1,
    parseInt(delDateParts[0], 10)
  );
  let delTime = parseInt(orderDetails.deliveryTime.substr(0, 2), 10);
  let d;
  if (delTime >= 13) {
    orderDetails.preparationDate = orderDetails.deliveryDate;
    d = new Date(delDate.valueOf());
  } else {
    orderDetails.preparationDate = formatDate(
      new Date(delDate.valueOf() - OneDayInMillis)
    );
    d = new Date(delDate.valueOf() - OneDayInMillis);
  }

  orderDetails.todaysDate = formatDate(new Date());

  let weekday = new Array(7);
  weekday[0] = "So";
  weekday[1] = "Mo";
  weekday[2] = "Di";
  weekday[3] = "Mi";
  weekday[4] = "Do";
  weekday[5] = "Fr";
  weekday[6] = "Sa";

  orderDetails.todaysDay = weekday[delDate.getDay()];
  orderDetails.delDay = weekday[d.getDay()];
};

exports.generateInvoice = function (orderDetails) {
  /**
   * Handles generation of PDF invoices.
   * Reads an invoice template file and replaces appropriate cake-order attributes in the template.
   * Then passes it through `html-pdf` module and generates an A5 sized pdf of the html template.
   */

  mkdirIfNotExists(settings.invoicesHome);

  let template = fs.readFileSync(
    path.join(__dirname, "invoice_template.html"),
    "utf8"
  );
  let invoiceName = "";
  invoiceName = orderDetails.internalOrder
    ? "shop_order_" + orderDetails.orderId + ".pdf"
    : "online_order_" + orderDetails.orderId + ".pdf";

  fs.appendFile("mynewfile1.txt", "Hello content!", function (err) {
    if (err) throw err;
    //console.log('Saved!');
  });

  for (let attribute in orderDetails) {
    let attrValue = "";

    if (
      orderDetails[attribute] === undefined ||
      orderDetails[attribute] === null
    ) {
      continue;
    }

    if (orderDetails[attribute].constructor === Array) {
      orderDetails[attribute].map((val) => {
        if (allProductNamesByCode[val]) {
          attrValue += allProductNamesByCode[val] + ", ";
        } else {
          attrValue += (val ? val : "") + ", ";
        }
      });
    } else {
      if (allProductNamesByCode[orderDetails[attribute]]) {
        attrValue = allProductNamesByCode[orderDetails[attribute]];
      } else {
        attrValue = orderDetails[attribute] ? orderDetails[attribute] : "";
      }
    }

    if (
      attrValue.length > 1 &&
      attrValue.slice(attrValue.length - 2) === ", "
    ) {
      attrValue = attrValue.substring(0, attrValue.length - 2);
    }

    if (attrValue === "") {
      attrValue = "Keine angabe";
    }

    template = template.replace("{{" + attribute + "}}", attrValue);
  }

  if (orderDetails.status === "CANC") {
    // console.log('order canc call');
    template = template.replace("{{waterMarkOrder}}", "not-visible");
    template = template.replace("{{waterMark}}", "");
  } else {
    if (orderDetails.internalOrder === false) {
      // console.log('external order true');
      template = template.replace("{{waterMarkOrder}}", "");
      template = template.replace("{{waterMark}}", "not-visible");
    } else {
      // console.log('internal order true');
      template = template.replace("{{waterMarkOrder}}", "not-visible");
      template = template.replace("{{waterMark}}", "not-visible");
    }
  }

  // if (orderDetails.internalOrder === false) { // commented by gaus //25/09/2021
  if (orderDetails.deliveryAddress) {
    template = template.replace("{{deliveryAddressOrder}}", "");
  } else {
    // console.log('internal admin order true');
    template = template.replace("{{deliveryAddressOrder}}", "not-visible");
  }

  // commented by gaus //25/09/2021
  // } else {
  //     // console.log('internal admin order true');
  //     template = template.replace('{{deliveryAddressOrder}}', 'not-visible');
  // }

  if (orderDetails.shape === "SHP99") {
    template = template.replace("{{priceClass}}", "priceClassSmall");
  } else {
    template = template.replace("{{priceClass}}", "priceClassNormal");
  }

  if (
    orderDetails.extraDecoration.length > 0 &&
    orderDetails.extraDecoration.includes("EDECO01")
  ) {
    if (orderDetails.photo) {
      template = template.replace("{{customPhotoClass}}", "");
      template = template.replace(
        "{{defaultPhotoClass}}",
        "cakePhotoNotVisible"
      );
    } else {
      template = template.replace(
        "{{customPhotoClass}}",
        "cakePhotoNotVisible"
      );
      template = template.replace("{{defaultPhotoClass}}", "");
    }
  } else {
    template = template.replace("{{customPhotoClass}}", "cakePhotoNotVisible");
    template = template.replace("{{defaultPhotoClass}}", "cakePhotoNotVisible");
  }

  if (
    orderDetails.decoration === "DRN02" ||
    orderDetails.decoration === "DRN05" ||
    orderDetails.decoration === "DRN06"
  ) {
    template = template.replace("{{sauseColorClass}}", "");
  } else {
    template = template.replace("{{sauseColorClass}}", "sauceColorNotVisible");
  }

  if (
    orderDetails.decoration === "DRN10" ||
    orderDetails.decoration === "DRN11"
  ) {
    template = template.replace("{{dekoColorClass}}", "");
  } else {
    template = template.replace("{{dekoColorClass}}", "dekoColorNotVisible");
  }

  if (orderDetails.decoration === "DRN13") {
    template = template.replace("{{dripColorClass}}", "");
  } else {
    template = template.replace("{{dripColorClass}}", "dripColorNotVisible");
  }

  template = template.replace("{{extraAmountClass}}", "");
  template = template.replace("{{partialPmntClass}}", "");
  template = template.replace("{{balClass}}", "");

  if (orderDetails.catalogNumber) {
    template = template.replace("{{catalogNumberClass}}", "");
    template = template.replace("{{cakeDecoClass}}", "not-visible");
    template = template.replace("{{cakeColorClass}}", "not-visible");
  } else {
    template = template.replace("{{catalogNumberClass}}", "not-visible");
    template = template.replace("{{cakeDecoClass}}", "");
    template = template.replace("{{cakeColorClass}}", "");
  }

  if (orderDetails.qty) {
    template = template.replace("{{cakeSizeClass}}", "not-visible");
    template = template.replace("{{cakeQtyClass}}", "");
  } else {
    template = template.replace("{{cakeSizeClass}}", "");
    template = template.replace("{{cakeQtyClass}}", "not-visible");
  }

  if (orderDetails.deliveryType == "delivery") {
    template = template.replace("{{cakeSizeClass}}", "not-visible");
    template = template.replace("{{cakeQtyClass}}", "");
  } else {
    template = template.replace("{{cakeSizeClass}}", "");
    template = template.replace("{{cakeQtyClass}}", "not-visible");
  }

  if (orderDetails.deliveryType == "delivery") {
    template = template.replace("{{deliveryTypeOrder}}", "");
  } else if (orderDetails.deliveryType == "pickup") {
    template = template.replace("{{deliveryTypeOrder}}", "not-visible");
  }

  if (orderDetails.employee) {
    template = template.replace("{{employeeName}}", "");
  } else {
    template = template.replace("{{employeeName}}", "not-visible");
  }

  var detailedPaymentText = paymentTypes[orderDetails.paymentType];
  if (orderDetails.paymentType === "paypal") {
    detailedPaymentText = detailedPaymentText.replace(
      "{{paypalTransactionID}}",
      orderDetails.paymentDetails.transactionID
    );
  }
  if (orderDetails.paymentType === "mollie") {
    detailedPaymentText = detailedPaymentText.replace(
      "{{paypalTransactionID}}",
      orderDetails.paymentDetails.method
    );
  }
  template = template.replace("{{detailedPaymentText}}", detailedPaymentText);

  return new Promise((resolve, reject) => {
    pdf
      .create(template, options)
      .toFile(
        path.join(settings.invoicesHome, invoiceName),
        function (err, res) {
          if (err) {
            // Invoice creation was not successful. Reject the promise
            console.log(err);
            reject(err);
          }
          // Invoice created. Resolve the promise
          resolve(res);
        }
      );
  });
};
