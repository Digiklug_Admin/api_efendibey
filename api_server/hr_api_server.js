/* eslint-disable jsx-a11y/href-no-hash */
/* jshint esversion: 6 */

var express = require('express');
var http = require('http');
var https = require('https');
var settings = require('./settings');
var path = require('path');
var compression = require('compression');
var bodyParser = require('body-parser');
var fs = require('fs');
// var vhost = require('vhost');

var api = require('./api');
// var adminApp = require('./admin.app');

var app = express();
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
    res.header("Access-Control-Allow-Methods", "POST,GET,PUT");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
// app.use(vhost(settings.adminAppHost, adminApp));
app.use(bodyParser.json({
    limit: '20mb'
}));
app.use(bodyParser.urlencoded({
    limit: '20mb',
    extended: true
}));
app.use(compression());

// Serve static files from the React app
app.use(express.static(path.join(__dirname, '../../ebhighresolution/build')));

// Test route for testing incremented fields
app.get('/api/getNextSequenceValue', api.getNextSequenceValue);
app.get('/api/productList', api.getProductList);

//Route for saving details from the Custom Cake Form.
app.post('/api/cakeOrder', api.cakeOrder);
app.get('/api/cakeOrders', api.cakeOrder);
app.put('/api/cakeOrderUpdate', api.cakeOrderUpdate);
app.put('/api/updateCakeOrderStatus', api.updateCakeOrderStatus);

app.post('/api/getMolliePayment', api.getMolliePayment);
app.post('/api/getCampaignList', api.getCampaignList); 
app.put('/api/getCountForCampaign', api.getCountForCampaign);

// The handler for serving the main app
app.get('*', function(req, res) {
    res.sendFile(path.join(__dirname, '../../ebhighresolution/build/index.html'));
});

var key = fs.readFileSync(path.join(__dirname, './certs/efendibey.de_privatekey.txt'));
var cert = fs.readFileSync(path.join(__dirname, './certs/www.efendibey.de.crt'));

if (settings.httpsEnabled) {
    https.createServer({ key: key, cert: cert }, app).listen(settings.hrHttpsPort);

    var redirectApp = express();
    redirectApp.get('*', (req, res) => {
        res.redirect(`https://${req.headers.host.split(':')[0]}:${settings.hrHttpsPort}${req.url}`);
    });

    http.createServer(redirectApp).listen(settings.hrHttpPort);

    console.log(`App listening on ${settings.hrHttpPort} (HTTP) and ${settings.hrHttpsPort} (HTTPS)`);
} else {
    http.createServer(app).listen(settings.hrHttpPort);

    console.log(`App listening on ${settings.hrHttpPort} (HTTP only)`);
}