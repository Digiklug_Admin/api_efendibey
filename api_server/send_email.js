/* jshint esversion: 6 */

const settings = require("./settings");
const nodemailer = require("nodemailer");
const path = require("path");

module.exports = function () {
  var smtpTransport = nodemailer.createTransport(settings.emailTransport);
  var fromEmail = settings.fromEmail != null ? settings.fromEmail : "";
  var ccEmail = settings.ccEmail != null ? settings.ccEmail : "";
  var ccEmailOrders =
    settings.ccEmailOrders != null ? settings.ccEmailOrders : "";
  var ccEmailTableReservations =
    settings.ccEmailTableReservations != null
      ? settings.ccEmailTableReservations
      : "";
  var ccEmailEventReservations =
    settings.ccEmailEventReservations != null
      ? settings.ccEmailEventReservations
      : "";
  var ccEmailContactUs =
    settings.ccEmailContactUs != null ? settings.ccEmailContactUs : "";
  var emailSignature =
    "Danke und bis bald.\nTeam Efendi Bey\n\nTelefon: 051117507\nMünzstraße 7, 30159 Hannover\n\nÖffnungszeiten 8Uhr bis 23Uhr\n \n ------------------------------------------------------------------------------------------\n Ince GmbH, Münzstraße 7, 30159 Hannover, Germany\n Sitz der Gesellschaft/Registered Office: Hannover, \n Registergericht/Registered Court: Amtsgericht Hannover, HRB 61695, \n USt.-ID-Nr./VAT-ID-No. DE 235665266 \n ------------------------------------------------------------------------------------------ ";
  var photoNotAttached =
    "Bitte senden Sie uns Ihr Foto für die Torte umgehend zu. Schrieben Sie bitte auch die Bestellnummer und Ihren Namen dazu.\n\n";
  var ccEmailForDBError =
    settings.ccEmailForDBError != null ? settings.ccEmailForDBError : "";
  var toEmailForDBError =
    settings.toEmailForDBError != null ? settings.toEmailForDBError : "";

  //console.log(`smtpTransport: ${smtpTransport}`);
  console.log(`fromEmail: ${fromEmail}`);
  console.log(`ccEmail: ${ccEmail}`);
  console.log(`ccEmailOrders: ${ccEmailOrders}`);
  console.log(`ccEmailTableReservations: ${ccEmailTableReservations}`);
  console.log(`ccEmailEventReservations: ${ccEmailEventReservations}`);
  console.log(`ccEmailContactUs: ${ccEmailContactUs}`);

  sendMail = (mailOptions) => {
    smtpTransport.sendMail(mailOptions, function (error, res) {
      if (error) {
        console.log(`Error while sending email: ${error}`);
      } else {
        console.log("Email sent with response: ", res);
      }
    });
  };

  sendDbErrorMail = (cakeData) => {
    // var test = cakeData;
    this.sendMail({
      from: fromEmail,
      to: [toEmailForDBError],
      cc: [ccEmailForDBError],
      subject: cakeData.subj,
      text: JSON.stringify(cakeData),
    });
    console.log(
      `The DB error email for order ${
        cakeData.orderId
      } has been sent at  ${getDateTimeNow()}`
    );
  };

  //Sending Email from Contact Us form.
  sendConfirmationEmail = (contactUs, res) => {
    this.sendMail({
      from: fromEmail,
      to: contactUs.email,
      cc: [ccEmail, ccEmailContactUs],
      subject: "Ihre Nachricht an Efendi Bey",
      text: `Hallo ${contactUs.name}!\n\nVielen Dank! Ihre Anfrage ist bei uns eingegangen. \nWir melden uns umgehend bei Ihnen.\n\nKontakt Angaben:\n----------------------------------------\n Name: ${contactUs.name}\n Email: ${contactUs.email}\n Handy: ${contactUs.mobile}\n Nachricht: ${contactUs.message}\n----------------------------------------\n\n${emailSignature}`,
    });
    console.log(
      `A confirmation email has been sent to ${
        contactUs.email
      } at ${getDateTimeNow()}`
    );
  };

  //Sending Email after Table has been reserved.
  sendTableConfirmationEmail = (tableReservation, res) => {
    this.sendMail({
      from: fromEmail,
      to: tableReservation.guestEmail,
      cc: [ccEmail, ccEmailTableReservations],
      subject: "Ihre Tischreservierung im Efendi Bey",
      text: `Hallo Lieber Gast!\n\nVielen Dank für Ihre Reservierung. Wir prüfen Ihre Anfrage und bestätigen in einer nächsten Email oder mit einem anruf Ihre Plätze.\n\n\nTisch Reservierung Angaben:\n------------------------------------------------\n Name: ${tableReservation.guestName}\n Email: ${tableReservation.guestEmail}\n Handy: ${tableReservation.guestMobile}\n Anzahl der Gäste: ${tableReservation.guestCount}\n Datum & Zeit: ${tableReservation.date}  ${tableReservation.time}\n------------------------------------------------\n\n${emailSignature} \n`,
    });
    console.log(
      `Table confirmation Email has been sent to ${
        tableReservation.guestEmail
      } at ${getDateTimeNow()}`
    );
  };
  //Event Reservation.
  sendEventConfirmationEmail = (eventReservation, res) => {
    this.sendMail({
      from: fromEmail,
      to: eventReservation.guestEmail,
      cc: [ccEmail, ccEmailEventReservations],
      subject: "Ihre Anfrage für ein Ereignis im Efendi Bey",
      text: `Hallo Lieber Gast! \n\nVielen Dank für Ihre Reservierung. Wir prüfen Ihre Anfrage und bestätigen in einer nächsten Email Ihre Plätze.\n\nFür die Art der Dekoration und die Tischzusammenstellung würden wir uns in einer nächsten Email melden.\n Gerne können wir Sie auch anrufen oder uns vor Ort treffen.\n\nEreignis Reservierung Angaben:\n------------------------------------------------\n Name: ${eventReservation.guestName}\n Email: ${eventReservation.guestEmail}\n Handy: ${eventReservation.guestMobile}\n Veranstaltungs Art: ${eventReservation.type}\n Anzahl der Gäste: ${eventReservation.size}\n Datum & Zeit: ${eventReservation.date}  ${eventReservation.startTime} \n Dauer: ${eventReservation.duration}\n------------------------------------------------\n\n${emailSignature} \n `,
    });
    console.log(
      `Event confirmation Email has been sent to ${
        eventReservation.guestEmail
      } at ${getDateTimeNow()}`
    );
  };

  sendCustomCakeEmail = (cakeData) => {
    let emailText = "";
    let emailAttachments = [];
    let invoiceName = "";
    invoiceName = cakeData.internalOrder
      ? `shop_order_${cakeData.orderId}.pdf`
      : `online_order_${cakeData.orderId}.pdf`;
    emailAttachments.push({
      filename: invoiceName,
      path: path.join(settings.invoicesHome, invoiceName),
    });

    if (cakeData.photoFilePath !== "" && cakeData.photoFilePath !== undefined) {
      emailAttachments.push({
        filename: `Photo_${cakeData.photoFileName}`,
        path: cakeData.photoFilePath,
      });
    }

    if (
      cakeData.musterPhotoFilePath !== "" &&
      cakeData.musterPhotoFilePath !== undefined
    ) {
      emailAttachments.push({
        filename: `Photo_${cakeData.musterPhotoFileName}`,
        path: cakeData.musterPhotoFilePath,
      });
    }

    var cakePhotoText =
      cakeData.extraDecoration.length !== 0 &&
      cakeData.extraDecoration.includes("EDECO01") &&
      cakeData.photoFilePath === ""
        ? photoNotAttached
        : "";

    // if (cakeData.paymentType === "cod") {
    //    emailText = `Vielen Dank! \n\nIhre Bestellung ist bei uns eingegangen. Sie haben die bezahlart 'Bar' gewählt.\n Hier bitten wir Sie oder eine Person die Sie beauftragen darum bei uns vorbeizukommen weil wir nur gegen eine Anzahlung Ihre Torte machen können.\n Bitte bringen Sie dazu Ihre Bestellnummer auf dem Auftragsschein anbei mit. Gerne können Sie diese auch ausdrucken oder auf dem Smartphone mitbringen.\n Bitte mindestens 24 Stunden vorher vorbeikommen.\n\n${cakePhotoText}\n\n${emailSignature} \n `;
    // } else if (cakeData.paymentType === "paypal" || cakeData.paymentType === "mollie") {
    emailText = `Vielen Dank! \n\nIhre Bestellung ist bei uns eingegangen. Anbei finden Sie Ihren Bestellschein. Bitte kontrollieren Sie alle Angaben. Bei Änderungen können Sie uns gerne telefonisch erreichen.\n\n${cakePhotoText} \n\n${emailSignature} \n `;
    // } else
    if (cakeData.paymentType === "creditcard") {
      emailText = `Vielen Dank! \n\nIhre Bestellung ist bei uns eingegangen. Sie haben die bezahlart 'Überweisung' gewählt.\n Hier bitten wir Sie darum die Überweisung spätestens drei Tage, bei Feiertagen fünf Tage vorher zu machen.  Anbei finden Sie Ihren Bestellschein.\n\n${cakePhotoText}\n\n${emailSignature} \n `;
      // Add additional pdf when selecting bank transfer
      emailAttachments.push({
        filename: "ueberweisung.jpg",
        path: path.join(__dirname, "ueberweisung.jpg"),
      });
    }

    // console.log("cakeData.updateRecord ================================ ",cakeData.updateRecord);
    var extraText = "";
    var emailSubject;
    if (cakeData.urgent !== "yes") {
      extraText = "";
    } else {
      extraText = "!";
    }

    let truckIcon = " 🚚 ";

    if (cakeData.updateRecord === "yes") {
      extraText += "UPD";
      emailText = `Vielen Dank! \n\nIhre Änderung ist bei uns eingegangen. Anbei finden Sie Ihren Bestellschein.\n\n${cakePhotoText}\n\n${emailSignature} \n `;
      if (cakeData.deliveryType == "delivery") {
        emailSubject =
          extraText +
          "[Bestellungs Nr. " +
          cakeData.orderId +
          "]" +
          truckIcon +
          "Änderung Tortenbestellung bei Efendi Bey";
      } else if (cakeData.deliveryType == "pickup") {
        emailSubject =
          extraText +
          "[Bestellungs Nr. " +
          cakeData.orderId +
          "] Änderung Tortenbestellung bei Efendi Bey";
      }
    }

    if (cakeData.status === "CANC") {
      emailText = `Schade, \n\nIhre Bestellung haben wir storniert. Wir würden uns über Ihre nächste Bestellung freuen. \n\n  ${emailSignature} \n `;

      if (cakeData.deliveryType == "delivery") {
        emailSubject =
          extraText +
          "[Bestellungs Nr. " +
          cakeData.orderId +
          "]" +
          truckIcon +
          " Ihre bestellung wurde Storniert";
      } else if (cakeData.deliveryType == "pickup") {
        emailSubject =
          extraText +
          "[Bestellungs Nr. " +
          cakeData.orderId +
          "] Ihre bestellung wurde Storniert";
      }
    } else {
      if (cakeData.deliveryType == "delivery") {
        emailSubject =
          extraText +
          "[Bestellungs Nr. " +
          cakeData.orderId +
          "]" +
          truckIcon +
          " Ihre Tortenbestellung bei Efendi Bey";
      } else if (cakeData.deliveryType == "pickup") {
        emailSubject =
          extraText +
          "[Bestellungs Nr. " +
          cakeData.orderId +
          "] Ihre Tortenbestellung bei Efendi Bey";
      }
    }

    this.sendMail({
      from: fromEmail,
      to: cakeData.email,
      cc: [ccEmail, ccEmailOrders],
      // subject: exclamation + "[Bestellungs Nr. " + cakeData.orderId + "] Ihre Tortenbestellung bei Efendi Bey",
      subject: emailSubject,
      text: emailText,
      attachments: emailAttachments,
    });
    console.log(
      `Sending custom cake Email for order ${cakeData.orderId}  to ${cakeData.email}... }`
    );
  };

  sendCustomMenuOrderEmail = (menuOrder) => {
    let emailText = "";
    let emailAttachments = [];

    let invoiceName = `online_delivery_${menuOrder.menuOrderId}.pdf`;
    emailAttachments.push({
      filename: invoiceName,
      path: path.join(settings.invoicesHome, invoiceName),
    });

    if (menuOrder.paymentType === "cod") {
      emailText = `Vielen Dank! \n\nIhre Bestellung ist bei uns eingegangen. Sie haben die bezahlart 'Bar' gewählt.\n Bitte halten Sie möglichst den passenden Betrag bereit.\n Wir rufen Sie vorher auch an um die Bestellung zu bestätigen.\n\n${emailSignature} \n `;
    } else if (menuOrder.paymentType === "paypal") {
      emailText = `Vielen Dank! \n\nIhre Bestellung ist bei uns eingegangen. Anbei finden Sie Ihren Bestellschein zur Paypal Zahlung.\n\n${emailSignature} \n `;
    } else if (menuOrder.paymentType === "creditcard") {
      console.log(" ===================== ");
      emailText = `Vielen Dank! \n\nIhre Bestellung ist bei uns eingegangen. Sie haben die bezahlart 'Überweisung' gewählt.\n Hier bitten wir Sie darum die Überweisung spätestens drei Tage, bei Feiertagen fünf Tage vorher zu machen.  Anbei finden Sie Ihren Bestellschein.\n\n${emailSignature} \n `;
      // Add additional pdf when selecting bank transfer
      emailAttachments.push({
        filename: "ueberweisung.jpg",
        path: path.join(__dirname, "ueberweisung.jpg"),
      });
    } else if (menuOrder.paymentType === "mollie") {
      emailText = `Ihre Bestellung ist bei uns eingegangen. Anbei finden Sie Ihren Bestellschein \n`;
    }
    var extraText = "";
    var emailSubject;

    emailSubject =
      extraText +
      "[Bestellungs Nr. " +
      menuOrder.menuOrderId +
      "] Ihre Bestellung bei Efendibey";

    this.sendMail({
      from: fromEmail,
      to: menuOrder.email,
      cc: [ccEmail, ccEmailOrders],
      subject: emailSubject,
      text: emailText,
      attachments: emailAttachments,
    });
    console.log(
      `Custom menu order email for menu order ${
        menuOrder.menuOrderId
      } has been sent to ${menuOrder.email} at ${getDateTimeNow()}`
    );
  };

  return this;
};

// function return date time now
function getDateTimeNow() {
  let date_ob = new Date();

  // current date
  // adjust 0 before single digit date
  let date = ("0" + date_ob.getDate()).slice(-2);

  // current month
  let month = ("0" + (date_ob.getMonth() + 1)).slice(-2);

  // current year
  let year = date_ob.getFullYear();

  // current hours
  let hours = date_ob.getHours();

  // current minutes
  let minutes = date_ob.getMinutes();

  // current seconds
  let seconds = date_ob.getSeconds();

  return (
    date +
    "-" +
    month +
    "-" +
    year +
    " " +
    hours +
    ":" +
    minutes +
    ":" +
    seconds
  );
}
