/* eslint-disable jsx-a11y/href-no-hash */
/* jshint esversion: 6 */

var path = require("path");
var fs = require("fs");

var settings = require("./settings");

var db = require("./db_controller");
var invoice = require("./invoice_generator");
var menuinvoice = require("./invoice_generatormenu");
var sendEmail = require("./send_email")();
const axios = require("axios");
var CakeOrder = require("../models/cake-order");
var ContactUs = require("../models/contact-us");
var EventReservation = require("../models/event-reservation");
var productList = require("../models/product-list");
var TableReservation = require("../models/table-reservation");
var katalog = require("../models/katalog");
var moment = require("moment");
const { ObjectId } = require("mongodb"); // or ObjectID
var MenuOrder = require("../models/menu-order");
var OnlineMenu = require("../models/onlinemune-order");
const { createMollieClient } = require("@mollie/api-client");
const { data } = require("jquery");
const mollieClient = createMollieClient({ apiKey: settings.mollieApiKey });

var __api = {};

__api.getPDFFile = function (req, res) {
  console.log(
    `Calling getPDFFile API at ${getDateTimeNow()} for order ${req.query.orderId
    }  `
  );

  var checkOrderId = req.query.orderId;
  let internalOrder = req.query.internalOrder;
  console.log(" internalOrder ", internalOrder);
  let invoiceName = "";

  return new Promise((resolve, reject) => {
    let invoiceName = "invoice_" + checkOrderId + ".pdf";
    if (internalOrder == "true") {
      invoiceName = "shop_order_" + checkOrderId + ".pdf";
    } else if (internalOrder == "false") {
      invoiceName = "online_order_" + checkOrderId + ".pdf";
    }
    console.log(" invoiceName ======== ", invoiceName);
    fs.readFile(path.join(settings.invoicesHome, invoiceName), (err, data) => {
      if (err) {
        reject(err);
      } else {
        resolve(data);
      }
    });
  })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      console.log(" catch ======= ", err);
      // this code is for old record
      let invoiceName = "invoice_" + checkOrderId + ".pdf";
      return new Promise((resolve, reject) => {
        fs.readFile(
          path.join(settings.invoicesHome, invoiceName),
          (err, data) => {
            if (err) {
              reject(err);
            } else {
              resolve(data);
            }
          }
        );
      })
        .then((data) => {
          res.send(data);
        })
        .catch((err) => {
          console.log(" err ", err);
          throw err;
        });
    });
};

__api.getNextSequenceValue = function (req, res) {
  console.log(`Calling getNextSequenceValue API at ${getDateTimeNow()} `);

  db.connect(function (err) {
    if (err) {
      console.log("Unable to connect to DB!");
      res.sendStatus(510);
    } else {
      db.getNextSequenceValue("CakeOrder", function (e, obj) {
        if (e) {
          console.log(e);
          res.sendStatus(501);
        } else {
          res.send(obj);
          console.log(
            "Next no. in the cake order sequence is :",
            obj.value.sequence_value
          );
        }
      });
    }
  });
};

__api.getNextSequenceValueForMenueOrder = function (req, res) {
  console.log(
    `Calling getNextSequenceValueForMenueOrder API at ${getDateTimeNow()} for request ${req.body
    } `
  );

  db.connect(function (err) {
    if (err) {
      console.log("Unable to connect to DB!");
      res.sendStatus(510);
    } else {
      console.log("Connected successfully");
      db.getNextSequenceValue("onlineOrder", function (e, obj) {
        if (e) {
          console.log(e);
          res.sendStatus(501);
        } else {
          console.log(" getNextSequenceValueForMenueOrder ", obj);
          res.send(obj);
        }
      });
    }
  });
};

__api.getshapecount = function (req, res) {
  console.log(
    `Calling getshapecount API on ${req.query.preparationDate
    } at ${getDateTimeNow()} `
  );

  var checkdate = req.query.preparationDate;
  db.connect(function (err) {
    if (err) {
      console.log(err);
      res.sendStatus(510);
    } else {
      var collection = db.get().collection("CakeOrder");
      console.log("Connected successfully");
      collection
        .aggregate([
          {
            $match: {
              catalogNumber: { $eq: null },
              preparationDate: { $eq: checkdate },
              status: { $ne: "CANC" },
            },
          },
          {
            $group: {
              _id: { shape: "$shape", size: "$size" },
              flavor: { $push: "$flavor" },
              extraFlavor: { $push: "$extraFlavor" },
              totalSizeCount: { $sum: 1 },
            },
          },
        ])
        .toArray(function (err, respo) {
          let respoArr = [];
          if (err) {
            console.log(err);
            return res.status(500).send({ message: err.message });
          }
          if (respo) {
            respoArr.push(respo);
          }
          res.send(respoArr);
          1;
        });
    }
  });
};

__api.getcakeshapecount = function (req, res) {
  console.log(
    "Calling getcakeshapecount API at",
    getDateTimeNow(),
    "for",
    +req.body
  );

  db.connect(function (err) {
    if (err) {
      console.log(err);
      res.sendStatus(510);
    } else {
      var collection = db.get().collection("CakeOrder");
      console.log("Connected successfully");
      collection
        .aggregate([
          { $match: { catalogNumber: { $eq: null }, status: { $ne: "CANC" } } },
          {
            $group: {
              _id: { shape: "$shape", size: "$size" },
              totalSizeCount: { $sum: 1 },
            },
          },
        ])
        .toArray(function (err, respo) {
          let respoArr = [];
          if (err) {
            console.log(err);
            return res.status(500).send({ message: err.message });
          }
          if (respo) {
            respoArr.push(respo);
          }
          res.send(respoArr);
        });
    }
  });
};

__api.getPaymentTypeCount = function (req, res) {
  console.log(
    "Calling getPaymentTypeCount API at ",
    getDateTimeNow(),
    "for request",
    +req.body
  );

  db.connect(function (err) {
    if (err) {
      console.log(err);
      res.sendStatus(510);
    } else {
      var collection = db.get().collection("CakeOrder");
      console.log("Connected successfully");
      collection
        .aggregate([
          {
            $match: {
              status: { $nin: ["CANC", "canceled", "expired"] },
            },
          },
          {
            $group: {
              _id: {
                year: { $year: "$createdDate" },
                month: { $month: "$createdDate" },
                day: { $dayOfMonth: "$createdDate" },
                time: {
                  $dateToString: {
                    format: "%H:%M:%S:%L",
                    date: "$createdDate",
                  },
                },
                type: "$paymentType",
              },
              totalpriceCount: { $sum: "$price" },
              totalSizeCount: { $sum: 1 },
            },
          },
        ])
        .toArray(function (err, respo) {
          let respoArr = [];
          if (err) {
            console.log(err);
            return res.status(500).send({ message: err.message });
          }
          if (respo) {
            respoArr.push(respo);
          }
          res.send(respoArr);
        });
    }
  });
};

// This API is being used by pie chart diagram for payment type
__api.getPaymentTypeInExCount = function (req, res) {
  console.log(
    "Calling getPaymentTypeInExCount API at",
    getDateTimeNow(),
    "for request ",
    req.body
  );

  var date_from = req.query.fromDate;
  var date_to = req.query.toDate;
  var dateFrom = date_from ? moment(new Date(date_from)).toDate() : null;
  var dateTo = date_to ? moment(new Date(date_to)).toDate() : null;

  db.connect(function (err) {
    if (err) {
      console.log(err);
      res.sendStatus(510);
    } else {
      var collection = db.get().collection("CakeOrder");
      console.log("Connected successfully");

      if (dateFrom && dateTo) {
        collection
          .aggregate([
            {
              $match: {
                $and: [
                  {
                    createdDate: {
                      $gte: dateFrom,
                    },
                  },
                  {
                    createdDate: {
                      $lte: dateTo,
                    },
                  },
                  {
                    status: { $nin: ["CANC", "canceled", "expired"] },
                  },
                ],
              },
            },
            {
              $group: {
                _id: {
                  type: "$paymentType",
                  ordertype: "$internalOrder",
                },
                totalSizeCount: { $sum: 1 },
              },
            },
          ])
          .toArray(function (err, respo) {
            let respoArr = [];
            if (err) {
              console.log(err);
              return res.status(500).send({ message: err.message });
            }
            if (respo) {
              respoArr.push(respo);
            }
            res.send(respoArr);
          });
      }
      else {
        collection
          .aggregate([
            {
              $match: {
                status: { $nin: ["CANC", "canceled", "expired"] },
              },
            },
            {
              $group: {
                _id: {
                  type: "$paymentType",
                  ordertype: "$internalOrder",
                },
                totalSizeCount: { $sum: 1 },
              },
            },
          ])
          .toArray(function (err, respo) {
            let respoArr = [];
            if (err) {
              console.log(err);
              return res.status(500).send({ message: err.message });
            }
            if (respo) {
              respoArr.push(respo);
            }
            res.send(respoArr);
          });
      }
    }
  });
};

//This API is being used by summarize-view component
__api.getFlavorCount = function (req, res) {
  console.log(
    `Calling getFlavorCount API on ${req.query.preparationDate
    } at ${getDateTimeNow()}`
  );

  var checkdate = req.query.preparationDate;
  db.connect(function (err) {
    if (err) {
      console.log(err);
      res.sendStatus(510);
    } else {
      var collection = db.get().collection("CakeOrder");
      console.log("Connected successfully");
      collection
        .aggregate([
          {
            $match: {
              preparationDate: { $eq: checkdate },
              status: { $ne: "CANC" },
            },
          },
          {
            $group: {
              _id: { shape: "$shape", size: "$size", flavor: "$flavor" },
              totalFlavorCount: { $sum: 1 },
              flavorCountOneData: { $push: { orderId: "$orderId", internalOrder: "$internalOrder" } }
            },
          },
          // {
          //     $group: {
          //         _id: {
          //             flavor: "$flavor"
          //         },
          //         totalFlavorCount: { '$sum': 1 },
          //         size: { $push: "$size" }
          //     }
          // }
        ])
        .toArray(function (err, respo) {
          let respoArr = [];
          if (err) {
            console.log(err);
            return res.status(500).send({ message: err.message });
          }
          if (respo) {
            respo.forEach((data) => {
              respoArr.push(data);
            });
          }
          res.send(respoArr);
        });
    }
  });
};

__api.getColorCount = function (req, res) {
  console.log(
    `Calling getColorCount API on ${req.query.preparationDate
    } at ${getDateTimeNow()}`
  );

  var checkdate = req.query.preparationDate;
  db.connect(function (err) {
    if (err) {
      console.log(err);
      res.sendStatus(510);
    } else {
      var collection = db.get().collection("CakeOrder");
      console.log("Connected successfully");
      collection
        .aggregate([
          {
            $match: {
              preparationDate: { $eq: checkdate },
              status: { $ne: "CANC" },
            },
          },
          {
            $group: {
              _id: { shape: "$shape", size: "$size", color: "$color" },
              totalColorCount: { $sum: 1 },
            },
          },
        ])
        .toArray(function (err, respo) {
          let respoArr = [];
          if (err) {
            console.log(err);
            return res.status(500).send({ message: err.message });
          }
          if (respo) {
            respo.forEach((data) => {
              respoArr.push(data);
            });
          }
          res.send(respoArr);
        });
    }
  });
};

__api.getPriceCount = function (req, res) {
  console.log(
    `Calling getPriceCount API at ${getDateTimeNow()} for request ${req.query.status
    } `
  );

  var checkstatus = req.query.status;

  db.connect(function (err) {
    if (err) {
      console.log(err);
      res.sendStatus(510);
    } else {
      var collection = db.get().collection("CakeOrder");

      if (checkstatus == "pricecount") {
        collection
          .aggregate([
            {
              $match: { status: { $ne: "CANC" } },
            },
            {
              $group: {
                _id: {
                  year: { $year: "$createdDate" },
                  month: { $month: "$createdDate" },
                },
                sum: { $sum: 1 },
              },
            },
          ])
          .toArray(function (err, respo) {
            let respoArr = [];
            if (err) {
              console.log(err);
              return res.status(500).send({ message: err.message });
            }
            if (respo) {
              respoArr.push(respo);
            }
            res.send(respoArr);
          });
      } else {
        collection
          .aggregate([
            {
              $match: { status: { $ne: "CANC" } },
            },
            {
              $group: {
                _id: {
                  year: { $year: "$createdDate" },
                  month: { $month: "$createdDate" },
                },
                sum: { $sum: "$price" },
              },
            },
          ])
          .toArray(function (err, respo) {
            let respoArr = [];
            if (err) {
              console.log(err);
              return res.status(500).send({ message: err.message });
            }
            if (respo) {
              respoArr.push(respo);
            }
            res.send(respoArr);
          });
      }
    }
  });
};

__api.getPartialAmountCount = function (req, res) {
  console.log(`Calling getPartialAmountCount API at ${getDateTimeNow()}`);
  var d = new Date();

  db.connect(function (err) {
    if (err) {
      console.log(err);
      res.sendStatus(510);
    } else {
      var collection = db.get().collection("CakeOrder");
      console.log("Connected successfully");
      collection
        .aggregate([
          {
            $addFields: {
              deliveryDate: {
                $dateFromString: {
                  dateString: "$deliveryDate",
                  format: "%d.%m.%Y",
                },
              },
            },
          },
          {
            $match: {
              balanceAmount: { $ne: null },
              partialAmount: { $ne: null },
              deliveryDate: { $gte: d },
              status: { $ne: "CANC" },
            },
          },
          {
            $group: {
              _id: {
                year: { $year: "$createdDate" },
                month: { $month: "$createdDate" },
                day: { $dayOfMonth: "$createdDate" },
                time: {
                  $dateToString: {
                    format: "%H:%M:%S:%L",
                    date: "$createdDate",
                  },
                },
                partialAmount: "$partialAmount",
                balanceAmount: "$balanceAmount",
              },
            },
          },
        ])
        .toArray(function (err, respo) {
          let respoArr = [];
          if (err) {
            console.log(err);
            return res.status(500).send({ message: err.message });
          }
          if (respo) {
            respoArr.push(respo);
          }
          res.send(respoArr);
        });
    }
  });
};

__api.getInternalExternalOrder = function (req, res) {
  console.log(`Calling getInternalExternalOrder API at ${getDateTimeNow()}`);

  var date_from = req.query.fromDate;
  var date_to = req.query.toDate;
  var dateFrom = date_from ? moment(new Date(date_from)).toDate() : null;
  var dateTo = date_to ? moment(new Date(date_to)).toDate() : null;

  db.connect(function (err) {
    if (err) {
      console.log(err);
      res.sendStatus(510);
    } else {
      var collection = db.get().collection("CakeOrder");
      console.log("Connected successfully");
      if (dateFrom && dateTo) {
        collection
          .aggregate([
            {
              $match: {
                $and: [
                  {
                    createdDate: {
                      $gte: dateFrom,
                    },
                  },
                  {
                    createdDate: {
                      $lte: dateTo,
                    },
                  },
                  {
                    status: { $ne: "CANC" },
                  },
                ],
              },
              // $match: {
              //   status: { $ne: "CANC" },
              // },
            },
            {
              $group: {
                _id: "$internalOrder",
                ordercount: { $sum: 1 },
              },
            },
          ])
          .toArray(function (err, respo) {
            let respoArr = [];
            if (err) {
              console.log(err);
              return res.status(500).send({ message: err.message });
            }
            if (respo) {
              respoArr.push(respo);
            }
            res.send(respoArr);
          });
      }
      else {
        collection
          .aggregate([
            {
              $match: {
                status: { $ne: "CANC" },
              },
            },
            {
              $group: {
                _id: "$internalOrder",
                ordercount: { $sum: 1 },
              },
            },
          ])
          .toArray(function (err, respo) {
            let respoArr = [];
            if (err) {
              console.log(err);
              return res.status(500).send({ message: err.message });
            }
            if (respo) {
              respoArr.push(respo);
            }
            res.send(respoArr);
          });
      }
    }
  });
};

__api.getEventReservationCount = function (req, res) {
  console.log(`Calling getEventReservationCount API at ${getDateTimeNow()} `);

  db.connect(function (err) {
    if (err) {
      console.log(err);
      res.sendStatus(510);
    } else {
      var collection = db.get().collection("EventReservation");
      console.log("Connected successfully");
      collection
        .aggregate([
          {
            $group: {
              _id: { shape: "$type" },
              totalEventCount: { $sum: 1 },
            },
          },
        ])
        .toArray(function (err, respo) {
          let respoArr = [];
          if (err) {
            console.log(err);
            return res.status(500).send({ message: err.message });
          }
          if (respo) {
            respoArr.push(respo);
          }
          res.send(respoArr);
        });
    }
  });
};

__api.getTableReservationCount = function (req, res) {
  console.log(`Calling getTableReservationCount API at ${getDateTimeNow()} `);

  db.connect(function (err) {
    if (err) {
      console.log(err);
      res.sendStatus(510);
    } else {
      var collection = db.get().collection("TableReservation");
      console.log("Connected successfully");
      collection
        .aggregate([
          {
            $group: {
              _id: {
                year: { $year: "$createdDate" },
                month: { $month: "$createdDate" },
              },
              totalTableCount: { $sum: 1 },
            },
          },
        ])
        .toArray(function (err, respo) {
          let respoArr = [];
          if (err) {
            console.log(err);
            return res.status(500).send({ message: err.message });
          }
          if (respo) {
            respoArr.push(respo);
          }
          res.send(respoArr);
        });
    }
  });
};

__api.getCampaignList = function (req, res) {
  console.log(
    `Calling getCampaignList API at ${getDateTimeNow()} for coupon ${req.body.example.cuponname
    } `
  );

  db.connect(function (err) {
    if (err) {
      console.log(err);
      res.sendStatus(510);
    } else {
      var collection = db.get().collection("Campaign");
      var todaysDateTime = moment(new Date()).toDate();
      collection
        .find({ Code: { $eq: req.body.example.cuponname } })
        .count()
        .then((count) => {
          if (count === 0) {
            return res.status(200).send({ message: `No Such Coupon Found!` });
          } else {
            collection
              .find({
                TotalCount: { $gt: 0 },
                Code: { $eq: req.body.example.cuponname },
              })
              .count()
              .then((count) => {
                if (count === 0) {
                  return res
                    .status(200)
                    .send({ message: `Coupon code finished!` });
                } else {
                  collection
                    .find({
                      ValidFrom: { $lte: todaysDateTime },
                      ValidTo: { $gte: todaysDateTime },
                      Code: { $eq: req.body.example.cuponname },
                      TotalCount: { $gt: 0 },
                    })
                    .toArray(function (err, respo) {
                      let respoArr = [];
                      if (err) {
                        console.log(err);
                        return res
                          .status(500)
                          .send({ message: `Coupon expired!` });
                      }
                      if (respo[0]) {
                        let minAmount = respo[0].MiniPrice;
                        if (minAmount > req.body.example.price) {
                          return res
                            .status(200)
                            .send({
                              message: `Price Amount for this should be greater`,
                            });
                        } else {
                          respoArr.push(respo);
                        }
                      } else {
                        return res
                          .status(200)
                          .send({ message: `Coupon expired!` });
                      }
                      res.send(respoArr);
                    });
                }
              });
          }
        });
    }
  });
};

__api.getCountForCampaign = function (req, res) {
  console.log(
    `Calling getCountForCampaign API at ${getDateTimeNow()} for coupon  ${req.body.cuponname
    } `
  );

  const cuponName = req.body.cuponname;
  db.connect(function (err) {
    if (err) {
      console.log(err);
      res.sendStatus(510);
    } else {
      var collection = db.get().collection("Campaign");
      console.log("Connected successfully Campaign");

      collection.update(
        { Code: cuponName, TotalCount: { $gt: 0 } },
        { $inc: { CurrentCount: 1, TotalCount: -1 } },
        function (err, results) {
          if (err) {
            console.log(err);
          }

          res.send(results);
        }
      );
    }
  });
};

__api.getTotalCountWithPhoto = function (req, res) {
  console.log(`Calling getTotalCountWithPhoto API at ${getDateTimeNow()} `);

  var date = req.query.date.slice(1, -1);

  db.connect(function (err) {
    if (err) {
      console.log(err);
      res.sendStatus(510);
    } else {
      var collection = db.get().collection("CakeOrder");
      console.log("Connected successfully getTotalCountWithPhoto");
      if (date === "null") {
        collection
          .find({
            extraDecoration: { $eq: "EDECO01" },
            status: { $ne: "CANC" },
          })
          .count()
          .then((count) => {
            res.json({ count: count });
          });
      } else {
        collection
          .find({
            extraDecoration: { $eq: "EDECO01" },
            todaysDate: { $eq: date },
            status: { $ne: "CANC" },
          })
          .count()
          .then((count) => {
            res.json({ count: count });
          });
      }
    }
  });
};

__api.getTotalCountWithoutPhoto = function (req, res) {
  console.log("Calling getTotalCountWithoutPhoto API at ", getDateTimeNow());
  var date = req.query.date.slice(1, -1);

  db.connect(function (err) {
    if (err) {
      console.log(err);
      res.sendStatus(510);
    } else {
      var collection = db.get().collection("CakeOrder");
      console.log("Connected successfully getTotalCountWithoutPhoto");
      if (date === "null") {
        collection
          .find({
            extraDecoration: { $ne: "EDECO01" },
            status: { $ne: "CANC" },
          })
          .count()
          .then((count) => {
            res.json({ count: count });
          });
      } else {
        collection
          .find({
            extraDecoration: { $ne: "EDECO01" },
            todaysDate: { $eq: date },
            status: { $ne: "CANC" },
          })
          .count()
          .then((count) => {
            res.json({ count: count });
          });
      }
    }
  });
};

__api.getPastCakeOrder = function (req, res) {
  console.log(`Calling getPastCakeOrder API at ${getDateTimeNow()}`);

  var d = new Date();
  db.connect(function (err) {
    if (err) {
      console.log(err);
      res.sendStatus(510);
    } else {
      var collection = db.get().collection("CakeOrder");
      console.log("Connected successfully");
      collection
        .aggregate([
          {
            $addFields: {
              deliveryDate: {
                $dateFromString: {
                  dateString: "$deliveryDate",
                  format: "%d.%m.%Y",
                },
              },
            },
          },
          {
            $match: { deliveryDate: { $lte: d }, status: { $ne: "CANC" } },
          },
          {
            $group: {
              _id: {
                year: { $year: "$createdDate" },
                month: { $month: "$createdDate" },
                day: { $dayOfMonth: "$createdDate" },
                time: {
                  $dateToString: {
                    format: "%H:%M:%S:%L",
                    date: "$createdDate",
                  },
                },
              },
            },
          },
        ])
        .toArray(function (err, respo) {
          let respoArr = [];
          if (err) {
            console.log(err);
            return res.status(500).send({ message: err.message });
          }
          if (respo) {
            respoArr.push(respo);
          }
          res.send(respoArr);
        });
    }
  });
};

__api.getFutureCakeOrder = function (req, res) {
  console.log(`Calling getFutureCakeOrder API at ${getDateTimeNow()}`);
  var d = new Date();
  db.connect(function (err) {
    if (err) {
      console.log(err);
      res.sendStatus(510);
    } else {
      var collection = db.get().collection("CakeOrder");
      console.log("Connected successfully");
      collection
        .aggregate([
          {
            $addFields: {
              deliveryDate: {
                $dateFromString: {
                  dateString: "$deliveryDate",
                  format: "%d.%m.%Y",
                },
              },
            },
          },
          {
            $match: { deliveryDate: { $gt: d }, status: { $ne: "CANC" } },
          },
          {
            $group: {
              _id: {
                year: { $year: "$createdDate" },
                month: { $month: "$createdDate" },
                day: { $dayOfMonth: "$createdDate" },
                time: {
                  $dateToString: {
                    format: "%H:%M:%S:%L",
                    date: "$createdDate",
                  },
                },
              },
            },
          },
        ])
        .toArray(function (err, respo) {
          let respoArr = [];
          if (err) {
            console.log(err);
            return res.status(500).send({ message: err.message });
          }
          if (respo) {
            respoArr.push(respo);
          }
          res.send(respoArr);
        });
    }
  });
};

__api.getCountWithPhoto = function (req, res) {
  console.log(
    `Calling getCountWithPhoto API on ${req.query.preparationDate
    } at ${getDateTimeNow()} `
  );

  var checkdate = req.query.preparationDate;

  db.connect(function (err) {
    if (err) {
      console.log(err);
      res.sendStatus(510);
    } else {
      var collection = db.get().collection("CakeOrder");
      console.log("Connected successfully");
      collection
        .find({
          $and: [
            { preparationDate: { $eq: checkdate } },
            { status: { $ne: "CANC" } },
            { extraDecoration: { $eq: "EDECO01" } },
          ],
        })
        .count()
        .then((count) => {
          res.json({ count: count });
        });
    }
  });
};

__api.getCountWithoutPhoto = function (req, res) {
  console.log(
    `Calling getCountWithoutPhoto API at ${getDateTimeNow()} on  ${req.query.preparationDate
    } `
  );

  var checkdate = req.query.preparationDate;

  db.connect(function (err) {
    if (err) {
      console.log(err);
      res.sendStatus(510);
    } else {
      var collection = db.get().collection("CakeOrder");
      console.log("Connected successfully");
      collection
        .find({
          $and: [
            { preparationDate: { $eq: checkdate } },
            { status: { $ne: "CANC" } },
            { extraDecoration: { $ne: "EDECO01" } },
          ],
        })
        .count()
        .then((count) => {
          res.json({ count: count });
        });
    }
  });
};

__api.getCakeOrderCounts = function (req, res) {
  console.log(
    `Calling getCakeOrderCounts API at ${getDateTimeNow()} for  ${req.query.preparationDate
    } `
  );
  var checkdate = req.query.preparationDate;

  db.connect(function (err) {
    if (err) {
      console.log(err);
      res.sendStatus(510);
    } else {
      var collection = db.get().collection("CakeOrder");
      console.log("Connected successfully");
      collection
        .find({
          $and: [
            { preparationDate: { $eq: checkdate } },
            { status: { $ne: "CANC" } },
          ],
        })
        .count()
        .then((count) => {
          res.json({ count: count });
        });
    }
  });
};

__api.getCakeOrderCountsByDate = function (req, res) {
  console.log(
    `Calling getCakeOrderCountsByDate API at ${getDateTimeNow()} for  ${req.query.filterDate
    } `
  );
  var checkdate = req.query.filterDate;

  db.connect(function (err) {
    if (err) {
      console.log(err);
      res.sendStatus(510);
    } else {
      let countsData = {};
      var collection = db.get().collection("CakeOrder");
      console.log("Connected successfully");
      collection
        .find({
          $and: [
            { deliveryDate: { $eq: checkdate } },
            { status: { $eq: "ORDR" } },
          ],
        })
        .toArray(function (err, respo) {
          if (err) {
            console.log(err);
            return res.status(500).send({ message: `No data found` });
          } else {
            for (let i = 8; i < 24; i++) {
              let count = 0;
              countsData[i] = 0;
              let startTime = moment(`${checkdate} ${i}:00`, "DD.MM.YYYY H:mm");
              let endTime = moment(`${checkdate} ${i}:59`, "DD.MM.YYYY H:mm");
              for (const iterator of respo) {
                let deliveryDateTimeStr = `${checkdate} ${iterator.deliveryTime}`;
                let deliveryDateTime = moment(
                  deliveryDateTimeStr,
                  "DD.MM.YYYY H:mm"
                );
                if (
                  deliveryDateTime.isBetween(
                    startTime,
                    endTime,
                    undefined,
                    "[]"
                  )
                ) {
                  count++;
                }
              }
              countsData[i] = count;
            }
            res.send(countsData);
          }
        });
    }
  });
};

function escapeRegex(text) {
  return text.replace(/[^A-Za-z0-9_]/g, "\\$&");
}

__api.getCakeOrders = function (req, res) {
  console.log(`Calling getCakeOrders API at ${getDateTimeNow()} `);

  var pageNr = parseInt(req.query.pageNumber);
  var pageSize = parseInt(req.query.pageSize);
  var sortdata = req.query.sortOrder;
  var sortactive = req.query.sortActive;
  var searchAll = req.query.filter.trim();
  var skips = pageSize * (pageNr + 1 - 1);
  var sortorder;

  var dataList = req.query.dataList;

  if (sortdata === "asc") {
    sortorder = 1;
  } else {
    sortorder = -1;
  }

  db.connect(function (err) {
    if (err) {
      console.log(err);
      res.sendStatus(510);
    } else {
      var today = new Date();
      var dd = String(today.getDate()).padStart(2, "0");
      var mm = String(today.getMonth() + 1).padStart(2, "0"); //January is 0!
      var yyyy = today.getFullYear();
      var mydate = yyyy + "-" + mm + "-" + dd + "T00:00:00.000Z";
      var d = new Date(mydate);
      var collection = db.get().collection("CakeOrder");
      console.log("Connected successfully");
      if (searchAll) {
        const regex = new RegExp(escapeRegex(searchAll), "gi");
        if (
          searchAll === "clse" ||
          searchAll === "CLSE" ||
          searchAll === "PAYL" ||
          searchAll === "payl"
        ) {
          matchCondition = {
            $match: {
              $and: [
                {
                  $or: [
                    { orderId: parseInt(searchAll) },
                    { name: regex },
                    { email: regex },
                    { mobile: regex },
                    { deliveryDate: regex },
                    { deliveryTime: regex },
                    { size: regex },
                    { shape: regex },
                    { todaysDate: regex },
                    { status: regex },
                    { price: regex },
                    { catalogNumber: regex },
                    { qty: regex },
                    { deliveryType: regex },
                    { employee: regex },
                  ],
                },
                // { 'deliveryDateCustome': { $gte: d } },
                { status: { $ne: "CANC" } },
                { status: { $ne: "DELV" } },
              ],
            },
          };
        } else {
          matchCondition = {
            $match: {
              $and: [
                {
                  $or: [
                    { orderId: parseInt(searchAll) },
                    { name: regex },
                    { email: regex },
                    { mobile: regex },
                    { deliveryDate: regex },
                    { deliveryTime: regex },
                    { size: regex },
                    { shape: regex },
                    { todaysDate: regex },
                    { status: regex },
                    { price: regex },
                    { catalogNumber: regex },
                    { qty: regex },
                    { deliveryType: regex },
                    { employee: regex },
                  ],
                },
                { deliveryDateCustome: { $gte: d } },
                { status: { $ne: "CANC" } },
                { status: { $ne: "DELV" } },
              ],
            },
          };
        }
        collection
          .aggregate(
            [
              {
                $addFields: {
                  deliveryDateCustome: {
                    $dateFromString: {
                      dateString: "$deliveryDate",
                      format: "%d.%m.%Y",
                    },
                  },
                },
              },
              {
                $addFields: {
                  diff_msecs: { $subtract: ["$deliveryDateCustome", d] },
                },
              },
              {
                $addFields: {
                  diff_days: { $divide: ["$diff_msecs", 1000 * 60 * 60 * 24] },
                },
              },
              {
                $project: {
                  orderId: 1,
                  name: 1,
                  email: 1,
                  mobile: 1,
                  deliveryDate: 1,
                  deliveryTime: 1,
                  shape: 1,
                  size: 1,
                  price: 1,
                  internalOrder: 1,
                  catalogNumber: 1,
                  qty: 1,
                  status: 1,
                  preparationDate: 1,
                  todaysDate: 1,
                  urgent: 1,
                  deliveryDateCustome: 1,
                  diff_days: 1,
                  deliveryType: 1,
                  employee: 1,
                  createdDate: 1,
                },
              },
              matchCondition,
              // sortOperator,
              { $skip: skips },
              { $limit: pageSize },
            ],
            { allowDiskUse: true }
          )
          .toArray(function (err, respo) {
            let respoArr = [];
            if (err) {
              return res.status(500).send({ message: err.message });
            }
            if (respo) {
              respoArr.push(respo);
            }
            res.send(respoArr);
          });
      } else {
        var matchCondition = {};
        matchCondition = {
          $match: {
            $and: [
              {},
              { deliveryDateCustome: { $gte: d } },
              { status: { $ne: "CANC" } },
              { status: { $ne: "DELV" } },
            ],
          },
        };

        if (sortactive === "preparationDate") {
          sortactive = "preParationDateCustome";
          var sortOperator = { $sort: {} },
            sort = sortactive;
          sortOperator["$sort"][sort] = sortorder;
        } else if (sortactive === "deliveryDate") {
          sortactive = "deliveryDateCustome";
          var sortOperator = { $sort: {} },
            sort = sortactive;
          sortOperator["$sort"][sort] = sortorder;
        } else {
          var sortOperator = { $sort: {} },
            sort = sortactive;
          sortOperator["$sort"][sort] = sortorder;
        }

        collection
          .aggregate(
            [
              {
                $addFields: {
                  deliveryDateCustome: {
                    $dateFromString: {
                      dateString: "$deliveryDate",
                      format: "%d.%m.%Y",
                    },
                  },
                },
              },
              {
                $addFields: {
                  diff_msecs: { $subtract: ["$deliveryDateCustome", d] },
                },
              },
              {
                $addFields: {
                  diff_days: { $divide: ["$diff_msecs", 1000 * 60 * 60 * 24] },
                },
              },
              {
                $addFields: {
                  preParationDateCustome: {
                    $dateFromString: {
                      dateString: "$preparationDate",
                      format: "%d.%m.%Y",
                    },
                  },
                },
              },
              {
                $project: {
                  orderId: 1,
                  name: 1,
                  email: 1,
                  mobile: 1,
                  deliveryDate: 1,
                  deliveryTime: 1,
                  shape: 1,
                  size: 1,
                  price: 1,
                  internalOrder: 1,
                  catalogNumber: 1,
                  qty: 1,
                  status: 1,
                  preparationDate: 1,
                  todaysDate: 1,
                  urgent: 1,
                  deliveryDateCustome: 1,
                  diff_days: 1,
                  deliveryType: 1,
                  employee: 1,
                  createdDate: 1,
                },
              },
              matchCondition,
              sortOperator,
              { $skip: skips },
              { $limit: pageSize },
            ],
            { allowDiskUse: true }
          )
          .toArray(function (err, respo) {
            let respoArr = [];
            if (err) {
              return res.status(500).send({ message: err.message });
            }
            if (respo) {
              respoArr.push(respo);
            }
            res.send(respoArr);
          });
      }
    }
  });
};

__api.getTotalForTableListing = function (req, res) {
  console.log(`Calling getTotalForTableListing API at ${getDateTimeNow()}`);
  db.connect(function (err) {
    if (err) {
      console.log(err);
      res.sendStatus(510);
    } else {
      var today = new Date();
      var dd = String(today.getDate()).padStart(2, "0");
      var mm = String(today.getMonth() + 1).padStart(2, "0"); //January is 0!
      var yyyy = today.getFullYear();
      var mydate = yyyy + "-" + mm + "-" + dd + "T00:00:00.000Z";
      var d = new Date(mydate);

      var collection = db.get().collection("CakeOrder");
      var searchAll = req.query.filter.trim();
      const regex = new RegExp(escapeRegex(searchAll), "gi");
      var matchCondition = {};
      if (searchAll) {
        if (
          searchAll === "clse" ||
          searchAll === "CLSE" ||
          searchAll === "PAYL" ||
          searchAll === "payl"
        ) {
          matchCondition = {
            $match: {
              $and: [
                {
                  $or: [
                    { orderId: parseInt(searchAll) },
                    { name: regex },
                    { email: regex },
                    { mobile: regex },
                    { deliveryDate: regex },
                    { deliveryTime: regex },
                    { size: regex },
                    { shape: regex },
                    { todaysDate: regex },
                    { status: regex },
                    { price: regex },
                    { catalogNumber: regex },
                    { qty: regex },
                  ],
                },

                { status: { $ne: "CANC" } },
                { status: { $ne: "DELV" } },
              ],
            },
          };
        } else {
          matchCondition = {
            $match: {
              $and: [
                {
                  $or: [
                    { orderId: parseInt(searchAll) },
                    { name: regex },
                    { email: regex },
                    { mobile: regex },
                    { deliveryDate: regex },
                    { deliveryTime: regex },
                    { size: regex },
                    { shape: regex },
                    { todaysDate: regex },
                    { status: regex },
                    { price: regex },
                    { catalogNumber: regex },
                    { qty: regex },
                  ],
                },
                { deliveryDateCustome: { $gte: d } },
                { status: { $ne: "CANC" } },
                { status: { $ne: "DELV" } },
              ],
            },
          };
        }
      } else {
        matchCondition = {
          $match: {
            $and: [
              {},
              { deliveryDateCustome: { $gte: d } },
              { status: { $ne: "CANC" } },
              { status: { $ne: "DELV" } },
            ],
          },
        };
      }

      collection
        .aggregate(
          [
            {
              $addFields: {
                deliveryDateCustome: {
                  $dateFromString: {
                    dateString: "$deliveryDate",
                    format: "%d.%m.%Y",
                  },
                },
              },
            },
            matchCondition,
            { $group: { _id: null, recordCount: { $sum: 1 } } },
          ],
          { allowDiskUse: true }
        )
        .toArray(function (err, respo) {
          let respoArr = [];
          if (err) {
            console.log(err);
            return res.status(500).send({ message: err.message });
          }
          if (respo) {
            if (respo == "") {
              res.send([{ _id: null, recordCount: 0 }]);
            } else {
              res.send(respo);
            }
          }
        });
    }
  });
};

__api.getCakeOrdersForBaker = function (req, res) {
  console.log(`Calling getCakeOrdersForBaker API at ${getDateTimeNow()} `);
  var pageNr = parseInt(req.query.pageNumber);
  var pageSize = parseInt(req.query.pageSize);
  var sortdata = req.query.sortOrder;
  var sortactive = req.query.sortActive;
  var searchAll = req.query.filter;
  var skips = pageSize * (pageNr + 1 - 1);
  var sortorder;
  var filterByFlavor = req.query.filterByFlavor;
  var filterByShape = req.query.filterByShape;
  var filterBySize = req.query.filterBySize;
  var filterByFlvrShpSize = false;

  var list = productList.getProductList();
  for (let index in list.shapes) {
    if (searchAll === list.shapes[index].name) {
      searchAll = lists.shapes[index].code;
    }
  }
  for (let index in list.flavors) {
    if (searchAll === list.flavors[index].name) {
      searchAll = list.flavors[index].code;
    }
  }
  for (let index in list.colors) {
    if (searchAll === list.colors[index].name) {
      searchAll = list.colors[index].code;
    }
  }
  for (let index in list.decorations) {
    if (searchAll === list.decorations[index].name) {
      searchAll = list.decorations[index].code;
    }
  }
  for (let index in list.extraDecorations) {
    if (searchAll === list.extraDecorations[index].name) {
      searchAll = list.extraDecorations[index].code;
    }
  }
  for (let index in list.extraFlavor) {
    if (searchAll === list.extraFlavor[index].name) {
      searchAll = list.extraFlavor[index].code;
    }
  }

  if (sortdata === "asc") {
    sortorder = 1;
  } else {
    sortorder = -1;
  }

  if (filterByFlavor && filterByShape && filterBySize && searchAll) {
    filterByFlvrShpSize = true;
  }

  db.connect(function (err) {
    if (err) {
      console.log(err);
      res.sendStatus(510);
    } else {
      var today = new Date();
      var dd = String(today.getDate()).padStart(2, "0");
      var mm = String(today.getMonth() + 1).padStart(2, "0"); //January is 0!
      var yyyy = today.getFullYear();
      var mydate = yyyy + "-" + mm + "-" + dd + "T00:00:00.000Z";
      var d = new Date(mydate);
      var collection = db.get().collection("CakeOrder");
      console.log("Connected successfully");
      if (filterByFlvrShpSize) {
        const regex = new RegExp(escapeRegex(searchAll), "gi");
        matchCondition = {
          $match: {
            $and: [
              {
                $and: [
                  { preparationDate: regex },
                  { size: filterBySize },
                  { shape: filterByShape },
                  { flavor: filterByFlavor },
                ],
              },
              { deliveryDateCustome: { $gte: d } },
              { status: { $ne: "CANC" } },
              { status: { $ne: "DELV" } },
            ],
          },
        };
        collection
          .aggregate(
            [
              {
                $addFields: {
                  deliveryDateCustome: {
                    $dateFromString: {
                      dateString: "$deliveryDate",
                      format: "%d.%m.%Y",
                    },
                  },
                },
              },
              {
                $addFields: {
                  diff_msecs: {
                    $subtract: ["$deliveryDateCustome", "$createdDate"],
                  },
                },
              },
              {
                $addFields: {
                  diff_days: { $divide: ["$diff_msecs", 1000 * 60 * 60 * 24] },
                },
              },
              matchCondition,
              // sortOperator,
              { $skip: skips },
              { $limit: pageSize },
            ],
            { allowDiskUse: true }
          )
          .toArray(function (err, respo) {
            let respoArr = [];
            if (err) {
              console.log(err);
              return res.status(500).send({ message: err.message });
            }
            if (respo) {
              respoArr.push(respo);
            }
            res.send(respoArr);
          });
      } else if (searchAll) {
        const regex = new RegExp(escapeRegex(searchAll), "gi");
        matchCondition = {
          $match: {
            $and: [
              {
                $or: [
                  { orderId: parseInt(searchAll) },
                  { deliveryDate: regex },
                  { preparationDate: regex },
                  { deliveryTime: regex },
                  { size: regex },
                  { shape: regex },
                  { flavor: regex },
                  { extraFlavor: regex },
                  { color: regex },
                  { sauceColor: regex },
                  { decoration: regex },
                  { extraDecoration: regex },
                  { catalogNumber: regex },
                  { qty: regex },
                  { deliveryType: regex },
                  { employee: regex },
                ],
              },
              { deliveryDateCustome: { $gte: d } },
              { status: { $ne: "CANC" } },
              { status: { $ne: "DELV" } },
            ],
          },
        };
        collection
          .aggregate(
            [
              {
                $addFields: {
                  deliveryDateCustome: {
                    $dateFromString: {
                      dateString: "$deliveryDate",
                      format: "%d.%m.%Y",
                    },
                  },
                },
              },
              {
                $addFields: {
                  diff_msecs: {
                    $subtract: ["$deliveryDateCustome", "$createdDate"],
                  },
                },
              },
              {
                $addFields: {
                  diff_days: { $divide: ["$diff_msecs", 1000 * 60 * 60 * 24] },
                },
              },
              matchCondition,
              // sortOperator,
              { $skip: skips },
              { $limit: pageSize },
            ],
            { allowDiskUse: true }
          )
          .toArray(function (err, respo) {
            let respoArr = [];
            if (err) {
              console.log(err);
              return res.status(500).send({ message: err.message });
            }
            if (respo) {
              respoArr.push(respo);
            }
            res.send(respoArr);
          });
      } else {
        var matchCondition = {};
        matchCondition = {
          $match: {
            $and: [
              {
                $or: [{ status: { $eq: "ORDR" } }, { status: { $eq: "PROG" } }],
              },
              { deliveryDateCustome: { $gte: d } },
              { status: { $ne: "CANC" } },
              { status: { $ne: "DELV" } },
            ],
          },
        };
        if (sortactive === "preparationDate") {
          collection
            .aggregate(
              [
                {
                  $addFields: {
                    deliveryDateCustome: {
                      $dateFromString: {
                        dateString: "$deliveryDate",
                        format: "%d.%m.%Y",
                      },
                    },
                  },
                },
                {
                  $addFields: {
                    diff_msecs: {
                      $subtract: ["$deliveryDateCustome", "$createdDate"],
                    },
                  },
                },
                {
                  $addFields: {
                    diff_days: {
                      $divide: ["$diff_msecs", 1000 * 60 * 60 * 24],
                    },
                  },
                },
                {
                  $addFields: {
                    preParationDateCustome: {
                      $dateFromString: {
                        dateString: "$preparationDate",
                        format: "%d.%m.%Y",
                      },
                    },
                  },
                },
                matchCondition,
                {
                  $project: {
                    orderId: 1,
                    preparationDate: 1,
                    preParationDateCustome: 1,
                    deliveryDate: 1,
                    deliveryDateCustome: 1,
                    deliveryTime: 1,
                    size: 1,
                    shape: 1,
                    catalogNumber: 1,
                    flavor: 1,
                    extraFlavor: 1,
                    color: 1,
                    sauceColor: 1,
                    decoColor: 1,
                    extraDecoration: 1,
                    decoration: 1,
                    qty: 1,
                    status: 1,
                    todaysDate: 1,
                    urgent: 1,
                    diff_days: 1,
                    paymentType: 1,
                    createdDate: 1,
                    internalOrder: 1,
                    deliveryType: 1,
                    employee: 1,
                  },
                },
                {
                  $sort: { preParationDateCustome: sortorder, deliveryTime: 1 },
                },
                { $skip: skips },
                { $limit: pageSize },
              ],
              { allowDiskUse: true }
            )
            .toArray(function (err, respo) {
              let respoArr = [];
              if (err) {
                console.log(err);
                return res.status(500).send({ message: err.message });
              }
              if (respo) {
                respoArr.push(respo);
              }
              res.send(respoArr);
            });
        } else {
          if (sortactive === "deliveryDate") {
            sortactive = "deliveryDateCustome";
            var sortOperator = { $sort: {} },
              sort = sortactive;
            sortOperator["$sort"][sort] = sortorder;
          } else {
            var sortOperator = { $sort: {} },
              sort = sortactive;
            sortOperator["$sort"][sort] = sortorder;
          }
          collection
            .aggregate(
              [
                {
                  $addFields: {
                    deliveryDateCustome: {
                      $dateFromString: {
                        dateString: "$deliveryDate",
                        format: "%d.%m.%Y",
                      },
                    },
                  },
                },
                {
                  $addFields: {
                    diff_msecs: {
                      $subtract: ["$deliveryDateCustome", "$createdDate"],
                    },
                  },
                },
                {
                  $addFields: {
                    diff_days: {
                      $divide: ["$diff_msecs", 1000 * 60 * 60 * 24],
                    },
                  },
                },
                {
                  $addFields: {
                    preParationDateCustome: {
                      $dateFromString: {
                        dateString: "$preparationDate",
                        format: "%d.%m.%Y",
                      },
                    },
                  },
                },
                {
                  $project: {
                    orderId: 1,
                    preparationDate: 1,
                    preParationDateCustome: 1,
                    deliveryDate: 1,
                    deliveryDateCustome: 1,
                    deliveryTime: 1,
                    size: 1,
                    shape: 1,
                    catalogNumber: 1,
                    flavor: 1,
                    extraFlavor: 1,
                    color: 1,
                    sauceColor: 1,
                    decoColor: 1,
                    extraDecoration: 1,
                    decoration: 1,
                    qty: 1,
                    status: 1,
                    todaysDate: 1,
                    urgent: 1,
                    diff_days: 1,
                    paymentType: 1,
                    createdDate: 1,
                    internalOrder: 1,
                    deliveryType: 1,
                    employee: 1,
                  },
                },
                matchCondition,
                sortOperator,
                { $skip: skips },
                { $limit: pageSize },
              ],
              { allowDiskUse: true }
            )
            .toArray(function (err, respo) {
              let respoArr = [];
              if (err) {
                console.log(err);
                return res.status(500).send({ message: err.message });
              }
              if (respo) {
                respoArr.push(respo);
              }
              res.send(respoArr);
            });
        }
      }
    }
  });
};

__api.getTotalForBakerList = function (req, res) {
  console.log(`Calling getTotalForBakerList API at ${getDateTimeNow()} `);

  db.connect(function (err) {
    if (err) {
      console.log(err);
      res.sendStatus(510);
    } else {
      var today = new Date();
      var dd = String(today.getDate()).padStart(2, "0");
      var mm = String(today.getMonth() + 1).padStart(2, "0"); //January is 0!
      var yyyy = today.getFullYear();
      var mydate = yyyy + "-" + mm + "-" + dd + "T00:00:00.000Z";
      var d = new Date(mydate);
      var collection = db.get().collection("CakeOrder");
      var searchAll = req.query.filter.trim();
      var filterByFlavor = req.query.filterByFlavor;
      var filterByShape = req.query.filterByShape;
      var filterBySize = req.query.filterBySize;
      var filterByFlvrShpSize = false;

      var list = productList.getProductList();
      for (let index in list.shapes) {
        if (searchAll === list.shapes[index].name) {
          searchAll = lists.shapes[index].code;
        }
      }
      for (let index in list.flavors) {
        if (searchAll === list.flavors[index].name) {
          searchAll = list.flavors[index].code;
        }
      }
      for (let index in list.colors) {
        if (searchAll === list.colors[index].name) {
          searchAll = list.colors[index].code;
        }
      }
      for (let index in list.decorations) {
        if (searchAll === list.decorations[index].name) {
          searchAll = list.decorations[index].code;
        }
      }
      for (let index in list.extraDecorations) {
        if (searchAll === list.extraDecorations[index].name) {
          searchAll = list.extraDecorations[index].code;
        }
      }
      for (let index in list.extraFlavor) {
        if (searchAll === list.extraFlavor[index].name) {
          searchAll = list.extraFlavor[index].code;
        }
      }
      if (filterByFlavor && filterByShape && filterBySize && searchAll) {
        filterByFlvrShpSize = true;
      }
      const regex = new RegExp(escapeRegex(searchAll), "gi");
      var matchCondition = {};
      if (filterByFlvrShpSize) {
        matchCondition = {
          $match: {
            $and: [
              {
                $and: [
                  { preparationDate: regex },
                  { size: filterBySize },
                  { shape: filterByShape },
                  { flavor: filterByFlavor },
                ],
              },
              { deliveryDateCustome: { $gte: d } },
              { status: { $ne: "CANC" } },
              { status: { $ne: "DELV" } },
            ],
          },
        };
      } else if (searchAll) {
        matchCondition = {
          $match: {
            $and: [
              {
                $or: [
                  { orderId: parseInt(searchAll) },
                  { deliveryDate: regex },
                  { preparationDate: regex },
                  { deliveryTime: regex },
                  { size: regex },
                  { shape: regex },
                  { flavor: regex },
                  { extraFlavor: regex },
                  { color: regex },
                  { sauceColor: regex },
                  { decoration: regex },
                  { extraDecoration: regex },
                  { catalogNumber: regex },
                  { qty: regex },
                ],
              },
              { deliveryDateCustome: { $gte: d } },
              { status: { $ne: "CANC" } },
              { status: { $ne: "DELV" } },
            ],
          },
        };
      } else {
        matchCondition = {
          $match: {
            $and: [
              {
                $or: [{ status: { $eq: "ORDR" } }, { status: { $eq: "PROG" } }],
              },
              { deliveryDateCustome: { $gte: d } },
              { status: { $ne: "CANC" } },
              { status: { $ne: "DELV" } },
            ],
          },
        };
      }

      collection
        .aggregate(
          [
            {
              $addFields: {
                deliveryDateCustome: {
                  $dateFromString: {
                    dateString: "$deliveryDate",
                    format: "%d.%m.%Y",
                  },
                },
              },
            },
            matchCondition,
            { $group: { _id: null, recordCount: { $sum: 1 } } },
          ],
          { allowDiskUse: true }
        )
        .toArray(function (err, respo) {
          let respoArr = [];
          if (err) {
            // console.log("message: err.message ", err.message);
            return res.status(500).send({ message: err.message });
          }
          if (respo) {
            if (respo == "") {
              res.send([{ _id: null, recordCount: 0 }]);
            } else {
              res.send(respo);
            }
          }
        });
    }
  });
};

__api.getCakeOrderByDates = function (req, res) {
  console.log(
    `Calling getCakeOrderByDates API at ${getDateTimeNow()} for preparation date ${req.query.preparationDate
    } `
  );

  var checkdate = req.query.preparationDate;
  db.connect(function (err) {
    if (err) {
      console.log(err);
      res.sendStatus(510);
    } else {
      var collection = db.get().collection("CakeOrder");
      console.log("Connected successfully getCakeOrderByDates");
      matchCondition = {
        $match: {
          $and: [
            { preparationDate: { $eq: checkdate } },
            { status: { $eq: "ORDR" } },
            // { 'status': { $ne: "DELV" } }
          ],
        },
      };
      collection
        .aggregate([
          matchCondition,
          {
            $project: {
              orderId: 1,
              preparationDate: 1,
              deliveryDate: 1,
              status: 1,
              price: 1,
              partialAmount: 1,
              paymentType: 1,
              internalOrder: 1,
              deliveryTime: 1,
            },
          },
          { $sort: { deliveryDate: 1, deliveryTime: 1 } },
        ])
        .toArray(function (err, respo) {
          let respoArr = [];
          if (err) {
            console.log(err);
            return res.status(500).send({ message: err.message });
          }
          if (respo) {
            respoArr.push(respo);
          }
          res.send(respoArr);
        });
    }
  });
};

__api.getByStatus = function (req, res) {
  console.log(
    `Calling getByStatus API at ${getDateTimeNow()} for preparation date ${req.query.preparationDate
    } with ${req.body.status} status`
  );
  var checkdate = req.query.preparationDate;
  var checkStatus = req.body.status;

  db.connect(function (err) {
    if (err) {
      console.log(err);
      res.sendStatus(510);
    } else {
      var collection = db.get().collection("CakeOrder");
      console.log("Connected successfully");
      collection
        .find(
          { preparationDate: checkdate, status: checkStatus },
          {
            projection: {
              orderId: 1,
              preparationDate: 1,
              deliveryDate: 1,
              status: 1,
              paymentType: 1,
              partialAmount: 1,
              price: 1,
              color: 1,
              internalOrder: 1
            },
          }
        )
        .toArray(function (err, respo) {
          let respoArr = [];
          if (err) {
            console.log(err);
            return res.status(500).send({ message: err.message });
          }
          if (respo) {
            respoArr.push(respo);
          }
          res.send(respoArr);
        });
    }
  });
};

__api.updateCakeOrderStatus = function (req, status) {
  console.log(
    `Calling updateCakeOrderStatus API at ${getDateTimeNow()} for updating orderId ${req.query.orderId
    } to ${req.body.status
    } status. \n Looks like something went wrong with respect to Paypal.`
  );

  var updatedStatus = req.body.status;

  db.connect(function (err) {
    if (err) {
      console.log(`Error: ${err}`);
    } else {
      var collection = db.get().collection("CakeOrder");
      console.log("Connected successfully");

      const orderId = req.query.orderId;

      collection.update(
        { orderId: parseInt(orderId) },
        { $set: { status: updatedStatus } },
        function (err, results) {
          if (err) {
            console.log(err);
          }
          console.log(`updated successfully order : ${orderId}`);
          status.send(results);
          // res.sendStatus(510);
        }
      );
    }
  });
};

__api.getCakeOrder = function (req, res) {
  console.log(
    `Calling getCakeOrder API at ${getDateTimeNow()} for orderId ${req.query.orderId
    } `
  );

  var orderId = req.query.orderId;
  var fetchImage = function (fileName) {
    if (fileName) {
      return new Promise((resolve, reject) => {
        fs.readFile(path.join(settings.photosHome, fileName), (err, data) => {
          if (err) {
            reject(err);
          } else {
            resolve(data);
          }
        });
      });
    } else {
      return new Promise((resolve, reject) => {
        resolve("success");
      });
    }
  };

  db.connect(function (err) {
    if (err) {
      console.log(err);
      res.sendStatus(510);
    } else {
      var collection = db.get().collection("CakeOrder");
      console.log("Connected successfully");
      collection
        .find({ orderId: parseInt(orderId) })
        .toArray(function (err, results) {
          if (err) {
            console.log(err);
            res.sendStatus(511);
          } else if (results.length === 0) {
            res.sendStatus(512);
          } else {
            if (results[0].photoFileName || results[0].musterPhotoFileName) {
              async function resolvePromise() {
                if (results[0].photoFileName) {
                  await fetchImage(results[0].photoFileName).then(function (
                    data
                  ) {
                    let buff = new Buffer(data);
                    let base64data = buff.toString("base64");
                    results[0].photo = `data:image/png;base64,${base64data}`;
                  });
                }
                if (results[0].musterPhotoFileName) {
                  await fetchImage(results[0].musterPhotoFileName).then(
                    function (data) {
                      let buff = new Buffer(data);
                      let base64data = buff.toString("base64");
                      results[0].musterPhoto = `data:image/png;base64,${base64data}`;
                    }
                  );
                }

                res.send(results[0]);
              }
              resolvePromise();
            } else {
              res.send(results[0]);
            }
          }
        });
    }
  });
};
/*********** check  */
__api.updateCakeOrders = function (req, data) {
  console.log(`Calling updateCakeOrders API at ${getDateTimeNow()}.`, req.body);

  var parseMusterPhoto = function (orderId, musterPhoto) {
    if (!musterPhoto.startsWith("data:image/")) {
      return null;
    }

    var musterMeta = musterPhoto.split(";base64,");
    var musterFileName = `Muster_${orderId}.${musterMeta[0].split("/")[1]}`;

    return {
      musterFileName: musterFileName,
      musterFilePath: path.join(settings.photosHome, musterFileName),
      musterRawData: musterMeta[1],
    };
  };

  var saveMusterPhoto = function (orderId, musterPhoto) {
    try {
      fs.mkdirSync(settings.photosHome);
    } catch (err) {
      if (err.code !== "EEXIST") throw err;
    }

    fs.writeFile(
      musterPhoto.musterFilePath,
      musterPhoto.musterRawData,
      { encoding: "base64" },

      function (err) {
        if (err) {
          console.log("Error saving muster photo for Order: " + orderId);
          console.log(err);
        }
      }
    );
  };

  var parsePhoto = function (orderId, photo) {
    if (!photo.startsWith("data:image/")) {
      return null;
    }

    var meta = photo.split(";base64,");
    var fileName = `${orderId}.${meta[0].split("/")[1]}`;

    return {
      fileName: fileName,
      filePath: path.join(settings.photosHome, fileName),
      rawData: meta[1],
    };
  };

  var savePhoto = function (orderId, photo) {
    try {
      fs.mkdirSync(settings.photosHome);
    } catch (err) {
      if (err.code !== "EEXIST") throw err;
    }

    fs.writeFile(
      photo.filePath,
      photo.rawData,
      { encoding: "base64" },
      function (err) {
        if (err) {
          console.log("Error saving photo for Order: " + orderId);
          console.log(err);
        }
      }
    );
  };

  db.connect(function (err) {
    if (err) {
      console.log(`There was an error: ${err}`);
      //res.sendStatus(510);
    } else {
      var collection = db.get().collection("CakeOrder");
      console.log("Connected successfully");
      const orderId = req.query.orderId;
      // Change order status when order status will be PAYL or CLSE
      var statusChange = data.req.body.status;
      if (statusChange == "CLSE" || statusChange == "PAYL") {
        statusChange = "ORDR";
      }
      var musterPhotoName = null;
      var photo = null;

      var musterPhotoName = "";
      var musterFileName = "";
      var musterFilePath = "";

      if (data.req.body.musterPhoto) {
        musterPhotoName = parseMusterPhoto(orderId, data.req.body.musterPhoto);
        musterFileName = musterPhotoName.musterFileName;
        musterFilePath = musterPhotoName.musterFilePath;
      }
      if (musterPhotoName) {
        saveMusterPhoto(orderId, musterPhotoName);
      }
      var photo_name = "";
      var photo_FilePath = "";

      if (data.req.body.photo) {
        photo = parsePhoto(orderId, data.req.body.photo);
        photo_name = photo.fileName;
        photo_FilePath = photo.filePath;
      }
      if (photo) {
        savePhoto(orderId, photo);
      }

      var updatedData = {
        name: data.req.body.name,
        email: data.req.body.email,
        mobile: data.req.body.mobile,
        deliveryDate: data.req.body.deliveryDate,
        deliveryTime: data.req.body.deliveryTime,
        shape: data.req.body.shape,
        size: data.req.body.size,
        color: data.req.body.color,
        sauceColor: data.req.body.sauceColor,
        dripColor: data.req.body.dripColor,
        decoColor: data.req.body.decoColor,
        decoration: data.req.body.decoration,
        extraDecoration: data.req.body.extraDecoration,
        flavor: data.req.body.flavor,
        extraFlavor: data.req.body.additional_flavor,
        text: data.req.body.text,
        remarks: data.req.body.remarks,
        price: data.req.body.price,
        taxAtDefaultRate: data.req.body.taxAtDefaultRate,
        taxAtCustomRate: data.req.body.taxAtCustomRate,
        photo: null,
        photoFileName: photo_name,
        photoFilePath: photo_FilePath,
        photo_name: photo_name,
        photoLaterChecked: data.req.body.photoLaterChecked,
        catalogNumber: data.req.body.catalogNumber,
        extraFoodPrice: data.req.body.extraFoodPrice,
        extraNonFoodPrice: data.req.body.extraNonFoodPrice,
        extraTaxes: data.req.body.extraTaxes,
        partialAmount: data.req.body.partialAmount,
        balanceAmount: data.req.body.balanceAmount,
        totalTax: data.req.body.totalTax,
        qty: data.req.body.qty,
        musterPhoto: null,
        musterPhoto_name: musterPhotoName,
        musterPhotoChecked: data.req.body.musterPhotoChecked,
        status: statusChange,
        musterPhotoFileName: musterFileName,
        musterPhotoFilePath: musterFilePath,
        // comment: data.req.body.comment,
        deliveryAddressPincod: data.req.body.deliveryAddressPincod,
        deliveryCharge: data.req.body.deliveryCharge,
        deliveryDistance: data.req.body.deliveryDistance,
        deliveryAddress: data.req.body.deliveryAddress,
        employee: data.req.body.employee,
        deliveryType: data.req.body.deliveryType,
        updatedBy: data.req.body.updatedBy,
      };
      invoice.calculateDates(updatedData);
      var preparationDate = updatedData.preparationDate;
      var pDate = preparationDate.split(".");
      preparationDate = pDate[2] + "-" + pDate[1] + "-" + pDate[0];
      const date1 = new Date(preparationDate);
      const date2 = new Date();
      const diffTime = Math.abs(date2 - date1);
      const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
      if (diffDays <= 2 && data.req.body.paymentType === "cod") {
        updatedData.urgent = "yes";
      } else {
        updatedData.urgent = "no";
      }
      collection.update(
        { orderId: parseInt(orderId) },
        {
          $set: updatedData,
        },
        function (err, results) {
          if (err) {
            console.log(err);
          } else {
            collection
              .find({ orderId: parseInt(orderId) })
              .toArray(function (err, resultsTosend) {
                // passing payment type as cash when status will be CLSE or PAYL because in invoice generator fetching payment detail on the basis of status for paypal but it will not be awailable
                if (
                  data.req.body.status == "CLSE" ||
                  data.req.body.status == "PAYL"
                ) {
                  resultsTosend[0].paymentType = "cod";
                }
                resultsTosend[0].updateRecord = "yes";
                resultsTosend[0].photo = data.req.body.photo;
                resultsTosend[0].musterPhoto = data.req.body.musterPhoto;

                if (resultsTosend[0].deliveryAddress) {
                  resultsTosend[0].addressTxt = "Lieferdatum";
                } else {
                  resultsTosend[0].addressTxt = "Abholdatum";
                }

                invoice
                  .generateInvoice(resultsTosend[0])
                  .then(function () {
                    console.log(
                      `Generated invoice for updated cake order at ${getDateTimeNow()}`
                    );
                    sendEmail.sendCustomCakeEmail(resultsTosend[0]);
                  })
                  .catch(function (err) {
                    console.log(err);
                  });
                data.send(resultsTosend);
              });
          }
        }
      );
    }
  });
};

__api.cancelFromCakeOrder = function (req, res) {
  console.log(
    `Calling cancelFromCakeOrder API at ${getDateTimeNow()}for order ${req.query.orderId
    } `
  );
  db.connect(function (err) {
    if (err) {
      console.log(`There was an error: ${err}`);
    } else {
      var collection = db.get().collection("CakeOrder");
      console.log("Connected successfully");

      const orderIdForUpdate = req.query.orderId;
      var updatedData = {
        status: req.body.status,
        orderCancelMsg: req.body.cancelOrderMsg,
      };

      console.log(
        "check mail or without mail:",
        req.body.cancelWithMailOrWithout
      );

      collection.update(
        { orderId: parseInt(orderIdForUpdate) },
        {
          $set: updatedData,
        },
        function (err, results) {
          if (err) {
            console.log(err);
          } else {
            collection
              .find({ orderId: parseInt(orderIdForUpdate) })
              .toArray(function (err, resultsTosend) {
                resultsTosend[0].paymentType = "cod";
                if (resultsTosend[0].deliveryAddress) {
                  resultsTosend[0].addressTxt = "Lieferdatum";
                } else {
                  resultsTosend[0].addressTxt = "Abholdatum";
                }

                invoice
                  .generateInvoice(resultsTosend[0])
                  .then(function () {
                    console.log(
                      "cancel order invoice generated date and time: ",
                      getDateTimeNow()
                    );
                    if (req.body.cancelWithMailOrWithout === "cancelWithMail") {
                      sendEmail.sendCustomCakeEmail(resultsTosend[0]);
                      console.log("updated successfully");
                      res.send(orderIdForUpdate);
                    } else if (
                      req.body.cancelWithMailOrWithout === "cancelWithoutMail"
                    ) {
                      console.log("updated successfully without mail");
                      res.send(orderIdForUpdate);
                    }
                  })
                  .catch(function (err) {
                    console.log(`Error occurred in sending email: ${err}`);
                    console.log("START ERROR /api/cakeOrder:generateInvoice()");
                    // console.log(err);
                    // console.log(cakeOrder);
                    console.log("END ERROR /api/cakeOrder:generateInvoice()");
                  });
              });
          }
        }
      );
    }
  });
};

__api.login = function (req, res) {
  console.log(`Calling login API at ${getDateTimeNow()} `);

  if (!req.body) {
    res.sendStatus(403);
    return;
  }

  var username = req.body.username;
  var password = req.body.password;
  if (!username || !password || username === "" || password === "") {
    res.sendStatus(510);
    return;
  }

  res.send({ loginResult: true, key: "hjshdjshdsdsdsmn797987" });
};

__api.isLoggedIn = function (key) {
  return true;
};

__api.contactUs = function (req, res) {
  console.log(
    `Calling contactUs API at ${getDateTimeNow()}, Name: ${req.body.contact_name
    }, Mobile:${req.body.contact_mobile}, email:${req.body.contact_email
    }, message: ${req.body.contact_message}`
  );

  db.connect(function (err) {
    if (err) {
      console.log(`Unable to connect to DB! : ${err}`);
      res.sendStatus(510);
    } else {
      var collection = db.get().collection("ContactUs");
      var contactUs = ContactUs();
      contactUs.name = req.body.contact_name;
      contactUs.email = req.body.contact_email;
      contactUs.message = req.body.contact_message;
      contactUs.mobile = req.body.contact_mobile;

      collection.insertOne(contactUs, function (err) {
        if (err) {
          res.send("Error finding collection");
          console.log(err);
        } else {
          sendEmail.sendConfirmationEmail(contactUs, res);
          res.send("Vielen Dank! Ihre Anfrage ist bei uns eingegangen.");
        }
      });
      db.close(function (err) {
        if (err) {
          console.log("Cant close connection", err);
        }
      });
    }
  });
};

__api.bookTable = function (req, res) {
  console.log(
    `Calling bookTable API at ${getDateTimeNow()}, Name:${req.body.guest_name
    },Email: ${req.body.guest_email}, Mobile:${req.body.guest_mobile}, Date:${req.body.date
    }, Time:${req.body.time}, Guests: ${req.body.guest_number}`
  );

  var newTableReservation = function (reservationId) {
    var tableReservation = TableReservation();

    tableReservation.reservationId = reservationId;
    tableReservation.guestName = req.body.guest_name;
    tableReservation.guestEmail = req.body.guest_email;
    tableReservation.guestMobile = req.body.guest_mobile;
    tableReservation.guestCount = Number.parseInt(req.body.guest_number, 10);
    tableReservation.date = req.body.date;
    tableReservation.time = req.body.time;
    tableReservation.newsletterConsent = req.body.newsletterConsent;
    return tableReservation;
  };

  var insertTableReservation = function (tableReservation) {
    var collection = db.get().collection("TableReservation");
    collection.insertOne(tableReservation, function (err) {
      if (err) {
        res.send("Error finding collection");
        console.log(err);
      } else {
        res.send(
          "Vielen Dank! Ihre Anfrage ist bei uns eingegangen. Wir melden uns umgehend bei Ihnen."
        );
        sendEmail.sendTableConfirmationEmail(tableReservation, res);
      }

      db.close(function (err) {
        if (err) {
          console.log("Cant close connection", err);
        }
      });
    });
  };

  var isBookingValid = function(){
    if(req.body.guest_name=="" || req.body.guest_email=="" || req.body.guest_mobile==""){
      return false;
    }
    if(req.body.guest_number<2 || req.body.date=="" || req.body.time==":"){
      return false;
    }
    return true;
  }

  db.connect(function (err) {
    if (err) {
      console.log("Unable to connect to DB!");
      res.sendStatus(510);
    } else {
      console.log("Connected successfully");
      var bookingValid = isBookingValid();
      if(bookingValid){
        db.getNextSequenceValue("TableReservation", function (err, obj) {
          if (err) {
            console.log(`Error: ${err}`);
            res.sendStatus(511);
          } else {
            var tableReservation = newTableReservation(obj.value.sequence_value);
            insertTableReservation(tableReservation);
          }
        });
      }
      else{
        res.status(400).send("Incomplete data");
      }
    }
  });
};

__api.bookEvent = function (req, res) {
  console.log(
    `Calling bookEvent API at ${getDateTimeNow()}, Name:${req.body.guest_fname
    }, Email: ${req.body.guest_femail}, Mobile:${req.body.guest_fmobile
    }, Date: ${req.body.date}, Event type:${req.body.event_type}, Event size:${req.body.event_size
    }, From: ${req.body.fromtime}, Duration:${req.body.event_duration} `
  );

  var newEventReservation = function (reservationId) {
    var eventReservation = EventReservation();

    eventReservation.reservationId = reservationId;
    eventReservation.guestName = req.body.guest_fname;
    eventReservation.guestEmail = req.body.guest_femail;
    eventReservation.guestMobile = req.body.guest_fmobile;
    eventReservation.type = req.body.event_type;
    eventReservation.size = req.body.event_size;
    eventReservation.date = req.body.date;
    eventReservation.startTime = req.body.fromtime;
    eventReservation.duration = req.body.event_duration;
    eventReservation.newsletterConsent = req.body.newsletterConsent;
    return eventReservation;
  };

  var insertEventReservation = function (eventReservation) {
    var collection = db.get().collection("EventReservation");
    collection.insertOne(eventReservation, function (err) {
      if (err) {
        res.send("Error finding collection");
        console.log(err);
      } else {
        res.send(
          "Vielen Dank! Ihre Anfrage ist bei uns eingegangen. Wir melden uns umgehend bei Ihnen."
        );
        sendEmail.sendEventConfirmationEmail(eventReservation, res);
      }

      db.close(function (err) {
        if (err) {
          console.log("Cant close connection", err);
        }
      });
    });
  };

  var isBookingValid = function(){
    if(req.body.guest_fname=="" || req.body.guest_femail=="" || req.body.guest_fmobile==""){
      return false;
    }
    if(req.body.event_size=="" || req.body.event_type=="" || req.body.event_duration==""){
      return false;
    }
    if(req.body.date=="" || req.body.fromtime==":"){
      return false;
    }
    return true;
  }

  db.connect(function (err) {
    if (err) {
      console.log("Unable to connect to DB!");
      res.sendStatus(510);
    } else {
      var bookingValid = isBookingValid();
      if(bookingValid){
        db.getNextSequenceValue("EventReservation", function (err, obj) {
          if (err) {
            console.log(`Error: ${err}`);
            res.sendStatus(511);
          } else {
            var eventReservation = newEventReservation(obj.value.sequence_value);
            insertEventReservation(eventReservation);
          }
        });
      }
      else{
        res.status(400).send("Incomplete data");
      }
    }
  });
};

__api.cakeOrder = function (req, res) {
  const { photo, url, musterPhoto, Musterurl, ...cakeOrderToPrint } = req.body;
  console.log(
    "Calling cakeOrder API at ",
    getDateTimeNow(),
    "Order details :",
    cakeOrderToPrint
  );

  if (req.body.deliveryDate === "NaN.NaN.NaN") {
    throw console.error("Erroneous date: exiting");
  }
  var newCakeOrder = function (orderId) {
    var cakeOrder = CakeOrder();
    cakeOrder.orderId = orderId;
    cakeOrder.name = req.body.fname + " " + req.body.lname;
    cakeOrder.email = req.body.email;
    cakeOrder.mobile = req.body.mobile;
    cakeOrder.deliveryDate = req.body.deliveryDate;
    cakeOrder.deliveryTime = req.body.deliveryTime;
    cakeOrder.street = req.body.street;
    cakeOrder.city = req.body.city;
    cakeOrder.shape = req.body.shape;
    cakeOrder.size = req.body.size;
    cakeOrder.color = req.body.color;
    cakeOrder.decoration = req.body.decoration;
    cakeOrder.extraDecoration = req.body.extra_decoration;
    cakeOrder.sauceColor = req.body.sauceColor;
    cakeOrder.dripColor = req.body.dripColor;
    cakeOrder.decoColor = req.body.decoColor;
    cakeOrder.flavor = req.body.flavor;
    cakeOrder.extraFlavor = req.body.additional_flavor;
    cakeOrder.text = req.body.text;
    cakeOrder.remarks = req.body.remarks;
    cakeOrder.price = req.body.price;
    cakeOrder.taxAtDefaultRate = req.body.taxAtDefaultRate;
    cakeOrder.taxAtCustomRate = req.body.taxAtCustomRate;
    cakeOrder.photo = req.body.photo;
    cakeOrder.photoPath = "";
    cakeOrder.paymentType = req.body.paymentType;
    cakeOrder.mollieTransactionId = req.body.mollieTransactionId;
    cakeOrder.paymentDetails = req.body.paymentDetails;
    cakeOrder.photoLaterChecked = req.body.photoLaterChecked;
    cakeOrder.catalogNumber = req.body.catalogNumber;
    cakeOrder.extraFoodPrice = req.body.extraFoodPrice;
    cakeOrder.extraNonFoodPrice = req.body.extraNonFoodPrice;
    cakeOrder.extraTaxes = req.body.extraTaxes;
    cakeOrder.partialAmount = req.body.partialAmount;
    cakeOrder.balanceAmount = req.body.balanceAmount;
    cakeOrder.totalTax = req.body.totalTax;
    cakeOrder.qty = req.body.qty;
    cakeOrder.cuponname = req.body.cuponname;
    cakeOrder.cuponPrice = req.body.cuponPrice;
    cakeOrder.musterPhoto = req.body.musterPhoto;
    cakeOrder.musterPhotoPath = "";
    cakeOrder.musterPhotoChecked = req.body.musterPhotoChecked;

    cakeOrder.deliveryAddressPincod = req.body.deliveryAddressPincod;
    cakeOrder.deliveryCharge = req.body.deliveryCharge;
    cakeOrder.deliveryDistance = req.body.deliveryDistance;
    cakeOrder.deliveryAddress = req.body.deliveryAddress;

    cakeOrder.employee = req.body.employee;

    cakeOrder.addressTxt = req.body.deliveryAddress
      ? "Lieferdatum"
      : "Abholdatum";
    cakeOrder.status =
      req.body.paymentType == "cod" || req.body.paymentType == "creditcard"
        ? "ORDR"
        : "PAYL";
    cakeOrder.deliveryType = req.body.pickupDeliverySelection
      ? "delivery"
      : "pickup";
    cakeOrder.internalOrder = req.body.internalOrder ? true : false;
    //console.log(`cakeOrder.addressTxt : ${cakeOrder.addressTxt }, cakeOrder.status: ${cakeOrder.status}, cakeOrder.deliveryType: ${cakeOrder.deliveryType}, cakeOrder.internalOrder:${cakeOrder.internalOrder} `);
    return cakeOrder;
  };

  var newCakeOrderForDB = function (orderId) {
    var cakeOrder = CakeOrder();
    cakeOrder.orderId = orderId;
    cakeOrder.name = req.body.fname + " " + req.body.lname;
    cakeOrder.email = req.body.email;
    cakeOrder.mobile = req.body.mobile;
    cakeOrder.deliveryDate = req.body.deliveryDate;
    cakeOrder.deliveryTime = req.body.deliveryTime;
    cakeOrder.street = req.body.street;
    cakeOrder.city = req.body.city;
    cakeOrder.shape = req.body.shape;
    cakeOrder.size = req.body.size;
    cakeOrder.color = req.body.color;
    cakeOrder.decoration = req.body.decoration;
    cakeOrder.extraDecoration = req.body.extra_decoration;
    cakeOrder.sauceColor = req.body.sauceColor;
    cakeOrder.dripColor = req.body.dripColor;
    cakeOrder.decoColor = req.body.decoColor;
    cakeOrder.flavor = req.body.flavor;
    cakeOrder.extraFlavor = req.body.additional_flavor;
    cakeOrder.text = req.body.text;
    cakeOrder.remarks = req.body.remarks;
    cakeOrder.price = req.body.price;
    cakeOrder.taxAtDefaultRate = req.body.taxAtDefaultRate;
    cakeOrder.taxAtCustomRate = req.body.taxAtCustomRate;
    cakeOrder.photo = null;
    cakeOrder.photoPath = "";
    cakeOrder.paymentType = req.body.paymentType;
    cakeOrder.paymentDetails = req.body.paymentDetails;
    cakeOrder.photoLaterChecked = req.body.photoLaterChecked;
    cakeOrder.catalogNumber = req.body.catalogNumber;
    cakeOrder.extraFoodPrice = req.body.extraFoodPrice;
    cakeOrder.extraNonFoodPrice = req.body.extraNonFoodPrice;
    cakeOrder.extraTaxes = req.body.extraTaxes;
    cakeOrder.partialAmount = req.body.partialAmount;
    cakeOrder.balanceAmount = req.body.balanceAmount;
    cakeOrder.totalTax = req.body.totalTax;
    cakeOrder.qty = req.body.qty;
    cakeOrder.cuponname = req.body.cuponname;
    cakeOrder.cuponPrice = req.body.cuponPrice;

    cakeOrder.musterPhoto = null;
    cakeOrder.musterPhotoPath = "";
    cakeOrder.musterPhotoChecked = req.body.musterPhotoChecked;

    cakeOrder.deliveryAddressPincod = req.body.deliveryAddressPincod;
    cakeOrder.deliveryCharge = req.body.deliveryCharge;
    cakeOrder.deliveryDistance = req.body.deliveryDistance;
    cakeOrder.deliveryAddress = req.body.deliveryAddress;

    cakeOrder.employee = req.body.employee;

    cakeOrder.status =
      req.body.paymentType == "cod" || req.body.paymentType == "creditcard"
        ? "ORDR"
        : "PAYL";
    cakeOrder.deliveryType = req.body.pickupDeliverySelection
      ? "delivery"
      : "pickup";
    cakeOrder.internalOrder = req.body.internalOrder ? true : false;

    // cakeOrder.comment = req.body.comment;
    return cakeOrder;
  };

  var parsePhoto = function (orderId, photo) {
    // Format: "data:image/png;base64,kdqgx6W+..."
    if (!photo.startsWith("data:image/")) {
      return null;
    }

    var meta = photo.split(";base64,");
    var fileName = `${orderId}.${meta[0].split("/")[1]}`;

    return {
      fileName: fileName,
      filePath: path.join(settings.photosHome, fileName),
      rawData: meta[1],
    };
  };

  var parseMusterPhoto = function (orderId, musterPhoto) {
    if (!musterPhoto.startsWith("data:image/")) {
      return null;
    }

    var musterMeta = musterPhoto.split(";base64,");
    var musterFileName = `Muster_${orderId}.${musterMeta[0].split("/")[1]}`;

    return {
      musterFileName: musterFileName,
      musterFilePath: path.join(settings.photosHome, musterFileName),
      musterRawData: musterMeta[1],
      // musterRawData: null
    };
  };

  var savePhoto = function (cakeOrder, photo) {
    try {
      fs.mkdirSync(settings.photosHome);
    } catch (err) {
      if (err.code !== "EEXIST") throw err;
    }

    fs.writeFile(
      photo.filePath,
      photo.rawData,
      { encoding: "base64" },
      function (err) {
        if (err) {
          console.log("Error saving photo for Order: " + cakeOrder.orderId);
          console.log(err);
        }
        if (
          cakeOrder.paymentType == "cod" ||
          cakeOrder.paymentType == "creditcard"
        ) {
          generateInvoice(cakeOrder);
        }
      }
    );
  };

  var saveMusterPhoto = function (cakeOrder, musterPhoto) {
    try {
      fs.mkdirSync(settings.photosHome);
    } catch (err) {
      if (err.code !== "EEXIST") throw err;
    }

    fs.writeFile(
      musterPhoto.musterFilePath,
      musterPhoto.musterRawData,
      { encoding: "base64" },
      // musterPhoto.musterRawData, null,
      function (err) {
        if (err) {
          console.log(
            `Error saving details for orderId ${cakeOrder.orderId} :: ${err}`
          );
        }
      }
    );
  };

  // Generate invoice and then send the confirmation email with invoice attached
  var generateInvoice = function (cakeOrder) {
    invoice
      .generateInvoice(cakeOrder)
      .then(function () {
        console.log(
          `invoice generated for ${cakeOrder.orderId} at ${getDateTimeNow()}`
        );
        if (
          cakeOrder.paymentType == "cod" ||
          cakeOrder.paymentType == "creditcard"
        ) {
          sendEmail.sendCustomCakeEmail(cakeOrder);
        }
      })
      .catch(function (err) {
        console.log("START ERROR /api/cakeOrder:generateInvoice()");
        console.log(err);
        console.log(cakeOrder);
        console.log("END ERROR /api/cakeOrder:generateInvoice()");
      });
  };

  var insertOrder = function (cakeOrder) {
    console.log("Inserting orderId ... ", cakeOrder.orderId);

    var collection = db.get().collection("CakeOrder");
    console.log(" collection =========== ", collection);
    collection.insertOne(cakeOrder, function (err) {
      if (err) {
        cakeOrder.subj =
          "Error while inserting into database with orderId " +
          cakeOrder.orderId;
        sendEmail.sendDbErrorMail(JSON.stringify(cakeOrder));
        res.send({
          error: true,
          orderId: cakeOrder.orderId,
          message: "Error inserting data",
        });
      } else {
        console.log(" cakeOrder.paymentDetails ");
        if (cakeOrder.paymentType == "mollie") {
          res.send(cakeOrder.paymentDetails);
        } else {
          res.send({
            error: false,
            orderId: cakeOrder.orderId,
            message:
              'Bei Bestellungen mit weniger als fünf Tage Vorlaufzeit nur "barzahlung" oder "paypal" möglich',
          });
        }
        db.close(function (err) {
          if (err) {
            console.log("Cant close connection", err);
          }
        });
      }
    });
  };

  var isOrderValid = function () {
    if (req.body.size == "" || req.body.shape == "" || req.body.decoration == "" || req.body.flavor == "") {
      return false;
    }
    if (req.body.deliveryTime == ":" || req.body.deliveryDate == "") {
      return false;
    }
    if(req.body.pickupDeliverySelection && (req.body.deliveryAddress=="" || req.body.deliveryAddressPincod=="")){
      return false;
    }
    if (req.body.fname == "" || req.body.lname == "" || req.body.email == "" || req.body.mobile == "") {
      return false;
    }
    if (req.body.price == null) {
      return false;
    }
    return true;
  }

  db.connect(function (err) {
    if (err) {
      console.log("Unable to connect to DB!.\nRequest:");
      console.log(newCakeOrder(-1));
      res.sendStatus(510);
    } else {
      var orderValid = isOrderValid();
      if (orderValid) {
        if (req.body.internalOrder) {
          db.getNextSequenceValue("CakeOrder", function (err, obj) {
            if (err) {
              console.log(`Error: ${err}`);
              res.sendStatus(511);
            } else {
              //console.log('Order from Admin --------------------')
              if (req.body.internalOrder) {
                var orderId =
                  new Date().getFullYear() * 100000 + obj.value.sequence_value; // 201800001 - 201899999
              } else {
                var orderId = req.body.orderId;
                // console.log('Order Id ', orderId)
              }
              var cakeOrder = newCakeOrder(orderId);
              var cakeOrderForDB = newCakeOrderForDB(orderId);

              invoice.calculateDates(cakeOrder);
              cakeOrderForDB.preparationDate = cakeOrder.preparationDate;
              cakeOrderForDB.todaysDate = cakeOrder.todaysDate;

              cakeOrder.urgent = "no";
              cakeOrderForDB.urgent = "no";

              var photo = null;
              var musterPhoto = null;
              if (cakeOrder.photo) {
                photo = parsePhoto(orderId, cakeOrder.photo);
              }

              if (cakeOrder.musterPhoto) {
                musterPhoto = parseMusterPhoto(orderId, cakeOrder.musterPhoto);
              }

              if (musterPhoto) {
                cakeOrder.musterPhotoFileName = musterPhoto.musterFileName;
                cakeOrder.musterPhotoFilePath = musterPhoto.musterFilePath;
                cakeOrderForDB.musterPhotoFileName = musterPhoto.musterFileName;
                cakeOrderForDB.musterPhotoFilePath = musterPhoto.musterFilePath;
                saveMusterPhoto(cakeOrder, musterPhoto);
              }

              if (photo) {
                cakeOrder.photoFileName = photo.fileName;
                cakeOrder.photoFilePath = photo.filePath;
                cakeOrderForDB.photoFileName = photo.fileName;
                cakeOrderForDB.photoFilePath = photo.filePath;
                savePhoto(cakeOrder, photo);
              } else {
                // console.log(' generateInvoice before -----------  ');
                if (cakeOrder.paymentType == "cod") {
                  generateInvoice(cakeOrder);
                }
              }
              insertOrder(cakeOrderForDB);
            }
          });
        } else {
          // Order from web --------------------
          var orderId = req.body.orderId;
          var collection = db.get().collection("CakeOrder");
          collection
            .find({ orderId: parseInt(orderId) })
            .toArray(async function (err, checkPreviousRecord) {
              var cakeOrder = newCakeOrder(orderId);
              var cakeOrderForDB = newCakeOrderForDB(orderId);
              // console.log(" checkPreviousRecord ", checkPreviousRecord);
              // Setting restbetrag(balanceAmount) if paymentType is cash or creditcard for differentiating this is unpaid order
              if (
                cakeOrder.paymentType === "cod" ||
                cakeOrder.paymentType === "creditcard"
              ) {
                cakeOrder.balanceAmount = cakeOrder.price;
              }

              invoice.calculateDates(cakeOrder);
              /// checking order is urgent or not on basis of prepartion date

              cakeOrderForDB.preparationDate = cakeOrder.preparationDate;
              cakeOrderForDB.todaysDate = cakeOrder.todaysDate;
              var preparationDate = cakeOrder.preparationDate;
              var pDate = preparationDate.split(".");
              preparationDate = pDate[2] + "-" + pDate[1] + "-" + pDate[0];
              const date1 = new Date(preparationDate);
              const date2 = new Date();
              const diffTime = Math.abs(date2 - date1);
              const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
              if (diffDays <= 2 && cakeOrder.paymentType === "cod") {
                cakeOrder.urgent = "yes";
                cakeOrderForDB.urgent = "yes";
              } else {
                cakeOrder.urgent = "no";
                cakeOrderForDB.urgent = "no";
              }
              console.log(
                " checkPreviousRecord.length ",
                checkPreviousRecord.length
              );
              if (checkPreviousRecord.length > 0) {
                await deleteCakeOrder(orderId);
              }

              var photo = null;
              var musterPhoto = null;
              if (cakeOrder.photo) {
                photo = parsePhoto(orderId, cakeOrder.photo);
              }

              if (cakeOrder.musterPhoto) {
                musterPhoto = parseMusterPhoto(orderId, cakeOrder.musterPhoto);
              }

              if (musterPhoto) {
                cakeOrder.musterPhotoFileName = musterPhoto.musterFileName;
                cakeOrder.musterPhotoFilePath = musterPhoto.musterFilePath;
                cakeOrderForDB.musterPhotoFileName = musterPhoto.musterFileName;
                cakeOrderForDB.musterPhotoFilePath = musterPhoto.musterFilePath;
                saveMusterPhoto(cakeOrder, musterPhoto);
              }

              if (photo) {
                cakeOrder.photoFileName = photo.fileName;
                cakeOrder.photoFilePath = photo.filePath;
                cakeOrderForDB.photoFileName = photo.fileName;
                cakeOrderForDB.photoFilePath = photo.filePath;
                savePhoto(cakeOrder, photo);
              } else {
                // console.log(' generateInvoice Befor -----------  ',cakeOrder.paymentType)
                if (
                  cakeOrder.paymentType === "cod" ||
                  cakeOrder.paymentType === "creditcard"
                ) {
                  generateInvoice(cakeOrder);
                }
              }
              let mollieAmount = cakeOrder.price.toFixed(2).toString();
              console.log(" mollieAmount ", mollieAmount);
              if (cakeOrder.paymentType == "mollie") {
                // let createMolliePayment = await createMolliePayment(cakeOrder, mollieAmount, 'cakeOrder')
                // cakeOrderForDB.mollieTransactionId = createMolliePayment.id;
                // cakeOrderForDB.paymentDetails = createMolliePayment;
                // insertOrder(cakeOrderForDB);

                await mollieClient.payments
                  .create({
                    amount: {
                      value: mollieAmount,
                      currency: "EUR",
                    },
                    description: `Zahlung für Bestellung ${cakeOrder.orderId}`,
                    redirectUrl: `${settings.mollieRedirectUrl}?orderId=${cakeOrder.orderId}`,
                    webhookUrl: settings.mollieWebhookUrl,
                    metadata: { orderId: cakeOrder.orderId },
                  })
                  .then((paymentData) => {
                    cakeOrderForDB.mollieTransactionId = paymentData.id;
                    cakeOrderForDB.paymentDetails = paymentData;
                    insertOrder(cakeOrderForDB);
                    // res.send(paymentData);
                  })
                  .catch((error) => {
                    console.log(" error in catch ");
                    // console.log(webhookUrl)
                    console.log(error);
                    res.send(error);
                  });
              } else {
                insertOrder(cakeOrderForDB);
              }
            });
        }
      }
      else {
        res.status(400).send("Incomplete data");
      }
    }
  });
};

/**The below API is called from the main app- custom-cake.js after successful paypal transaction and a response is received. */
__api.cakeOrderUpdate = function (req, res) {
  console.log(
    "Paypal Transaction successful! for ",
    req.body.orderIdForUpdate,
    " .. Calling API cakeOrderUpdate.",
    getDateTimeNow(),
    req.body
  );
  db.connect(function (err) {
    if (err) {
      console.log(`there was an error: ${err}`);
    } else {
      var collection = db.get().collection("CakeOrder");

      const orderIdForUpdate = req.body.orderIdForUpdate;

      var updatedData = {
        paymentDetails: req.body.paymentDetails,
        status: req.body.status,
        partialAmount: req.body.partialAmount,
      };

      collection.update(
        { orderId: parseInt(orderIdForUpdate) },
        {
          $set: updatedData,
        },
        function (err, results) {
          if (err) {
            console.log(err);
          } else {
            collection
              .find({ orderId: parseInt(orderIdForUpdate) })
              .toArray(async function (err, resultsTosend) {
                resultsTosend[0].photo = req.body.photo;
                if (resultsTosend[0].deliveryAddress) {
                  resultsTosend[0].addressTxt = "Lieferdatum";
                } else {
                  resultsTosend[0].addressTxt = "Abholdatum";
                }
                invoice.calculateDates(resultsTosend[0]);
                if (resultsTosend[0].photoFileName) {
                  await fetchImage(resultsTosend[0].photoFileName).then(
                    function (data) {
                      let buff = new Buffer(data);
                      let base64data = buff.toString("base64");
                      resultsTosend[0].photo = `data:image/png;base64,${base64data}`;
                    }
                  );
                }
                invoice
                  .generateInvoice(resultsTosend[0])
                  .then(function () {
                    console.log(
                      "Invoice updated successfully for ",
                      orderIdForUpdate,
                      "at",
                      getDateTimeNow()
                    );
                    sendEmail.sendCustomCakeEmail(resultsTosend[0]);
                  })
                  .catch(function (err) {
                    resultsTosend[0].subj =
                      "Error In Updation Into Database With OrderId " +
                      resultsTosend[0].orderId;
                    sendEmail.sendDbErrorMail(resultsTosend[0]);
                    console.log("Error in generating invoice:", err);
                  });
              });
            res.send(orderIdForUpdate);
          }
        }
      );
    }
  });
};

__api.getProductList = function (req, res) {
  console.log(`Calling getProductList at ${getDateTimeNow()}`);
  var list = productList.getProductList();

  res.send(JSON.stringify(list));
};

__api.getKatalog = function (req, res) {
  console.log("api getKatalog called date and time: ", getDateTimeNow());
  db.connect(function (err) {
    if (err) {
      console.log("Unable to connect to DB!");
      res.sendStatus(510);
    } else {
      console.log("Connected successfully");
      var Katalog = db.get().collection("Katalog");
      Katalog.find({}).toArray(function (err, katalogs) {
        res.send(katalogs);
      });
    }
  });
};

__api.reviews = function (req, res) {
  console.log(`Calling API reviews at ${getDateTimeNow()}`);
  db.connect(function (err) {
    if (err) {
      console.log("Unable to connect to DB!");
      res.sendStatus(510);
    } else {
      console.log("Connected successfully");
      var collection = db.get().collection("google_reviews");
      collection.find({}).toArray(function (err, respo) {
        res.send({
          review_name_1: respo[0].name,
          review_name_2: respo[1].name,
          review_name_3: respo[2].name,
          review_name_4: respo[3].name,
          review_name_5: respo[4].name,
          review_pic_1: respo[0].pic,
          review_pic_2: respo[1].pic,
          review_pic_3: respo[2].pic,
          review_pic_4: respo[3].pic,
          review_pic_5: respo[4].pic,
          review_1: respo[0].review,
          review_2: respo[1].review,
          review_3: respo[2].review,
          review_4: respo[3].review,
          review_5: respo[4].review,
          rating_1: respo[0].rating,
          rating_2: respo[1].rating,
          rating_3: respo[2].rating,
          rating_4: respo[3].rating,
          rating_5: respo[4].rating,
          relative_time_1: respo[0].relative_time_description,
          relative_time_2: respo[1].relative_time_description,
          relative_time_3: respo[2].relative_time_description,
          relative_time_4: respo[3].relative_time_description,
          relative_time_5: respo[4].relative_time_description,
        });
      });
      db.close(function (err) {
        if (err) {
          console.log("Cant close connection", err);
        }
      });
    }
  });
};

__api.sendTestEmail = function (req, res) {
  console.log(
    `Trying to send email to ${req.body.toEmail} from /api/sendTestEmail`
  );
  sendEmail.sendMail({
    from: req.body.fromEmail, //'info@efendibey.de',
    to: req.body.toEmail,
    cc: req.body.ccEmail,
    subject: "This is a Test Email",
    text: `Hello ${req.body.name}\n\nThis is a test email that was sent as a response to the /api/sendTestEmail call\n\nThanks and Regards\nDigiKlug Solutions LLP`,
  });

  res.sendStatus(200);
};

__api.generateTestPDF = function (req, res) {
  /*
    POST Body (Optional):
    {
       "orderId": 201700001
    }
    */

  var cakeOrder = CakeOrder();
  cakeOrder.orderId = req.body.orderId ? req.body.orderId : 201700001;
  cakeOrder.name = "Daanish Rumani";
  cakeOrder.email = "daanishrumani@hotmail.com";
  cakeOrder.mobile = "9898989898";
  cakeOrder.deliveryDate = "2018/05/30";
  cakeOrder.deliveryTime = "18.30";
  cakeOrder.street = "Circle St.";
  cakeOrder.city = "Hannover";
  cakeOrder.shape = "SHP01";
  cakeOrder.size = "NO1";
  cakeOrder.color = "CLR01";
  cakeOrder.decoration = "DRN02";
  cakeOrder.sauceColor = "CLR04";
  cakeOrder.decoColor = "CLFV01";
  cakeOrder.flavor = "FLV01";
  cakeOrder.extraFlavor = ["FLV03", "FLV05", "FLV08"];
  cakeOrder.extraDecoration = ["EDECO02", "EDECO03"];
  cakeOrder.text = "HAPPY PI DAY!";
  cakeOrder.remarks = "What Remarks?";
  cakeOrder.price = 28;
  cakeOrder.photo = null;
  cakeOrder.photoPath = "";
  cakeOrder.musterPhoto = null;
  cakeOrder.musterPhotoPath = "";
  cakeOrder.paymentType = "cod";
  cakeOrder.paymentDetails = null;
  cakeOrder.remarks =
    "Did you know that March (03) the 14th is called a PI day (3.14)";

  invoice.calculateDates(cakeOrder);
  invoice.generateInvoice(cakeOrder).then(function () {
    res.sendStatus(200);
  });
};

/**The below API is being called by the Admin- Dashbord page  */
__api.lineChartDataByRange = function (req, res) {
  console.log(
    `Calling lineChartDataByRange at ${getDateTimeNow()} from ${req.query.fromDate
    } to ${req.query.toDate}.`
  );
  var date_from = req.query.fromDate;
  var date_to = req.query.toDate;
  db.connect(function (err) {
    if (err) {
      // console.log("err ",err);
      res.sendStatus(510);
    } else {
      var collection = db.get().collection("CakeOrder");
      var options = {};
      if (date_from) {
        if (!options["createdDate"]) options["createdDate"] = {};
        var dateFrom = moment(new Date(date_from)).toDate();
        options["createdDate"]["$gte"] = dateFrom;
      }

      if (date_to) {
        if (!options["createdDate"]) options["createdDate"] = {};
        var dateTo = moment(new Date(date_to)).toDate();
        options["createdDate"]["$lte"] = dateTo;
      }

      collection
        .aggregate(
          [
            {
              $match: {
                $and: [
                  {
                    createdDate: {
                      $gte: dateFrom,
                    },
                  },
                  {
                    createdDate: {
                      $lte: dateTo,
                    },
                  },
                  {
                    status: { $nin: ["CANC", "canceled", "expired"] },
                  },
                ],
              },
            },
            {
              $project: {
                createdDate: {
                  $dateToString: { format: "%Y-%m-%d", date: "$createdDate" },
                },
                paymentType: "$paymentType",
                price: "$price",
              },
            },
            {
              $group: {
                _id: {
                  createdDate: "$createdDate",
                  paymentType: "$paymentType",
                },
                "COUNT(_id)": {
                  $sum: 1,
                },
                // "TOTALPRICE": { "$sum" : "$price" }
                TOTALPRICE: {
                  $sum: {
                    $convert: { input: "$price", to: "int" },
                  },
                },
              },
            },
            {
              $project: {
                COUNT: "$COUNT(_id)",
                TOTALPRICE: "$TOTALPRICE",
                createdDate: "$_id.createdDate",
                paymentType: "$_id.paymentType",
                _id: 0,
              },
            },
            {
              $sort: {
                createdDate: 1,
              },
            },
          ],
          {
            allowDiskUse: true,
          }
        )
        .toArray(function (err, respo) {
          let respoArr = [];
          if (err) {
            console.log(err);
            return res.status(500).send({ message: err.message });
          }
          if (respo) {
            respoArr.push(respo);
          }
          res.send(respoArr);
        });
    }
  });
};

/**The below API is being called by the Admin- Dashbord page  */
__api.lineChartDataByRangeLastYear = function (req, res) {
  console.log(
    `Calling lineChartDataByRangeLastYear at ${getDateTimeNow()} from ${req.query.fromDate
    } to ${req.query.toDate}`
  );
  var date_from = req.query.fromDate;
  var date_to = req.query.toDate;
  db.connect(function (err) {
    if (err) {
      console.log("err ", err);
      res.sendStatus(510);
    } else {
      var collection = db.get().collection("CakeOrder");
      var options = {};
      if (date_from) {
        if (!options["createdDate"]) options["createdDate"] = {};
        var dateFrom = moment(new Date(date_from)).toDate();
        options["createdDate"]["$gte"] = dateFrom;
      }

      if (date_to) {
        if (!options["createdDate"]) options["createdDate"] = {};
        var dateTo = moment(new Date(date_to)).toDate();
        options["createdDate"]["$lte"] = dateTo;
      }

      collection
        .aggregate(
          [
            {
              $match: {
                $and: [
                  {
                    createdDate: {
                      $gte: dateFrom,
                    },
                  },
                  {
                    createdDate: {
                      $lte: dateTo,
                    },
                  },
                  {
                    status: { $nin: ["CANC", "canceled", "expired"] },
                  },
                ],
              },
            },
            {
              $project: {
                createdDate: {
                  $dateToString: { format: "%Y-%m-%d", date: "$createdDate" },
                },
                paymentType: "$paymentType",
                price: "$price",
              },
            },
            {
              $group: {
                _id: {
                  createdDate: "$createdDate",
                  paymentType: "$paymentType",
                },
                "COUNT(_id)": {
                  $sum: 1,
                },
                TOTALPRICE: { $sum: "$price" },
              },
            },
            {
              $project: {
                COUNT: "$COUNT(_id)",
                TOTALPRICE: "$TOTALPRICE",
                createdDate: "$_id.createdDate",
                paymentType: "$_id.paymentType",
                _id: 0,
              },
            },
            {
              $sort: {
                createdDate: 1,
              },
            },
          ],
          {
            allowDiskUse: true,
          }
        )
        .toArray(function (err, respo) {
          let respoArr = [];
          if (err) {
            console.log(err);
            return res.status(500).send({ message: err.message });
          }
          if (respo) {
            respoArr.push(respo);
          }
          res.send(respoArr);
        });
    }
  });
};

/**The below API is being called from Summarize-view component */
__api.getCatlogCount = function (req, res) {
  console.log(
    `Calling getCatlogCount at ${getDateTimeNow()} on ${req.query.preparationDate
    }`
  );
  var checkdate = req.query.preparationDate;
  db.connect(function (err) {
    if (err) {
      console.log(err);
      res.sendStatus(510);
    } else {
      var collection = db.get().collection("CakeOrder");
      console.log("Connected successfully");
      collection
        .aggregate([
          {
            $match: {
              catalogNumber: { $ne: null },
              preparationDate: { $eq: checkdate },
              status: { $ne: "CANC" },
            },
          },
          {
            $group: {
              _id: {
                catalogNumber: "$catalogNumber",
                size: "$size",
                shape: "$shape",
                qty: "$qty",
              },
              totalSizeCount: { $sum: 1 },
            },
          },
        ])
        .toArray(function (err, respo) {
          let respoArr = [];
          if (err) {
            console.log(err);
            return res.status(500).send({ message: err.message });
          }
          if (respo) {
            respoArr.push(respo);
          }
          res.send(respoArr);
        });
    }
  });
};

__api.getOnlineOrders = function (req, res) {
  console.log(
    `Calling getOnlineOrders at ${getDateTimeNow()} on ${req.query.date} `
  );
  var date = req.query.date.slice(1, -1);
  db.connect(function (err) {
    if (err) {
      console.log(err);
      res.sendStatus(510);
    } else {
      var collection = db.get().collection("CakeOrder");
      console.log("Connected successfully");
      collection
        .aggregate(
          [
            {
              $match: {
                todaysDate: date,
              },
            },
            {
              $group: {
                _id: {
                  paymentType: "$paymentType",
                  internalOrder: "$internalOrder",
                },
                "COUNT(orderId)": {
                  $sum: 1,
                },
              },
            },
            {
              $project: {
                "COUNT(orderId)": "$COUNT(orderId)",
                internalOrder: "$_id.internalOrder",
                paymentType: "$_id.paymentType",
                _id: 0,
              },
            },
          ],
          {
            allowDiskUse: true,
          }
        )
        .toArray(function (err, respo) {
          let respoArr = [];
          if (err) {
            return res.status(500).send({ message: err.message });
          }
          if (respo) {
            respoArr.push(respo);
          }
          res.send(respoArr);
        });
    }
  });
};

/** This API is being called from MDashboard */
__api.getPayPalOrder = function (req, res) {
  console.log(
    `Calling getPayPalOrder at ${getDateTimeNow()} on ${req.query.date}`
  );
  var date = req.query.date.slice(1, -1);
  db.connect(function (err) {
    if (err) {
      console.log(err);
      res.sendStatus(510);
    } else {
      var collection = db.get().collection("CakeOrder");
      console.log("Connected successfully");
      collection
        .aggregate(
          [
            {
              $match: {
                paymentType: "paypal",
                todaysDate: date,
              },
            },
            {
              $group: {
                _id: {},
                "COUNT(orderId)": {
                  $sum: 1,
                },
              },
            },
            {
              $project: {
                "COUNT(orderId)": "$COUNT(orderId)",
                _id: 0,
              },
            },
          ],
          {
            allowDiskUse: true,
          }
        )
        .toArray(function (err, respo) {
          let respoArr = [];
          if (err) {
            return res.status(500).send({ message: err.message });
          }
          if (respo) {
            respoArr.push(respo);
          }
          res.send(respoArr);
        });
    }
  });
};

__api.getTableReservationList = function (req, res) {
  console.log(
    `Calling getTableReservationList at ${getDateTimeNow()} on ${req.query.date
    }`
  );
  var date = req.query.date.slice(1, -1);

  db.connect(function (err) {
    if (err) {
      console.log(err);
      res.sendStatus(510);
    } else {
      var collection = db.get().collection("TableReservation");
      collection
        .find({
          date: { $eq: date },
        })
        .toArray(function (err, respo) {
          let respoArr = [];
          if (err) {
            return res.status(500).send({ message: err.message });
          }
          if (respo) {
            respoArr.push(respo);
          }
          res.send(respoArr);
        });
    }
  });
};

__api.getPendingCakeOrders = function (req, res) {
  console.log(`Calling getPendingCakeOrders at ${getDateTimeNow()}`);

  db.connect(function (err) {
    if (err) {
      console.log(err);
      res.sendStatus(510);
    } else {
      var collection = db.get().collection("CakeOrder");
      console.log("Connected successfully");
      collection
        .aggregate(
          [
            {
              $match: {
                status: {
                  $in: ["CLSE", "PAYL", "paypal"],
                },
              },
            },
            {
              $group: {
                _id: {
                  status: "$status",
                },
                "COUNT(_id)": {
                  $sum: 1,
                },
              },
            },
            {
              $project: {
                "COUNT(_id)": "$COUNT(_id)",
                status: "$_id.status",
                _id: 0,
              },
            },
          ],
          {
            allowDiskUse: true,
          }
        )
        .toArray(function (err, respo) {
          let respoArr = [];
          if (err) {
            return res.status(500).send({ message: err.message });
          }
          if (respo) {
            respoArr.push(respo);
          }
          res.send(respoArr);
        });
    }
  });
};

__api.getEventReservationList = function (req, res) {
  console.log(
    `Calling getEventReservationList at ${getDateTimeNow()} on ${req.query.date
    }`
  );
  var date = req.query.date.slice(1, -1);
  console.log("getEventReservationList Date ", date);

  db.connect(function (err) {
    if (err) {
      console.log(err);
      res.sendStatus(510);
    } else {
      var collection = db.get().collection("EventReservation");
      // console.log('Connected successfully');
      collection
        .find({
          date: date,
        })
        .toArray(function (err, respo) {
          let respoArr = [];
          if (err) {
            return res.status(500).send({ message: err.message });
          }
          if (respo) {
            respoArr.push(respo);
          }
          res.send(respoArr);
        });
    }
  });
};

__api.getColumns = function (req, res) {
  console.log(
    `Calling getColumns at ${getDateTimeNow()} for user ${req.query.user
    }, for view ${req.query.for}`
  );

  var user = req.query.user;
  var forView = req.query.for;

  db.connect(function (err) {
    if (err) {
      console.log(err);
      res.sendStatus(510);
    } else {
      var collection = db.get().collection("columunConfig");
      console.log("Connected successfully getColumns");
      collection
        .find({
          user: user,
          for: forView,
        })
        .toArray(function (err, respo) {
          let respoArr = [];
          if (err) {
            return res.status(500).send({ message: err.message });
          }
          if (respo) {
            respo.forEach((data) => {
              respoArr.push(data);
            });
          }
          res.send(respoArr);
        });
    }
  });
};

__api.getColumnsupdate = function (req, res) {
  console.log(
    `Calling getColumnsupdate at ${getDateTimeNow()}, request : ${req.body}`
  );

  db.connect(function (err) {
    if (err) {
      console.log(`there was an error: ${err}`);
      //res.sendStatus(510);
    } else {
      var collection = db.get().collection("columunConfig");
      console.log("Connected successfully");

      var dbId = req.query.id;
      var updatedData = {
        columnInfo: req.body,
      };

      collection.update(
        { _id: ObjectId(dbId) },
        {
          $set: updatedData,
        },
        function (err, results) {
          if (err) {
            console.log(err);
          } else {
            console.log("columns updated successfully ");
            res.send(results);
          }
        }
      );
    }
  });
};

__api.getCouponList = function (req, res) {
  console.log(
    `Calling getCouponList at ${getDateTimeNow()}, request pageNr,pageSize,sortdata,sortactive: ${parseInt(
      req.query.pageNumber
    )},${parseInt(req.query.pageSize)},${req.query.sortOrder},${req.query.sortActive
    }`
  );
  var pageNr = parseInt(req.query.pageNumber);
  var pageSize = parseInt(req.query.pageSize);
  var sortdata = req.query.sortOrder;
  var sortactive = req.query.sortActive;
  var searchAll = req.query.filter.trim();
  var skips = pageSize * (pageNr + 1 - 1);
  var sortorder;

  if (sortdata === "asc") {
    sortorder = 1;
  } else {
    sortorder = -1;
  }

  db.connect(function (err) {
    if (err) {
      console.log(err);
      res.sendStatus(510);
    } else {
      var collection = db.get().collection("Campaign");
      if (searchAll) {
        const regex = new RegExp(escapeRegex(searchAll), "gi");
        matchCondition = {
          $match: {
            $and: [
              {
                $or: [
                  { CampaignId: regex },
                  { Code: regex },
                  { ValidFrom: regex },
                  { ValidTo: regex },
                  { DiscValue: regex },
                  { DiscType: regex },
                  { MiniPrice: regex },
                  { TotalCount: regex },
                  { CurrentCount: regex },
                ],
              },
            ],
          },
        };
        collection
          .aggregate(
            [
              {
                $project: {
                  CampaignId: 1,
                  Code: 1,
                  ValidFrom: 1,
                  ValidTo: 1,
                  DiscValue: 1,
                  DiscType: 1,
                  MiniPrice: 1,
                  TotalCount: 1,
                  CurrentCount: 1,
                },
              },
              matchCondition,
              { $skip: skips },
              { $limit: pageSize },
            ],
            { allowDiskUse: true }
          )
          .toArray(function (err, respo) {
            let respoArr = [];
            if (err) {
              //console.log("message: err.message ", err.message);
              return res.status(500).send({ message: err.message });
            }
            if (respo) {
              console.log(" respo", respo);
              respoArr.push(respo);
            }
            res.send(respoArr);
          });
      } else {
        var matchCondition = {};
        var sortOperator = { $sort: {} },
          sort = sortactive;
        sortOperator["$sort"][sort] = sortorder;

        collection
          .aggregate(
            [
              {
                $project: {
                  CampaignId: 1,
                  Code: 1,
                  ValidFrom: 1,
                  ValidTo: 1,
                  DiscValue: 1,
                  DiscType: 1,
                  MiniPrice: 1,
                  TotalCount: 1,
                  CurrentCount: 1,
                },
              },
              sortOperator,
              { $skip: skips },
              { $limit: pageSize },
            ],
            { allowDiskUse: true }
          )
          .toArray(function (err, respo) {
            let respoArr = [];
            if (err) {
              return res.status(500).send({ message: err.message });
            }
            if (respo) {
              respoArr.push(respo);
            }
            res.send(respoArr);
          });
      }
    }
  });
};

__api.getTotalForCouponTable = function (req, res) {
  console.log(
    `Calling getTotalForCouponTable at ${getDateTimeNow()}, filter: ${req.query.filter
    }`
  );

  db.connect(function (err) {
    if (err) {
      console.log(err);
      res.sendStatus(510);
    } else {
      var collection = db.get().collection("Campaign");
      var searchAll = req.query.filter.trim();
      const regex = new RegExp(escapeRegex(searchAll), "gi");
      var matchCondition = {};
      matchCondition = {
        $match: {
          $and: [
            {
              $or: [
                { CampaignId: regex },
                { Code: regex },
                { ValidFrom: regex },
                { ValidTo: regex },
                { DiscValue: regex },
                { DiscType: regex },
                { MiniPrice: regex },
                { TotalCount: regex },
                { CurrentCount: regex },
              ],
            },
          ],
        },
      };
      collection
        .aggregate(
          [matchCondition, { $group: { _id: null, recordCount: { $sum: 1 } } }],
          { allowDiskUse: true }
        )
        .toArray(function (err, respo) {
          let respoArr = [];
          if (err) {
            //console.log("message: err.message ", err.message);
            return res.status(500).send({ message: err.message });
          }
          if (respo) {
            if (respo == "") {
              res.send([{ _id: null, recordCount: 0 }]);
            } else {
              res.send(respo);
            }
          }
        });
    }
  });
};

__api.storeUpdateCoupon = function (req, res) {
  console.log(`Calling storeUpdateCoupon at ${getDateTimeNow()}, 
    request couponCode,validFromDate,validToDate,discountType,
    discountValue,minPirce,totalCount: ${req.body.couponCode},
    ${moment(req.body.validFromDate).toDate()},${moment(
    req.body.validToDate
  ).toDate()},
    ${req.body.discountType},${req.body.discountValue},${parseInt(
    req.body.minPirce
  )},
    ${parseInt(req.body.totalCount)}`);
  let CampaignId = req.query.CampaignId;
  let couponFormData = {
    Code: req.body.couponCode,
    ValidFrom: moment(req.body.validFromDate).toDate(),
    ValidTo: moment(req.body.validToDate).toDate(),
    DiscType: req.body.discountType,
    DiscValue: req.body.discountValue,
    MiniPrice: parseInt(req.body.minPirce),
    TotalCount: parseInt(req.body.totalCount),
  };

  if (CampaignId === "-1") {
    ////// Insert Coupon
    db.connect(function (err) {
      if (err) {
        console.log(err);
        res.sendStatus(510);
      } else {
        db.getNextSequenceValue("Campaign", function (e, obj) {
          if (e) {
            console.log(e);
            res.sendStatus(501);
          } else {
            couponFormData.CampaignId = "CAM00" + obj.value.sequence_value;
            couponFormData.CurrentCount = 0;
            let CampaignCollection = db.get().collection("Campaign");
            CampaignCollection.insertOne(couponFormData, function (err) {
              if (err) {
                res.send({
                  error: true,
                  message: "Error inserting data",
                });
              } else {
                res.send({
                  error: false,
                  message: "Success",
                });
              }
            });
          }
        });
      }
    });
  } else {
    ///// Update Coupon
    db.connect(function (err) {
      if (err) {
        console.log("there was an error");
        res.sendStatus(510);
      } else {
        var collection = db.get().collection("Campaign");
        collection.update(
          { CampaignId: CampaignId },
          {
            $set: couponFormData,
          },
          function (err, results) {
            if (err) {
              console.log(err);
            } else {
              console.log(" Updated......");

              res.send(results);
            }
          }
        );
      }
    });
  }
};

// This API is being called from the Online Order Menu.
__api.getOnlineOrderMenu = function (req, res) {
  console.log(`Calling getOnlineOrderMenu at ${getDateTimeNow()}`);
  db.connect(function (err) {
    if (err) {
      console.log(err);
      res.sendStatus(510);
    } else {
      var collection = db.get().collection("onlineMenu");
      // console.log('Connected successfully');\

      collection
        .find({}, { sort: { displayOrder: 1 } })
        .toArray(function (err, respo) {
          let respoArr = [];
          let productTypes = [];
          let allProductWithTitle = [];
          if (err) {
            console.log(err);
            // return res.status(500).send({ message: `Coupon expired!` });
          }
          if (respo[0]) {
            respo.forEach((data) => {
              productTypes.indexOf(data.productType) === -1
                ? productTypes.push(data.productType)
                : "This item already exists";
              // productTypes.push(data.productType);
            });

            let i = 1;
            productTypes.forEach((data) => {
              let allMenuWithPeticularTitle = [];
              respo.forEach((elementOfMenuList) => {
                if (elementOfMenuList.productType === data) {
                  allMenuWithPeticularTitle.push(elementOfMenuList);
                }
                // totalTitleFromApi.indexOf(element.productType) === -1 ? totalTitleFromApi.push(element.productType) : console.log("This item already exists");
              });

              // allProductWithTitle[data] = allMenuWithPeticularTitle;
              allProductWithTitle.push({
                id: i,
                productType: data,
                data: allMenuWithPeticularTitle,
              });
              i++;
            });
            res.send(allProductWithTitle);
          }
        });
    }
  });
};

__api.getOnlineProductType = function (req, res) {
  console.log(
    `Calling getOnlineProductType at ${getDateTimeNow()}, request: ${req.body}`
  );
  db.connect(function (err) {
    if (err) {
      console.log(err);
      res.sendStatus(510);
    } else {
      var collection = db.get().collection("onlineMenu");

      collection
        .aggregate([{ $group: { _id: "$productType" } }])
        .toArray(function (err, respo) {
          let respoArr = [];
          if (err) {
            console.log(err);
            return res.status(500).send({ message: err.message });
          }
          if (respo) {
            respoArr.push(respo);
          }
          res.send(respoArr[0]);
        });
    }
  });
};

__api.menuOrderUpdate = function (req, res) {
  console.log(
    "Calling menuOrderUpdate at " + getDateTimeNow() + " details: " + req.body
  );

  db.connect(function (err) {
    if (err) {
      console.log("there was an error");
    } else {
      var collection = db.get().collection("OnlineOrder");
      const menuOrderIdUpdate = req.body.menuOrderIdUpdate;

      let price = req.body.price.toString();
      let splitValue = price.split(".");
      let priceModified = "";
      if (splitValue.length > 1)
        priceModified = splitValue[0] + "," + splitValue[1];
      else priceModified = price + ",00";

      var updatedData = {
        paymentDetails: req.body.paymentDetails,
        status: req.body.status,
        //  partialAmount: priceModified
      };

      collection.update(
        { menuOrderId: parseInt(menuOrderIdUpdate) },
        {
          $set: updatedData,
        },
        function (err, results) {
          if (err) {
            console.log(err);
          } else {
            collection
              .find({ menuOrderId: parseInt(menuOrderIdUpdate) })
              .toArray(function (err, resultsTosend) {
                menuinvoice
                  .generateMenuInvoice(resultsTosend[0])
                  .then(function () {
                    sendEmail.sendCustomMenuOrderEmail(resultsTosend[0]);
                    console.log(
                      `${menuOrderIdUpdate} updated successfully with Paypal payment details.`
                    );
                  })
                  .catch(function (err) {
                    resultsTosend[0].subj =
                      "Error In Updation Into Database With OrderId " +
                      resultsTosend[0].menuOrderId;
                    sendEmail.sendDbErrorMail(resultsTosend[0]);
                    console.log(`Error: ${err}`);
                  });
              });
            res.send(menuOrderIdUpdate);
          }
        }
      );
    }
  });
};

__api.menuOrderUpdateAsCLSE = function (req, res) {
  console.log(
    `Calling menuOrderUpdateAsCLSE at ${getDateTimeNow()}, Request: ${req.body}`
  );

  db.connect(function (err) {
    if (err) {
      console.log("there was an error");
    } else {
      var collection = db.get().collection("OnlineOrder");
      console.log("Connected successfully");

      const menuOrderIdUpdate = req.body.menuOrderIdUpdate;
      var updatedData = {
        status: req.body.status,
      };
      collection.update(
        { menuOrderId: parseInt(menuOrderIdUpdate) },
        {
          $set: updatedData,
        },
        function (err, results) {
          if (err) {
            updatedData.subj =
              "Error In Changing Status As CLSE With menuOrderId " +
              menuOrderIdUpdate;
            sendEmail.sendDbErrorMail(updatedData);
            console.log(err);
          }
          console.log("successfully status changed to CLSE");
          res.send(results);
          // res.sendStatus(510);
        }
      );
    }
  });
};

__api.menuOrder = function (req, res) {
  console.log(
    "Calling menuOrder at " + getDateTimeNow() + " details: " + req.body
  );
  var newMenuOrder = function (menuOrderId) {
    var menuOrder = MenuOrder();
    menuOrder.menuOrderId = menuOrderId;
    menuOrder.name = req.body.fname + " " + req.body.lname;
    menuOrder.email = req.body.email;
    menuOrder.mobile = req.body.mobile;
    menuOrder.text = req.body.text;
    menuOrder.paymentType = req.body.paymentType;
    menuOrder.mollieTransactionId = req.body.mollieTransactionId;
    menuOrder.paymentDetails = req.body.paymentDetails;
    menuOrder.deliveryAddressPincod = req.body.deliveryAddressPincod;
    menuOrder.deliveryCharge = req.body.deliveryCharge
      .toFixed(2)
      .toString()
      .replace(".", ",");
    menuOrder.deliveryDistance = req.body.deliveryDistance;
    menuOrder.deliveryAddress = req.body.deliveryAddress;
    menuOrder.deliveryType = req.body.deliveryType;
    menuOrder.pickupTime = req.body.pickupTime;
    menuOrder.cartContain = req.body.cartContain;
    menuOrder.productPrice = req.body.productPrice
      .toFixed(2)
      .toString()
      .replace(".", ",");
    menuOrder.totalAmount = req.body.totalAmount
      .toFixed(2)
      .toString()
      .replace(".", ",");
    menuOrder.taxAmount = req.body.taxAmount;
    menuOrder.price = req.body.productPrice
      .toFixed(2)
      .toString()
      .replace(".", ",");
    menuOrder.pickupchecked = req.body.pickupchecked;
    menuOrder.createdDate = new Date();

    menuOrder.status =
      req.body.paymentType == "cod" || req.body.paymentType == "creditcard"
        ? "ORDR"
        : "PAYL";
    return menuOrder;
  };

  var newMenuOrderForDB = function (menuOrderId) {
    var menuOrder = MenuOrder();
    menuOrder.menuOrderId = menuOrderId;
    menuOrder.name = req.body.fname + " " + req.body.lname;
    menuOrder.email = req.body.email;
    menuOrder.mobile = req.body.mobile;
    menuOrder.text = req.body.text;
    menuOrder.paymentType = req.body.paymentType;
    menuOrder.paymentDetails = req.body.paymentDetails;
    menuOrder.deliveryAddressPincod = req.body.deliveryAddressPincod;
    menuOrder.deliveryCharge = req.body.deliveryCharge
      .toFixed(2)
      .toString()
      .replace(".", ",");
    menuOrder.deliveryDistance = req.body.deliveryDistance;
    menuOrder.deliveryAddress = req.body.deliveryAddress;
    menuOrder.deliveryType = req.body.deliveryType;
    menuOrder.pickupTime = req.body.pickupTime;
    menuOrder.cartContain = req.body.cartContain;
    menuOrder.productPrice = req.body.productPrice
      .toFixed(2)
      .toString()
      .replace(".", ",");
    menuOrder.totalAmount = req.body.totalAmount
      .toFixed(2)
      .toString()
      .replace(".", ",");
    menuOrder.taxAmount = req.body.taxAmount;
    menuOrder.price = req.body.productPrice
      .toFixed(2)
      .toString()
      .replace(".", ",");
    menuOrder.pickupchecked = req.body.pickupchecked;
    menuOrder.createdDate = new Date();

    menuOrder.status =
      req.body.paymentType == "cod" || req.body.paymentType == "creditcard"
        ? "ORDR"
        : "PAYL";

    /// console.log('menuOrder in 2 db @@@@@@',menuOrder);
    return menuOrder;
  };

  //Generate invoice and then send the confirmation email with invoice attached
  var generateMenuInvoice = function (menuOrder) {
    console.log(" menuOrder generateMenuInvoice ", menuOrder);
    menuinvoice
      .generateMenuInvoice(menuOrder)
      .then(function () {
        console.log("menu order invoice generated at: ", getDateTimeNow());
        if (
          menuOrder.paymentType == "cod" ||
          menuOrder.paymentType == "creditcard"
        ) {
          sendEmail.sendCustomMenuOrderEmail(menuOrder);
        }
      })
      .catch(function (err) {
        console.log("Error occurred in sending email");
        console.log(`START ERROR /api/menuOrder:generateMenuInvoice(), ${err}`);
        console.log("END ERROR /api/menuOrder:generateMenuInvoice()");
      });
  };

  var insertOrder = function (menuOrder) {
    console.log(" cameinto insertOrder ");
    var collection = db.get().collection("OnlineOrder");
    collection.insertOne(menuOrder, function (err) {
      if (err) {
        console.log(" menuOrder ======= if ", err);
        menuOrder.subj =
          "Error In Inserting Into Database With menuOrderId " +
          menuOrder.menuOrderId;
        sendEmail.sendDbErrorMail(JSON.stringify(menuOrder));
        res.send({
          error: true,
          menuOrderId: menuOrder.menuOrderId,
          message: "Error inserting data",
        });
      } else {
        console.log(" menuOrderelse =======  ", menuOrder.paymentType);
        if (menuOrder.paymentType == "mollie") {
          res.send(menuOrder.paymentDetails);
          db.close(function (err) {
            if (err) {
              console.log("Cant close connection", err);
            }
          });
        } else {
          res.send({
            error: false,
            orderId: menuOrder.menuOrderId,
            message:
              'Bei Bestellungen mit weniger als fünf Tage Vorlaufzeit nur "barzahlung" oder "paypal" möglich',
          });
          db.close(function (err) {
            if (err) {
              console.log("Cant close connection", err);
            }
          });
        }
      }
    });
  };

  var isOrderValid = function () {
    if (req.body.cartContain.length == 0 || req.body.price == null) {
      return false;
    }
    if (req.body.fname == "" || req.body.lname == "" || req.body.email == "" || req.body.mobile == "") {
      return false;
    }
    if (req.body.deliveryType == "Selbstabholung" && req.body.pickupTime == "") {
      return false;
    }
    if (req.body.deliveryType == "Lieferung" && (req.body.deliveryAddress == "" || req.body.deliveryAddressPincod == "")) {
      return false;
    }
    return true;
  }

  db.connect(function (err) {
    if (err) {
      console.log("Unable to connect to DB!.\nRequest:");
      console.log(newMenuOrder(-1));
      res.sendStatus(510);
    } else {
      var orderValid = isOrderValid();
      if (orderValid) {
        var menuOrderId = req.body.menuOrderId;
        var collection = db.get().collection("OnlineOrder");
        collection
          .find({ menuOrderId: parseInt(menuOrderId) })
          .toArray(async function (err, checkPreviousRecord) {
            var menuOrder = newMenuOrder(menuOrderId);
            var menuOrderForDB = newMenuOrderForDB(menuOrderId);

            menuinvoice.calculateDates(menuOrder);

            menuOrderForDB.todaysDate = menuOrder.todaysDate;
            console.log(" checkPreviousRecord ========= ", checkPreviousRecord);
            console.log(
              " checkPreviousRecord.length ",
              checkPreviousRecord.length
            );
            if (checkPreviousRecord.length > 0) {
              console.log(" if > 0 came here ======== ");
              // if already record found in database it means it is paypal transaction
              collection.deleteOne(
                { menuOrderId: parseInt(menuOrderId) },
                async function (err, obj) {
                  if (err) throw err;
                  // console.log(' generateInvoice Befor -----------  ')
                  if (
                    menuOrder.paymentType === "cod" ||
                    menuOrder.paymentType === "creditcard"
                  ) {
                    generateMenuInvoice(menuOrder);
                    insertOrder(menuOrderForDB);
                  } else if (menuOrder.paymentType == "mollie") {
                    //let mollieAmount = (req.body.productPrice.toFixed(2)).toString();
                    let mollieAmount = req.body.totalAmount.toFixed(2).toString();

                    let createMolliepaymentData = await createMolliePayment(
                      menuOrder,
                      mollieAmount,
                      "onlineMenu"
                    );
                    console.log(
                      " createMolliepaymentData ========= ",
                      createMolliepaymentData
                    );
                    menuOrderForDB.mollieTransactionId =
                      createMolliepaymentData.id;
                    menuOrderForDB.paymentDetails = createMolliepaymentData;
                    insertOrder(menuOrderForDB);
                  } else if (menuOrder.paymentType == "paypal") {
                    insertOrder(menuOrderForDB);
                  }
                }
              );
            } else {
              console.log(
                " menuOrder ========= elsecame here ",
                menuOrder.paymentType
              );
              if (
                menuOrder.paymentType === "cod" ||
                menuOrder.paymentType === "creditcard"
              ) {
                generateMenuInvoice(menuOrder);
                insertOrder(menuOrderForDB);
              } else if (menuOrder.paymentType == "mollie") {
                // let mollieAmount = (req.body.productPrice.toFixed(2)).toString();
                let mollieAmount = req.body.totalAmount.toFixed(2).toString();
                console.log(" mollieAmount ========= ", mollieAmount);
                let createMolliepaymentData = await createMolliePayment(
                  menuOrder,
                  mollieAmount,
                  "onlineMenu"
                );
                console.log(
                  " createMolliepaymentData ========= ",
                  createMolliepaymentData
                );
                menuOrderForDB.mollieTransactionId = createMolliepaymentData.id;
                menuOrderForDB.paymentDetails = createMolliepaymentData;
                insertOrder(menuOrderForDB);
              } else if (menuOrder.paymentType == "paypal") {
                insertOrder(menuOrderForDB);
              }
            }
          });
      }
      else{
        res.status(400).send("Incomplete data");
      }
    }
  });
};

__api.getAllMenuOrder = function (req, res) {
  console.log(`Calling getAllMenuOrder at ${getDateTimeNow()} `);

  db.connect(function (err) {
    if (err) {
      console.log(err);
      res.sendStatus(510);
    } else {
      var collection = db.get().collection("OnlineOrder");
      //console.log('Connected successfully getColumns');
      collection
        .find()
        .sort({ _id: -1 })
        .toArray(function (err, respo) {
          let respoArr = [];
          if (err) {
            return res.status(500).send({ message: err.message });
          }
          if (respo) {
            respo.forEach((data) => {
              respoArr.push(data);
            });
          }
          res.send(respoArr);
        });
    }
  });
};

__api.getMenuOrder = function (req, res) {
  console.log(
    `Calling getMenuOrder at ${getDateTimeNow()} for id: ${req.query.Id}`
  );
  var ID = req.query.Id;
  db.connect(function (err) {
    if (err) {
      console.log(err);
      res.sendStatus(510);
    } else {
      var collection = db.get().collection("OnlineOrder");
      console.log("Connected successfully getColumns");
      collection
        .find({ menuOrderId: parseInt(ID) })
        .toArray(function (err, respo) {
          let respoArr = [];
          if (err) {
            return res.status(500).send({ message: err.message });
          }
          if (respo) {
            respo.forEach((data) => {
              respoArr.push(data);
            });
          }
          res.send(respoArr);
        });
    }
  });
};

__api.getAllMenuProducts = function (req, res) {
  console.log(`Calling getAllMenuProducts at ${getDateTimeNow()}`);

  db.connect(function (err) {
    if (err) {
      console.log(err);
      res.sendStatus(510);
    } else {
      var collection = db.get().collection("onlineMenu");
      console.log("Connected successfully getColumns");
      collection.find().toArray(function (err, respo) {
        let respoArr = [];
        if (err) {
          return res.status(500).send({ message: err.message });
        }
        if (respo) {
          respo.forEach((data) => {
            respoArr.push(data);
          });
        }
        res.send(respoArr);
      });
    }
  });
};

__api.getMenuProduct = function (req, res) {
  console.log(
    `Calling getMenuProduct at ${getDateTimeNow()} for product id: ${req.query.Id
    }`
  );

  var ID = req.query.Id;
  db.connect(function (err) {
    if (err) {
      console.log(err);
      res.sendStatus(510);
    } else {
      var collection = db.get().collection("onlineMenu");
      collection.find({ productId: ID }).toArray(function (err, respo) {
        let respoArr = [];
        if (err) {
          return res.status(500).send({ message: err.message });
        }
        if (respo) {
          respo.forEach((data) => {
            respoArr.push(data);
          });
        }
        res.send(respoArr);
      });
    }
  });
};

__api.getMenuOrderPDFFile = function (req, res) {
  console.log(
    `Calling getMenuOrderPDFFile at ${getDateTimeNow()}, for manu order id: ${req.query.Id
    } `
  );
  var checkmenuOrderId = req.query.Id;
  let invoiceName = "online_delivery_" + checkmenuOrderId + ".pdf";

  return new Promise((resolve, reject) => {
    fs.readFile(path.join(settings.invoicesHome, invoiceName), (err, data) => {
      if (err) {
        reject(err);
      } else {
        resolve(data);
      }
    });
  })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      throw err;
    });
};

__api.addMenuProduct = function (req, res) {
  console.log(
    `Calling addMenuProduct at ${getDateTimeNow()}, Details: ${req.body} `
  );

  var newOnlineMenu = function (productId) {
    var onlineMenu = OnlineMenu();
    onlineMenu.productId = productId.toString();
    onlineMenu.productType = req.body.productType;
    onlineMenu.productName = req.body.productName;
    onlineMenu.productDescription = req.body.productDescription;
    onlineMenu.extraInfo = req.body.extraInfo;
    onlineMenu.allergens = req.body.allergens;
    onlineMenu.productRate = req.body.productRate;
    onlineMenu.extraProductType = req.body.extraProductType;
    onlineMenu.productExtras = req.body.prodExtras;
    var productPhoto = req.body.photo;
    var productPhotoName = req.body.photo_name;
    var productOhotoUrl = req.body.imageurl;

    return onlineMenu;
  };

  var insertOrder = function (onlineMenu) {
    var collection = db.get().collection("onlineMenu");
    collection.insertOne(onlineMenu, function (err) {
      if (err) {
        console.log(err);
        res.send({
          error: true,
          productId: onlineMenu.productId,
          message: "Error inserting data",
        });
      } else {
        res.send({
          error: false,
          productId: onlineMenu.productId,
          message:
            'Bei Bestellungen mit weniger als fünf Tage Vorlaufzeit nur "barzahlung" oder "paypal" möglich',
        });
      }

      db.close(function (err) {
        if (err) {
          console.log("Cant close connection", err);
        }
      });
    });
  };

  db.connect(function (err) {
    if (err) {
      console.log("Unable to connect to DB!.\nRequest:");
      console.log(newOnlineMenu(-1));
      res.sendStatus(510);
    } else {
      db.getNextSequenceValue("onlineOrder", function (err, obj) {
        if (err) {
          console.log(`Error: ${err}`);
          res.sendStatus(511);
        } else {
          var productId = 01001 + obj.value.sequence_value; // 201800001 - 201899999
          var onlineMenu = newOnlineMenu(productId);
          console.log(`Processing OnlineMenu: ${onlineMenu.productId}`);
          insertOrder(onlineMenu);
        }
      });
    }
  });
};

__api.updateMenuProduct = function (req, data) {
  console.log(
    `Calling  at ${getDateTimeNow()},for product id ${req.query.productId
    } Details: ${data.req.body}`
  );
  console.log("in update cake form api");

  db.connect(function (err) {
    if (err) {
      console.log(`There was an error: ${err}`);
      //res.sendStatus(510);
    } else {
      var collection = db.get().collection("onlineMenu");
      console.log("Connected successfully");
      const productId = req.query.productId;

      var updatedData = {
        productType: data.req.body.productType,
        productName: data.req.body.productName,
        productDescription: data.req.body.productDescription,
        extraInfo: data.req.body.extraInfo,
        allergens: data.req.body.allergens,
        productRate: data.req.body.productRate,
        extraProductType: data.req.body.extraProductType,
        productExtras: data.req.body.prodExtras,
      };

      console.log(" updatedData ", updatedData);

      collection.update(
        { productId: productId },
        {
          $set: updatedData,
        },
        function (err, results) {
          if (err) {
            console.log(err);
          } else {
            data.send(results);
            console.log("updated successfully ========== ");
          }
        }
      );
    }
  });
};

__api.shopOpenTime = function (req, res) {
  console.log(`Calling shopOpenTime at ${getDateTimeNow()}`);
  db.connect(function (err) {
    if (err) {
      console.log(err);
      res.sendStatus(510);
    } else {
      var collection = db.get().collection("ShopTime");
      collection.find().toArray(function (err, respo) {
        res.send(respo[0]);
      });
    }
  });
};

__api.getTotalForFoodOrder = function (req, res) {
  console.log(`Calling getTotalForFoodOrder at ${getDateTimeNow()}`);
  db.connect(function (err) {
    if (err) {
      console.log(err);
      res.sendStatus(510);
    } else {
      var searchAll = req.query.filter.trim();
      var collection = db.get().collection("OnlineOrder");
      const regex = new RegExp(escapeRegex(searchAll), "gi");
      var matchCondition = {};
      if (searchAll) {
        matchCondition = {
          $match: {
            $and: [
              {
                $or: [
                  { menuOrderId: parseInt(searchAll) },
                  { name: regex },
                  { email: regex },
                  { mobile: regex },
                  { price: regex },
                ],
              },
              { status: { $eq: "ORDR" } },
            ],
          },
        };
        collection
          .aggregate(
            [
              matchCondition,
              { $group: { _id: null, recordCount: { $sum: 1 } } },
            ],
            { allowDiskUse: true }
          )
          .toArray(function (err, respo) {
            let respoArr = [];
            if (err) {
              console.log("message: err.message Sayeed ", err.message);
              return res.status(500).send({ message: err.message });
            }
            if (respo) {
              if (respo == "") {
                res.send([{ _id: null, recordCount: 0 }]);
              } else {
                res.send(respo);
              }
            }
          });
      } else {
        matchCondition = {
          $match: {
            $and: [
              {
                $or: [
                  { menuOrderId: parseInt(searchAll) },
                  { name: regex },
                  { email: regex },
                  { mobile: regex },
                  { price: regex },
                ],
              },
              { status: { $eq: "ORDR" } },
            ],
          },
        };
        collection
          .aggregate(
            [
              matchCondition,
              { $group: { _id: null, recordCount: { $sum: 1 } } },
            ],
            { allowDiskUse: true }
          )
          .toArray(function (err, respo) {
            let respoArr = [];
            if (err) {
              return res.status(500).send({ message: err.message });
            }
            if (respo) {
              if (respo == "") {
                res.send([{ _id: null, recordCount: 0 }]);
              } else {
                res.send(respo);
              }
            }
          });
      }
    }
  });
};

__api.getFoodOrders = function (req, res) {
  console.log(`Calling getFoodOrders at ${getDateTimeNow()}, request 
    pageNr,pageSize,sortdata,sortactive: ${parseInt(
    req.query.pageNumber
  )},${parseInt(req.query.pageSize)},
    ${req.query.sortOrder},${req.query.sortActive}`);

  var pageNr = parseInt(req.query.pageNumber);
  var pageSize = parseInt(req.query.pageSize);
  var sortdata = req.query.sortOrder;
  var sortactive = req.query.sortActive;
  var searchAll = req.query.filter.trim();
  var skips = pageSize * (pageNr + 1 - 1);
  var sortorder;

  sortorder = sortdata === "asc" ? 1 : -1;
  db.connect(function (err) {
    if (err) {
      console.log(err);
      res.sendStatus(510);
    } else {
      var collection = db.get().collection("OnlineOrder");
      var sortOperator = { $sort: {} },
        sort = sortactive;
      sortOperator["$sort"][sort] = sortorder;
      var matchCondition = {};
      const regex = new RegExp(escapeRegex(searchAll), "gi");
      if (searchAll) {
        matchCondition = {
          $match: {
            $and: [
              {
                $or: [
                  { menuOrderId: parseInt(searchAll) },
                  { name: regex },
                  { email: regex },
                  { mobile: regex },
                  { price: regex },
                ],
              },
              { status: { $eq: "ORDR" } },
            ],
          },
        };
        collection
          .aggregate(
            [
              {
                $project: {
                  menuOrderId: 1,
                  status: 1,
                  name: 1,
                  email: 1,
                  mobile: 1,
                  price: 1,
                  createdDate: 1,
                },
              },
              matchCondition,
              sortOperator,
              { $skip: skips },
              { $limit: pageSize },
            ],
            { allowDiskUse: true }
          )
          .toArray(function (err, respo) {
            let respoArr = [];
            if (err) {
              //  console.log("message: err.message  ", err.message);
              return res.status(500).send({ message: err.message });
            }
            if (respo) {
              respoArr.push(respo);
            }
            res.send(respoArr);
          });
      } else {
        matchCondition = {
          $match: {
            $and: [
              {
                $or: [
                  { menuOrderId: parseInt(searchAll) },
                  { name: regex },
                  { email: regex },
                  { mobile: regex },
                  { price: regex },
                ],
              },
              { status: { $eq: "ORDR" } },
            ],
          },
        };
        collection
          .aggregate(
            [
              {
                $project: {
                  menuOrderId: 1,
                  status: 1,
                  name: 1,
                  email: 1,
                  mobile: 1,
                  price: 1,
                  createdDate: 1,
                },
              },
              matchCondition,
              sortOperator,
              { $skip: skips },
              { $limit: pageSize },
            ],
            { allowDiskUse: true }
          )
          .toArray(function (err, respo) {
            let respoArr = [];
            if (err) {
              return res.status(500).send({ message: err.message });
            }
            if (respo) {
              respoArr.push(respo);
            }
            res.send(respoArr);
          });
      }
    }
  });
};

__api.changeStatusOfOnlineOrder = function (req, res) {
  console.log(
    `Calling changeStatusOfOnlineOrder at ${getDateTimeNow()} for menu order id ${req.query.menuOrderId
    } to ${req.body.status} status`
  );
  if (req.body.status && req.query.menuOrderId) {
    db.connect(function (err) {
      if (err) {
        console.log(`There was an error: ${err}`);
      } else {
        var collection = db.get().collection("OnlineOrder");
        console.log("Connected successfully");

        const orderIdForUpdate = req.query.menuOrderId;
        var updatedData = {
          status: req.body.status,
          updatedAt: new Date(),
        };
        collection.update(
          { menuOrderId: parseInt(orderIdForUpdate) },
          {
            $set: updatedData,
          },
          function (err, results) {
            if (err) {
              console.log(err);
            } else {
              res.send(updatedData);
            }
          }
        );
      }
    });
  } else {
    res.send(12345);
  }
};

__api.deliveryTime = function (req, res) {
  console.log(
    `Calling deliveryTime at ${getDateTimeNow()} for get the delivery time`
  );
  db.connect(function (err) {
    if (err) {
      console.log(`There was an error: ${err}`);
    } else {
      console.log("Connected successfully");
      var collection = db.get().collection("DeliveryTime");
      collection.find({ id: 1 }).toArray(function (err, resultsTosend) {
        res.send(resultsTosend);
      });
    }
  });
  // res.send(req);
};
__api.updateDeliveryTime = function (req, res) {
  console.log(
    `Calling updateDeliveryTime at ${getDateTimeNow()} for update the time and body data is ${req.body
    }`
  );
  var updatedData = {
    deliveryTime: req.body.deliveryTime,
  };
  var collection = db.get().collection("DeliveryTime");
  collection.update(
    { id: 1 },
    {
      $set: updatedData,
    },
    function (err, results) {
      if (err) {
        console.log(err);
      } else {
        res.send({ status: "success", data: "Successfully updated" });
      }
    }
  );
};
__api.getMolliePayment = function (req, res) {
  //console.log("Calling get payment Mollie", req.body);
  var orderId = req.body.orderId;
  var onlineMenuTrue = req.body.onlineMenu;
  var menuOrderId = req.body.orderId;
  db.connect(function (err) {
    if (err) {
      console.log("there was an error");
    }
    var collection;

    if (onlineMenuTrue) {
      //console.log('inside if online menu')
      collection = db.get().collection("OnlineOrder");
      collection
        .find({ menuOrderId: parseInt(menuOrderId) })
        .toArray(function (err, checkPreviousRecord) {
          //console.log(" checkPreviousRecord online menu", checkPreviousRecord);
          let paymentId = checkPreviousRecord[0].mollieTransactionId;
          //let transactionData = '';
          (async () => {
            try {
              const payment = await mollieClient.payments.get(paymentId);
              res.send(payment);
            } catch (error) {
              console.warn(error);
            }
          })();
        });
    } else {
      //console.log('inside else cake order')
      collection = db.get().collection("CakeOrder");
      collection
        .find({ orderId: parseInt(orderId) })
        .toArray(function (err, checkPreviousRecord) {
          //console.log(" checkPreviousRecord ", checkPreviousRecord);
          let paymentId = checkPreviousRecord[0].mollieTransactionId;

          //let transactionData = '';
          (async () => {
            try {
              const payment = await mollieClient.payments.get(paymentId);
              res.send(payment);
            } catch (error) {
              console.warn(error);
            }
          })();
        });
    }

    //let payment = {};
  });
};

__api.mollieWebhook = function (req, res) {
  let transactionId = req.body.id;
  console.log(" mollieWebhook called  ", transactionId);

  //let transactionData = '';
  (async () => {
    try {
      const mollieData = await mollieClient.payments.get(transactionId);

      if (mollieData) {
        let updatedStatus = "";
        if (mollieData.status == "paid") {
          updatedStatus = "ORDR";
        } else {
          updatedStatus = mollieData.status;
        }
        const orderId = mollieData.metadata.orderId;

        db.connect(function (err) {
          if (err) {
            console.log(`Error: ${err}`);
          } else {
            var collection = db.get().collection("CakeOrder");
            collection
              .find({ orderId: parseInt(orderId) })
              .toArray(function (err, resultsTosend) {
                if (err) {
                  console.log(err);
                }

                if (resultsTosend[0].status != "ORDR") {
                  collection.update(
                    { orderId: parseInt(orderId) },
                    {
                      $set: {
                        status: updatedStatus,
                        paymentDetails: mollieData,
                        mollieTransactionId: transactionId,
                      },
                    },
                    async function (err, results) {
                      if (err) {
                        console.log(err);
                      }
                      console.log(`updated successfully order : ${orderId}`);
                      // resultsTosend[0].photo = req.body.photo;
                      if (resultsTosend[0].deliveryAddress) {
                        resultsTosend[0].addressTxt = "Lieferdatum";
                      } else {
                        resultsTosend[0].addressTxt = "Abholdatum";
                      }
                      resultsTosend[0].paymentDetails = mollieData;
                      invoice.calculateDates(resultsTosend[0]);
                      if (resultsTosend[0].photoFileName) {
                        await fetchImage(resultsTosend[0].photoFileName).then(
                          function (data) {
                            let buff = new Buffer(data);
                            let base64data = buff.toString("base64");
                            resultsTosend[0].photo = `data:image/png;base64,${base64data}`;
                          }
                        );
                      }
                      let partialAmount = parseFloat(
                        resultsTosend[0].price
                      ).toFixed(2);
                      resultsTosend[0].partialAmount = partialAmount.replace(
                        ".",
                        ","
                      );
                      console.log(
                        "resultsTosend[0] ======== ",
                        resultsTosend[0]
                      );

                      if (mollieData.status == "paid") {
                        invoice
                          .generateInvoice(resultsTosend[0])
                          .then(function () {
                            console.log(
                              "Mollie invoice generated for paid order"
                            );
                            sendEmail.sendCustomCakeEmail(resultsTosend[0]);
                            res.send(JSON.stringify(results));
                          })
                          .catch(function (err) {
                            resultsTosend[0].subj =
                              "Error In Updation Into Database With OrderId " +
                              resultsTosend[0].orderId;
                            sendEmail.sendDbErrorMail(resultsTosend[0]);
                            console.log(
                              "Error in generating Mollie invoice:",
                              err
                            );
                            res.send(JSON.stringify(resultsTosend));
                          });
                      }
                      // res.sendStatus(510);
                    }
                  );
                } else {
                  res.send(JSON.stringify(resultsTosend));
                }
              });
          }
        });
      }
    } catch (error) {
      console.log(error);
    }
  })();
};

__api.mollieWebhookForOnlineOrder = function (req, res) {
  let transactionId = req.body.id;
  console.log(
    " mollieWebhookForOnlineOrder mollieWebhook called  ",
    transactionId
  );

  //let transactionData = '';
  (async () => {
    try {
      const mollieData = await mollieClient.payments.get(transactionId);
      if (mollieData) {
        let updatedStatus = "";
        if (mollieData.status == "paid") {
          updatedStatus = "ORDR";
        } else {
          updatedStatus = mollieData.status;
        }
        const menuOrderId = mollieData.metadata.menuOrderId;
        console.log(" mollieData ========== ", menuOrderId);

        db.connect(function (err) {
          if (err) {
            console.log(`Error: ${err}`);
          } else {
            var collection = db.get().collection("OnlineOrder");
            collection
              .find({ menuOrderId: parseInt(menuOrderId) })
              .toArray(function (err, resultsTosend) {
                if (err) {
                  console.log(err);
                }
                console.log(
                  " mollieWebhookForOnlineOrder resultsTosend[0] ",
                  resultsTosend[0]
                );

                if (resultsTosend[0].status != "ORDR") {
                  collection.update(
                    { menuOrderId: parseInt(menuOrderId) },
                    {
                      $set: {
                        status: updatedStatus,
                        paymentDetails: mollieData,
                        mollieTransactionId: transactionId,
                      },
                    },
                    async function (err, results) {
                      if (err) {
                        console.log(err);
                      }
                      console.log(
                        `updated successfully order : ${menuOrderId}`
                      );
                      // resultsTosend[0].photo = req.body.photo;
                      // console.log(" resultsTosend[0] ", resultsTosend[0]);
                      resultsTosend[0].paymentDetails = mollieData;
                      if (mollieData.status == "paid") {
                        console.log("mollie paid");
                        menuinvoice
                          .generateMenuInvoice(resultsTosend[0])
                          .then(function () {
                            console.log(
                              "menu order invoice generated at: ",
                              getDateTimeNow()
                            );
                            sendEmail.sendCustomMenuOrderEmail(
                              resultsTosend[0]
                            );
                            res.send(JSON.stringify(resultsTosend));
                          })
                          .catch(function (err) {
                            console.log("Error occurred in sending email");
                            console.log(
                              `START ERROR /api/menuOrder:generateMenuInvoice(), ${err}`
                            );
                            console.log(
                              "END ERROR /api/menuOrder:generateMenuInvoice()"
                            );
                          });
                      } else {
                        res.send(JSON.stringify(resultsTosend));
                      }

                      // res.sendStatus(510);
                    }
                  );
                } else {
                  res.send(JSON.stringify(resultsTosend));
                }
              });
          }
        });
      }
    } catch (error) {
      console.log(error);
    }
  })();
};

// function return date time now
function getDateTimeNow() {
  let date_ob = new Date();

  // current date
  // adjust 0 before single digit date
  let date = ("0" + date_ob.getDate()).slice(-2);

  // current month
  let month = ("0" + (date_ob.getMonth() + 1)).slice(-2);

  // current year
  let year = date_ob.getFullYear();

  // current hours
  let hours = date_ob.getHours();

  // current minutes
  let minutes = date_ob.getMinutes();

  // current seconds
  let seconds = date_ob.getSeconds();

  return (
    date +
    "-" +
    month +
    "-" +
    year +
    " " +
    hours +
    ":" +
    minutes +
    ":" +
    seconds
  );
}

var fetchImage = function (fileName) {
  if (fileName) {
    return new Promise((resolve, reject) => {
      fs.readFile(path.join(settings.photosHome, fileName), (err, data) => {
        if (err) {
          reject(err);
        } else {
          resolve(data);
        }
      });
    });
  } else {
    return new Promise((resolve, reject) => {
      resolve("success");
    });
  }
};

var deleteCakeOrder = function (orderId) {
  return new Promise((resolve, reject) => {
    db.connect(function (err) {
      if (err) {
        console.log("Unable to connect to DB!.\nRequest:");
        console.log(newCakeOrder(-1));
        res.sendStatus(510);
      } else {
        var collection = db.get().collection("CakeOrder");
        collection.deleteOne(
          { orderId: parseInt(orderId) },
          function (err, obj) {
            if (err) {
              reject();
            } else {
              console.log(" record deleted ", orderId);
              resolve();
            }
          }
        );
      }
    });
  });
};

var createMolliePayment = function (orderDetail, mollieAmount, orderType) {
  let orderId = 0;
  let redirectUrl = "";
  let metadata = {};
  if (orderType === "onlineMenu") {
    orderId = orderDetail.menuOrderId;
    redirectUrl = `${settings.mollieRedirectUrl}online?menuOrderId=${orderId}`;
    metadata = { menuOrderId: orderId };
  } else {
    orderId = orderDetail.orderId;
    redirectUrl = `${settings.mollieRedirectUrl}?orderId=${orderId}`;
    metadata = { orderId: orderId };
  }
  return new Promise((resolve, reject) => {
    mollieClient.payments
      .create({
        amount: {
          value: mollieAmount,
          currency: "EUR",
        },
        description: `Zahlung für Bestellung ${orderId}`,
        redirectUrl: redirectUrl,
        webhookUrl: settings.mollieWebhookUrlForOnlineOrder,
        metadata: metadata,
      })
      .then((paymentData) => {
        resolve(paymentData);
      })
      .catch((error) => {
        console.log(" error in catch ");
        // console.log(webhookUrl)
        console.log(error);
        res.send(error);
      });
  });
};

module.exports = __api;
