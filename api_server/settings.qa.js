/* eslint-disable jsx-a11y/href-no-hash */
/* jshint esversion: 6 */

module.exports = {
   mode: 'QA',
   //dbConnectionString: 'mongodb://45.76.162.227:27017/efendibey?authMechanism=DEFAULT&authSource=efendibey',
   dbConnectionString: 'mongodb://He3pSMVrW2G8Nbay5z4Y:PhU6cPkP24DLfd3S4xg@45.76.162.227:27017/efendibey?authMechanism=DEFAULT&authSource=efendibey',
   httpPort: 80,
   httpsPort: 443,
   hrHttpPort: 81,
   hrHttpsPort: 442,
   httpsEnabled: true,
   emailTransport: {
      host: "wp10551405.mailout.server-he.de",
      port: 465,
      secure: true,
      auth: {
         user: "wp10551405-bestellung",
         pass: "Efendibey1!"
      },
      tls: {
         rejectUnauthorized: false // do not fail on invalid certs
      }
   },
   adminAppHost: "qa.ebadmin.digiklug.com",
   //adminMenuHost: "qa.onlinemenu.digiklug.com",
   /*productPhotosHome: '/var/www/efendibey/public/productTypeImage',
   invoicesHome: '/var/www/efendibey/invoices',
   photosHome: '/var/www/efendibey/photos',*/
   productPhotosHome: '/var/www/EfendibeyNewFS/Efendibey/public/productTypeImage',
   invoicesHome: '/var/www/EfendibeyNewFS/Efendibey/invoices',
   photosHome: '/var/www/EfendibeyNewFS/Efendibey/photos',
   fromEmail: 'bestellung@efendibey.de',
   ccEmail: ['sufyan@digiklug.com', 'minaz@digiklug.com'],
   ccEmailForDBError: ['info@digiklug.com', 'gausmohammad.shaikh@digiklug.com', 'farhan.shaikh@digiklug.com'],
   toEmailForDBError: ['saeed.ansari@digiklug.com'],
   mollieRedirectUrl: 'https://qa.efendibey.digiklug.com/',
   mollieWebhookUrl: 'https://qa.efendibey.digiklug.com/api/mollieWebhook',
   mollieWebhookUrlForOnlineOrder: 'https://qa.efendibey.digiklug.com/api/mollieWebhookForOnlineOrder',
   mollieApiKey: 'test_3tEf9rceQ6kr7WxkJ6emSj57qtJnnf',
};
