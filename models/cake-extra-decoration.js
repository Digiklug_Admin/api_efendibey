const Product = require('./cake-product');

module.exports = function(code, name, price) {
    return Product(code, name, 'extra-decoration', false, price);
};