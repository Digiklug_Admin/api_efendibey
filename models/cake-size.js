const Product = require('./cake-product');

module.exports = function(code, name) {
    return Product(code, name, 'size', true, 0.0);
};
