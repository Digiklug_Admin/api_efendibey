const Product = require('./cake-product');

module.exports = function(code, name) {
    return Product(code, name, 'decoColor', true, 0.0);
};