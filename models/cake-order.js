const CakeShape = require('./cake-shape');
const CakeSize = require('./cake-size');
const CakeColor = require('./cake-color');
const CakeDecoration = require('./cake-decoration');
const CakeFlavor = require('./cake-flavor');
//const CakeDekoFlavor = require('./cake-dekoFlavors');
const CakeDecoColor = require('./cake-deco-color');


module.exports = function() {
    return {
        orderId: new Date().getTime(),
        name: '',
        email: '',
        mobile: '',
        deliveryDate: '',
        deliveryTime: '',
        deliveryType: 'pickup',
        street: '',
        city: '',
        shape: 'SHPXX',
        size: 'SZEXX',
        color: 'CLRXX',
        decoration: 'DRNXX',
        sauceColor: 'CLRXX',
        extraDecoration: ['EDECOXX'],
        flavor: 'FLVXX',
        extraFlavor: ['FLVXX'],
        //dekoFlavors: ['CLFVXX'],
        decoColor: ['CLFVXX'],
        photo: '',
        photoFileName: '',
        photoFilePath: '',
        text: '',
        remarks: '',
        price: 0.0,
        totalTax: 0.0,
        taxAtDefaultRate: 0.0,
        taxAtCustomRate: 0.0,
        createdDate: new Date(),
        paymentType: "cod",
        mollieTransactionId: '',
        paymentDetails: null,
        internalOrder: false,
        photoLaterChecked: false,
        catalogNumber: '',
        extraFoodPrice: 0,
        extraNonFoodPrice: 0,
        extraTaxes: 0,
        partialAmount: 0,
        totalTax: 0,
        employee: ''
    };
};