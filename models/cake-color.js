const Product = require('./cake-product');

module.exports = function(code, name) {
    return Product(code, name, 'color', false, 0.0);
};
