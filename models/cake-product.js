module.exports = function(code, name, type, ruleBasedPricing, price) {
    return {
        code: code,
        name: name,
        type: type,
        ruleBasedPricing: ruleBasedPricing,
        price: price
    };
};
