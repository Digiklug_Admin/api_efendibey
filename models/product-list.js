var CakeShape = require('./cake-shape');
var CakeSize = require('./cake-size');
var CakeColor = require('./cake-color');
var CakeDecoration = require('./cake-decoration');
var CakeExtraDecoration = require('./cake-extra-decoration');
var CakeFlavor = require('./cake-flavor');
//var CakeDecoColor = require('./cake-deskoFlavors');
var CakeDecoColor = require('./cake-deco-color');

function newProductList() {
   return {
      shapes: {},
      sizes: {},
      colors: {},
      decorations: {},
      extraDecorations: {},
      flavors: {},
      extraFlavors: {},
      decoColors: {},
      priceRules: {},
      taxRules: {}
   };
}

function getProductList() {
   var productList = newProductList();

   productList.shapes.SHP01 = CakeShape('SHP01', 'Rund');
   productList.shapes.SHP02 = CakeShape('SHP02', 'Quadrat');
   productList.shapes.SHP03 = CakeShape('SHP03', 'Rechteck');
   productList.shapes.SHP04 = CakeShape('SHP04', 'Herz');
   //productList.shapes.SHP99 = CakeShape('SHP99', 'Nach Wunsch/Fondantdekor/Hochzeit');
   productList.shapes.SHP99 = CakeShape('SHP99', 'Hochzeit/Fondant');

   productList.sizes.NO1 = CakeSize('NO1', '6-8 Stück (1NO)');
   productList.sizes.NO2 = CakeSize('NO2', '8-10 Stück (2NO)');
   productList.sizes.NO3 = CakeSize('NO3', '10-12 Stück (3NO)');
   productList.sizes.EB0 = CakeSize('EB0', '16-20 Stück (EB0)');
   productList.sizes.EB1 = CakeSize('EB1', '25-30 Stück (EB1)');
   productList.sizes.EB2 = CakeSize('EB2', '30-40 Stück (EB2)');
   productList.sizes.EB3 = CakeSize('EB3', '50-60 Stück (EB3)');
   productList.sizes.OTH = CakeSize('OTH', 'Nach Wunsch/Fondantdekor/Hochzeit');
   // productList.sizes.HF = CakeSize('HF', 'Nach Wunsch/Fondantdekor/Hochzeit');
   productList.sizes.HF = CakeSize('HF', 'Hochzeit/Fondant');

   productList.colors.CLR01 = CakeColor('CLR01', 'Weiss'); 
   productList.colors.CLR02 = CakeColor('CLR02', 'Rosa');
   productList.colors.CLR03 = CakeColor('CLR03', 'Grün');
   productList.colors.CLR04 = CakeColor('CLR04', 'Blau');
   productList.colors.CLR05 = CakeColor('CLR05', 'Creme');
   productList.colors.CLR06 = CakeColor('CLR06', 'Hellblau');
   productList.colors.CLR07 = CakeColor('CLR07', 'Braun');
   productList.colors.CLR08 = CakeColor('CLR08', 'Lila');
   productList.colors.CLR09 = CakeColor('CLR09', 'Schwarz');
   productList.colors.CLR10 = CakeColor('CLR10', 'Rot');
   productList.colors.CLR99 = CakeColor('CLR99', 'Andere');

   productList.decorations.DRN01 = CakeDecoration('DRN01', 'Glatt', 0.0);
   productList.decorations.DRN02 = CakeDecoration('DRN02', 'Sosse', 0.0);
   productList.decorations.DRN03 = CakeDecoration('DRN03', 'gekämmt', 0.0);
   productList.decorations.DRN04 = CakeDecoration('DRN04', 'Icing', 0.0);
   productList.decorations.DRN05 = CakeDecoration('DRN05', 'Glatt mit Sosse', 0.0);
   productList.decorations.DRN06 = CakeDecoration('DRN06', 'Gekämmt mit Sosse', 0.0);
   productList.decorations.DRN07 = CakeDecoration('DRN07', 'Raspel Cremefarben', 0.0);
   productList.decorations.DRN08 = CakeDecoration('DRN08', 'Raspel Braun', 0.0);
   productList.decorations.DRN09 = CakeDecoration('DRN09', 'Raspel Rosa', 0.0);
   productList.decorations.DRN10 = CakeDecoration('DRN10', 'Glatt mit Deko', 0.0);
   productList.decorations.DRN11 = CakeDecoration('DRN11', 'Gekämmt mit Deko', 0.0);
   productList.decorations.DRN12 = CakeDecoration('DRN12', 'Naked cake', 10.0);
   productList.decorations.DRN13 = CakeDecoration('DRN13', 'Drip cake', 10.0);

   productList.extraDecorations.EDECO01 = CakeExtraDecoration('EDECO01', 'Mit Foto', 10.0);
   productList.extraDecorations.EDECO02 = CakeExtraDecoration('EDECO02', 'Kerzen', 2.0);
   productList.extraDecorations.EDECO03 = CakeExtraDecoration('EDECO03', 'Glitzer oder Perlen', 3.0);
   productList.extraDecorations.EDECO04 = CakeExtraDecoration('EDECO04', 'Einschulung', 15.0);
   productList.extraDecorations.EDECO05 = CakeExtraDecoration('EDECO05', 'Macarons 3stück', 4.0);
   productList.extraDecorations.EDECO06 = CakeExtraDecoration('EDECO06', 'Zuckerrosen  2stück', 3.0);
   productList.extraDecorations.EDECO07 = CakeExtraDecoration('EDECO07', '1 Boden höher', 7.0);
   productList.extraDecorations.EDECO08 = CakeExtraDecoration('EDECO08', 'Blümchen am Rand', 10.0);

   productList.flavors.FLV01 = CakeFlavor('FLV01', 'Schokolade', 0.0);
   productList.flavors.FLV02 = CakeFlavor('FLV02', 'Himbeere', 0.0);
   productList.flavors.FLV03 = CakeFlavor('FLV03', 'Oreo', 0.0); 
   productList.flavors.FLV04 = CakeFlavor('FLV04', 'Ganache', 0.0);
   productList.flavors.FLV05 = CakeFlavor('FLV05', 'Kirsch', 0.0);
   productList.flavors.FLV06 = CakeFlavor('FLV06', 'Karamell-Wallnuss', 0.0);
   productList.flavors.FLV07 = CakeFlavor('FLV07', 'Banane', 0.0);
   productList.flavors.FLV08 = CakeFlavor('FLV08', 'Fruchtgarten', 0.0);
   productList.flavors.FLV09 = CakeFlavor('FLV09', 'Krokant', 0.0);
   productList.flavors.FLV10 = CakeFlavor('FLV10', 'Schoko-Himbeere', 0.0);
   productList.flavors.FLV11 = CakeFlavor('FLV11', 'Schoko-Banane', 0.0);
   productList.flavors.FLV12 = CakeFlavor('FLV12', 'Erdbeere', 0.0, {
      month: 3,
      date: 1
   }, {
      month: 7,
      date: 31
   });
   productList.flavors.FLV13 = CakeFlavor('FLV13', 'Waldfrüchte', 0.0);

   productList.extraFlavors.FLV01 = CakeFlavor('FLV01', 'Schokolade', 5.0);
   productList.extraFlavors.FLV02 = CakeFlavor('FLV02', 'Himbeere', 5.0);
   productList.extraFlavors.FLV03 = CakeFlavor('FLV03', 'Oreo', 5.0);
   productList.extraFlavors.FLV04 = CakeFlavor('FLV04', 'Ganache', 5.0);
   productList.extraFlavors.FLV05 = CakeFlavor('FLV05', 'Kirsch', 5.0);
   productList.extraFlavors.FLV06 = CakeFlavor('FLV06', 'Karamell-Wallnuss', 5.0);
   productList.extraFlavors.FLV07 = CakeFlavor('FLV07', 'Banane', 5.0);
   productList.extraFlavors.FLV08 = CakeFlavor('FLV08', 'Fruchtgarten', 5.0);
   productList.extraFlavors.FLV09 = CakeFlavor('FLV09', 'Krokant', 5.0);
   productList.extraFlavors.FLV10 = CakeFlavor('FLV10', 'Schoko-Himbeere', 5.0);
   productList.extraFlavors.FLV11 = CakeFlavor('FLV11', 'Schoko-Banane', 5.0);
   productList.extraFlavors.FLV12 = CakeFlavor('FLV12', 'Erdbeere', 5.0, {
      month: 3,
      date: 1
   }, {
      month: 7,
      date: 31
   });
   productList.extraFlavors.FLV13 = CakeFlavor('FLV13', 'Waldfrüchte', 5.0);

   productList.decoColors.CLFV01 = CakeDecoColor('CLFV01', 'Sosse Rot');
   productList.decoColors.CLFV02 = CakeDecoColor('CLFV02', 'Karamell-Wallnuss');
   productList.decoColors.CLFV03 = CakeDecoColor('CLFV03', 'Banane');
   productList.decoColors.CLFV04 = CakeDecoColor('CLFV04', 'Sosse Grun');
   productList.decoColors.CLFV16 = CakeDecoColor('CLFV16', 'Schokolade');
   productList.decoColors.CLFV05 = CakeDecoColor('CLFV05', 'Weiss');
   productList.decoColors.CLFV06 = CakeDecoColor('CLFV06', 'Rosa');
   productList.decoColors.CLFV07 = CakeDecoColor('CLFV07', 'Grün');
   productList.decoColors.CLFV08 = CakeDecoColor('CLFV08', 'Blau');
   productList.decoColors.CLFV09 = CakeDecoColor('CLFV09', 'Creme');
   productList.decoColors.CLFV10 = CakeDecoColor('CLFV10', 'Hellblau');
   productList.decoColors.CLFV11 = CakeDecoColor('CLFV11', 'Braun');
   productList.decoColors.CLFV12 = CakeDecoColor('CLFV12', 'Lila');
   productList.decoColors.CLFV13 = CakeDecoColor('CLFV13', 'Schwarz');
   productList.decoColors.CLFV14 = CakeDecoColor('CLFV14', 'Rot');
   productList.decoColors.CLFV15 = CakeDecoColor('CLFV15', 'Andere');

   // Shape V/s Size
   productList.priceRules.SHP01_NO1 = 18.0;
   productList.priceRules.SHP01_NO2 = 24.0;
   productList.priceRules.SHP01_NO3 = 30.0;

   productList.priceRules.SHP02_NO1 = 24.0;
   productList.priceRules.SHP02_NO2 = 30.0;
   productList.priceRules.SHP02_NO3 = 36.0;

   productList.priceRules.SHP03_EB0= 45.0;
   productList.priceRules.SHP03_EB1 = 75.0;
   productList.priceRules.SHP03_EB2 = 90.0;
   productList.priceRules.SHP03_EB3 = 150.0;

   // Heart shape added
   productList.priceRules.SHP04_NO1 = 29.0;
   productList.priceRules.SHP04_NO2 = 35.0;
   productList.priceRules.SHP04_NO3 = 45.0;

   // Shape V/s Size V/s Decoration
   productList.priceRules.SHP01_NO1_DRN05 = 2.0;
   productList.priceRules.SHP01_NO2_DRN05 = 2.0;
   productList.priceRules.SHP01_NO3_DRN05 = 2.0;
   productList.priceRules.SHP02_NO1_DRN05 = 2.0;
   productList.priceRules.SHP02_NO2_DRN05 = 2.0;
   productList.priceRules.SHP02_NO3_DRN05 = 2.0;
   productList.priceRules.SHP03_EB0_DRN05 = 3.0;
   productList.priceRules.SHP03_EB1_DRN05 = 5.0;
   productList.priceRules.SHP03_EB2_DRN05 = 5.0;
   productList.priceRules.SHP03_EB3_DRN05 = 7.0;
   productList.priceRules.SHP04_NO1_DRN05 = 2.0;
   productList.priceRules.SHP04_NO2_DRN05 = 2.0;
   productList.priceRules.SHP04_NO3_DRN05 = 2.0;

   productList.priceRules.SHP01_NO1_DRN06 = 2.0;
   productList.priceRules.SHP01_NO2_DRN06 = 2.0;
   productList.priceRules.SHP01_NO3_DRN06 = 2.0;
   productList.priceRules.SHP02_NO1_DRN06 = 2.0;
   productList.priceRules.SHP02_NO2_DRN06 = 2.0;
   productList.priceRules.SHP02_NO3_DRN06 = 2.0;
   productList.priceRules.SHP03_EB0_DRN06 = 3.0;
   productList.priceRules.SHP03_EB1_DRN06 = 5.0;
   productList.priceRules.SHP03_EB2_DRN06 = 5.0;
   productList.priceRules.SHP03_EB3_DRN06 = 7.0;
   productList.priceRules.SHP04_NO1_DRN06 = 2.0;
   productList.priceRules.SHP04_NO2_DRN06 = 2.0;
   productList.priceRules.SHP04_NO3_DRN06 = 2.0;

   productList.priceRules.SHP01_NO1_DRN07 = 3.0;
   productList.priceRules.SHP01_NO2_DRN07 = 3.0;
   productList.priceRules.SHP01_NO3_DRN07 = 3.0;
   productList.priceRules.SHP02_NO1_DRN07 = 3.0;
   productList.priceRules.SHP02_NO2_DRN07 = 3.0;
   productList.priceRules.SHP02_NO3_DRN07 = 3.0;
   productList.priceRules.SHP03_EB0_DRN07 = 4.0;
   productList.priceRules.SHP03_EB1_DRN07 = 6.0;
   productList.priceRules.SHP03_EB2_DRN07 = 6.0;
   productList.priceRules.SHP03_EB3_DRN07 = 8.0;
   productList.priceRules.SHP04_NO1_DRN07 = 3.0;
   productList.priceRules.SHP04_NO2_DRN07 = 3.0;
   productList.priceRules.SHP04_NO3_DRN07 = 3.0;

   productList.priceRules.SHP01_NO1_DRN08 = 3.0;
   productList.priceRules.SHP01_NO2_DRN08 = 3.0;
   productList.priceRules.SHP01_NO3_DRN08 = 3.0;
   productList.priceRules.SHP02_NO1_DRN08 = 3.0;
   productList.priceRules.SHP02_NO2_DRN08 = 3.0;
   productList.priceRules.SHP02_NO3_DRN08 = 3.0;
   productList.priceRules.SHP03_EB0_DRN08 = 4.0;
   productList.priceRules.SHP03_EB1_DRN08 = 6.0;
   productList.priceRules.SHP03_EB2_DRN08 = 6.0;
   productList.priceRules.SHP03_EB3_DRN08 = 8.0;
   productList.priceRules.SHP04_NO1_DRN08 = 3.0;
   productList.priceRules.SHP04_NO2_DRN08 = 3.0;
   productList.priceRules.SHP04_NO3_DRN08 = 3.0;

   productList.priceRules.SHP01_NO1_DRN09 = 5.0;
   productList.priceRules.SHP01_NO2_DRN09 = 5.0;
   productList.priceRules.SHP01_NO3_DRN09 = 5.0;
   productList.priceRules.SHP02_NO1_DRN09 = 5.0;
   productList.priceRules.SHP02_NO2_DRN09 = 5.0;
   productList.priceRules.SHP02_NO3_DRN09 = 5.0;
   productList.priceRules.SHP03_EB0_DRN09 = 5.0;
   productList.priceRules.SHP03_EB1_DRN09 = 7.0;
   productList.priceRules.SHP03_EB2_DRN09 = 7.0;
   productList.priceRules.SHP03_EB3_DRN09 = 10.0;
   productList.priceRules.SHP04_NO1_DRN09 = 5.0;
   productList.priceRules.SHP04_NO2_DRN09 = 5.0;
   productList.priceRules.SHP04_NO3_DRN09 = 5.0;

   productList.priceRules.SHP01_NO1_DRN12 = 10.0;
   productList.priceRules.SHP01_NO2_DRN12 = 10.0;
   productList.priceRules.SHP01_NO3_DRN12 = 10.0;
   productList.priceRules.SHP02_NO1_DRN12 = 10.0;
   productList.priceRules.SHP02_NO2_DRN12 = 10.0;
   productList.priceRules.SHP02_NO3_DRN12 = 10.0;
   productList.priceRules.SHP03_EB0_DRN12 = 10.0;
   productList.priceRules.SHP03_EB1_DRN12 = 10.0;
   productList.priceRules.SHP03_EB2_DRN12 = 10.0;
   productList.priceRules.SHP03_EB3_DRN12 = 10.0;
   productList.priceRules.SHP04_NO1_DRN12 = 10.0;
   productList.priceRules.SHP04_NO2_DRN12 = 10.0;
   productList.priceRules.SHP04_NO3_DRN12 = 10.0;

   productList.priceRules.SHP01_NO1_DRN13 = 10.0;
   productList.priceRules.SHP01_NO2_DRN13 = 10.0;
   productList.priceRules.SHP01_NO3_DRN13 = 10.0;
   productList.priceRules.SHP02_NO1_DRN13 = 10.0;
   productList.priceRules.SHP02_NO2_DRN13 = 10.0;
   productList.priceRules.SHP02_NO3_DRN13 = 10.0;
   productList.priceRules.SHP03_EB0_DRN13 = 10.0;
   productList.priceRules.SHP03_EB1_DRN13 = 10.0;
   productList.priceRules.SHP03_EB2_DRN13 = 10.0;
   productList.priceRules.SHP03_EB3_DRN13 = 10.0;
   productList.priceRules.SHP04_NO1_DRN13 = 10.0;
   productList.priceRules.SHP04_NO2_DRN13 = 10.0;
   productList.priceRules.SHP04_NO3_DRN13 = 10.0;

   productList.taxRules.Default = 0.07;
   productList.taxRules.EDECO02 = 0.19;

   return productList;
};

function getAllProductNamesByCode() {
   var productList = getProductList();
   var allProductsByCode = {};

   for(var shape in productList.shapes) {
      allProductsByCode[shape] = productList.shapes[shape].name;
   }

   for(var size in productList.sizes) {
      allProductsByCode[size] = productList.sizes[size].name;
   }

   for(var color in productList.colors) {
      allProductsByCode[color] = productList.colors[color].name;
   }

   for(var decoration in productList.decorations) {
      allProductsByCode[decoration] = productList.decorations[decoration].name;
   }

   for(var extraDecoration in productList.extraDecorations) {
      allProductsByCode[extraDecoration] = productList.extraDecorations[extraDecoration].name;
   }

   for(var flavor in productList.flavors) {
      allProductsByCode[flavor] = productList.flavors[flavor].name;
   }

   for(var extraFlavor in productList.extraFlavors) {
      allProductsByCode[extraFlavor] = productList.extraFlavors[extraFlavor].name;
   }

   for(var decoColor in productList.decoColors) {
      allProductsByCode[decoColor] = productList.decoColors[decoColor].name;
   }
   /**
    * 
    * for(var decoColor in productList.decoColors) {
      allProductsByCode[decoColor] = productList.decoColors[decoColor].name;
   }
    */

   return allProductsByCode;
}


module.exports = newProductList;
module.exports.getProductList = getProductList;
module.exports.getAllProductNamesByCode = getAllProductNamesByCode;
